var defaultCardViewStyle = {
    color: 'rgb(0,0,0)',
    fontSize: '15px',
    lineHeight: '24px',
    fontWeight: '300',
    errorColor: 'red',
    placeholderColor: ''
}

TPDirect.card.setup('#tappay-iframe', defaultCardViewStyle);
var googlePaySetting = {
	// Optional in sandbox, Required in production
	googleMerchantId: "1251-5071-2398",
	tappayGoogleMerchantId: "standardinfomation_CTBC",
	allowedCardAuthMethods: ["PAN_ONLY", "CRYPTOGRAM_3DS"],
	merchantName: "TapPay Test!",
	emailRequired: true, // optional
	shippingAddressRequired: true, // optional,
	billingAddressRequired: true, // optional
	billingAddressFormat: "MIN", // FULL, MIN

	allowPrepaidCards: true,
	allowedCountryCodes: ['TW'],

	phoneNumberRequired: true // optional
}

TPDirect.googlePay.setupGooglePay(googlePaySetting);  

var paymentRequest = {
	allowedNetworks: ["AMEX", "JCB", "MASTERCARD", "VISA"],
	price: "1", // optional
	currency: "TWD", // optional
}

TPDirect.googlePay.setupPaymentRequest(paymentRequest, function(err, result){
	console.log(result);
	if (result.canUseGooglePay) {
		canUseGooglePay = true
	}
});

function samsungPay(amt) {
	TPDirect.samsungPay.setup({
		country_code: 'tw'
	});

	var paymentRequest = {
		supportedNetworks: ['AMEX', 'MASTERCARD', 'VISA'],
		total: {
			label: 'TapPay',
			amount: {
				currency: 'TWD',
				value: String(amt)
			}
		}
	}
	TPDirect.samsungPay.setupPaymentRequest(paymentRequest);
}

function applePay(amt) {
	var data = {
		supportedNetworks: ['MASTERCARD', 'VISA', 'AMEX'],
		supportedMethods: ['apple_pay'],
		displayItems: [{
			label: 'iPhone8',
			amount: {
				currency: 'TWD',
				value: String(amt)
			}
		}],
		total: {
			label: '付給 TapPay',
			amount: {
				currency: 'TWD',
				value: String(amt)
			}
		}
	};

	TPDirect.paymentRequestApi.setupApplePay({
		// required, your apple merchant id
		// merchant.tech.cherri.global.test 是 DEMO 頁面專門使用的 merchant id
		// 如果要在自己頁面上使用 Apple Pay 請參考 Required 去申請 Apple Pay merchant id
		merchantIdentifier: 'merchant.com.standardinformation',
		// defaults to 'TW'
		countryCode: 'TW'
	});

	TPDirect.paymentRequestApi.setupPaymentRequest(data, function (result) {
		console.log('TPDirect.paymentRequestApi.setupPaymentRequest.result', result)
	
		// 代表瀏覽器支援 payment request api (或 apple pay)
		// 和 TPDirect.paymentRequestApi.checkAvailability() 的結果是一樣的
		// if (!result.browserSupportPaymentRequest) {
		//     return
		// }
	
		// 代表使用者是否有符合 supportedNetworks 與 supportedMethods 的卡片
		// paymentRequestApi ---> canMakePaymentWithActiveCard is result of canMakePayment
		// apple pay         ---> canMakePaymentWithActiveCard is result of canMakePaymentsWithActiveCard

		if(result.browserSupportPaymentRequest == false) {
			alert("您的系統不支援ApplePay");
			return false;
		}
		
		// NOTE: apple pay 只會檢查使用者是否有在 apple pay 裡面綁卡片
		if (result.canMakePaymentWithActiveCard) {

		}
		else {
			// 如果有支援 basic-card 方式，仍然可以開啟 payment request sheet
			// 如果是 apple pay，會引導使用者去 apple pay 綁卡片
			alert("沒有可以支付的卡片");
			return false;
		}
	
		if (window.ApplePaySession) {
			
		}
		
	});
}

$(function () {

    $("a[name='cancelBtn']").on("click", function(){
		var url = $(this).attr("url");
		Swal.fire({
			title: '確定要取消此筆訂單嗎？',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: '是',
			cancelButtonText: '否'
		}).then((result) => {
			if (result.value) {
				$.get(url, {}, function(data){
					if(data.status == "success") {
						$("#orderStatus").text("通知取消");
						// alert(data.message);
						// $(this).hide();
						location.reload();
					}
					else {
						alert("取消發生問題，請聯絡客服人員");
					}
				});
			}
		})
    });

    $("a[name='payBtn']").on("click", function(){
        var ordNo = $(this).attr("ordNo");
        var ordId = $(this).attr("ordId");
        var ttlAmt = $(this).attr("ttlAmt");

        $("#ordNo").text(ordNo);
        $("#ordId").val(ordId);
        $('input[name="ttlAmt"]').val(parseInt(ttlAmt));
    });

    $("#invoiceType").on("change", function(){
        var val = $(this).val();

        if(val === "a3") {
            $(".a3").show();
        }
        else {
            $(".a3").hide();
        }

    });

    $("#payForm").on("submit", function(){
        $(this).prop("disabled", true);
        var invoiceType        = $("#invoiceType").val();
        var customerIdentifier = $("#customerIdentifier").val();
        var customerTitle      = $("#customerTitle").val();
        var payWay = $("#payWay").val();
        var ttlAmt = $('input[name="ttlAmt"]').val();

        if(invoiceType == "a3") {
            if(customerIdentifier == "" || customerTitle == "") {
                alert("三聯式發票，需要填寫「統一編號」與「發票抬頭」");
                return false;
            }
        }
        
        $this = this;
        if(payWay == "Credit") {
			
			$(".btnPay").text("與信用卡中心連結中...請勿重新整理");
			if($("input[name='useQuickPay']").val() == "YES") {
				payProcess();
				this.submit();	
			}
			else {
				TPDirect.card.getPrime(function(result) {
					if(result.status == 0) {
						$("input[name='prime']").val(result.card.prime);
						$("input[name='agreePayRemeber']").val($("input[name='creditRemember']:checked").val());
						payProcess();
						$this.submit();
					}
					else {
						alert('請完整填寫卡號、月/年、CVC(卡片背面三碼)');
						return false;
					}
					
				});
			}
		}
		else if(payWay == "GooglePay") {
			//googlePay();
			
            TPDirect.googlePay.getPrime(function(err, prime){
				if(prime != null) {
					$("input[name='prime']").val(prime);
					payProcess();
					$this.submit();
				}
				else {
					alert("無法使用Google Pay");
					return false;
				}
            });
		}
		else if(payWay == "LinePay") {
			TPDirect.linePay.getPrime(function(result) {
				if(result.prime != null) {
					$("input[name='prime']").val(result.prime);
					payProcess();
					$this.submit();
				}
				else {
					alert("無法使用Line Pay");
					return false;
				}
			})
		}
		else if(payWay == "SamsungPay") {
			samsungPay(ttlAmt);
			TPDirect.samsungPay.getPrime(function (result) {
				if (result.status !== 0) {
					return console.log('getPrime failed: ' + result.msg)
				}
			
				// 把 prime 傳到您的 server，並使用 Pay by Prime API 付款
				var prime = result.prime;
				$("input[name='prime']").val(result.prime);
				payProcess();
				$this.submit();
			})
		}
		else if(payWay == "ApplePay") {
			if(TPDirect.paymentRequestApi.checkAvailability()) {
				applePay(ttlAmt);

				TPDirect.paymentRequestApi.getPrime(function(result) {
					console.log('paymentRequestApi.getPrime result', result);
					var prime = result.prime;

					$("input[name='prime']").val(result.prime);
					payProcess();
					$this.submit();
				});

				return false;
			}
			else {
				alert("您的瀏覽器不支援Apple Pay");
				return false;
			}
		}
		else {
			payProcess();
			this.submit();
		}
		

		return false;
    });

    $("#dlv_city").on("change", function(){
        var val = $(this).val();

        $.get(BASE_URL + '/getAreaByCity', {'dlv_city': val}, function(data){
            if(data.status == "success") {
                var areaData = data.areaData;
                var str = '<option value="" selected>請選擇鄉鎮市區</option>';
                for(i in areaData) {
                    str += '<option zip="'+areaData[i]["zip_f"]+'" value="'+areaData[i]["dist_nm"]+'">'+areaData[i]["dist_nm"]+'</option>';
                }

                $("#dlv_area").html(str);
            }
        });
    });


    $("#dlv_area").on("change", function(){
        var zip = $("#dlv_area option:selected").attr("zip");
        $("#dlv_zip").val(zip);
    });

    $(".reSendSn").on("click", function(){
        $("#snModal").modal('show');
        var orderDetailId = $(this).attr('order-detail-id');
        var ordNo = $(this).attr("ordNo");

        $("#snOrdNo").text(ordNo);

        $.get(BASE_URL + '/getSn/' + orderDetailId, function(data){
            if(data.status == "success") {
                var sn = "";
                for(var i=0; i < data.sn.length; i++) {
                    sn += data.sn[i] + "\n";
                }
                $("#sn").val(sn);
                return;
            }
        }, 'JSON');
	});
	
	$(".searchLink").click(function(){
		$("#searchForm").submit();
	});
});

$(function(){
    $("#payWay").on("change", function(){
		var val = $(this).val();

		if(val == "Credit" || val == "Installment") {

            showCreditCardArea();

            if(val == "Installment") {
                showInstallmentArea();
            }
            else {
                hideInstallmentArea();
            }
        }
		else {
			hideCreditCardArea();
		}
	});
});

function showCreditCardArea() {
    var hasToken = $('input[name="oldQuickPay"]').val();
    if(hasToken == "YES") {
        $('input[name="useQuickPay"]').val("YES");
        $("#quickPayment").show();
    }
    else {
        $(".creditCardArea").show();
    }
    
    $(".btnPay").addClass("no_function");
}

function showInstallmentArea() {
    $('.installmentArea').show();
}

function hideInstallmentArea() {
    $('.installmentArea').hide();
}

function hideCreditCardArea() {
    $(".creditCardArea").hide();
    $("#quickPayment").hide();
    $("input[name='useQuickPay']").val("NO");
    $(".btnPay").removeClass("no_function");
}

$(function(){
    $("#btnChangeCard").on("click", function(){
		$(".creditCardArea").show();
		$("#quickPayment").hide();
		$("#btnPay").prop("disabled", true);
		$("input[name='useQuickPay']").val("NO");
	});
});

function payProcess() {
    Swal.fire({
        type: 'info',
        title: '付款連線中...請勿關掉或重新整理',
        showConfirmButton: false,
        allowOutsideClick: false
    });
}