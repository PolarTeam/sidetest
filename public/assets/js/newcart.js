var PERIODS = [];
var SHIP_FEE = [];
try {
    SHIP_FEE = JSON.parse($("#shipFeeArray").val());
}
catch(e) {

}

var periodsHidden = $("#periods-hidden").val();
if(periodsHidden != "") {
    try {
        PERIODS = JSON.parse(periodsHidden);
    }
    catch(e) {

    }
}
var numberWithCommas = function (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
$(function () {
    $('html,body').animate({
        scrollTop: $('div.product_info').eq($('div.product_info').length - 1).offset().top
    }, 900);
    
    $('.go00').click(function () {
        $('html,body').animate({
            scrollTop: 0
        }, 900);
        $('.go01').click(function () {
    });
        $('html,body').animate({
            scrollTop: $('#goThe01').offset().top
        }, 800);
    });
    $('.inside_go01').click(function () {
        $('html,body').animate({
            scrollTop: $('#inside_goto_01').offset().top
        }, 800);
    });
    return false;
});

$(function(){
	$('a.goto_step1').click(function(){
		$('#fill_info').hide();
		$('#pay_info').hide();
        $('#buy_list').show();
        $("#payForm").hide();
		$('html,body').animate({scrollTop:0},900);
		$('#top_step ul.contents_box').find('li').removeClass('btn_in');
        $('#top_step ul.contents_box').find('li.step-1').addClass('btn_in');
        
        $('.stepbtn1').show();
        $('.stepbtn2').hide();
        $('.stepbtn3').hide();

        $('.btnPay').removeClass('no_function');
        //$('.btnPay').addClass('no_function');
        $('a.mobile_goto_step2').show();
        $('a.mobileBtnPay').hide();
        $('a.mobile_goto_step3').hide();
	});
	return false;
});

$(function () {
    $('a.goto_step2').click(function () {
        $('#buy_list').hide();
        $('#pay_info').hide();
        $("#payForm").show();
        $('#fill_info').show();
        $('html,body').animate({
            scrollTop: $('.fill_info').offset().top
        }, 900);
        $('#top_step ul.contents_box').find('li').removeClass('btn_in');
        $('#top_step ul.contents_box').find('li.step-2').addClass('btn_in');
        $('.stepbtn1').hide();
        $('.stepbtn2').show();
        $('.stepbtn3').hide();

        $('.btnPay').removeClass('no_function');
        //$('.btnPay').addClass('no_function');
        $('a.mobile_goto_step2').hide();
        $('a.mobileBtnPay').hide();
        $('a.mobile_goto_step3').show();
    });
    return false;
});

$(function () {
    $('a.goto_step3').click(function () {
        var chk = chkForm();

        if(chk == false) {
            return false;
        }
        $('#buy_list').hide();
        $('#fill_info').hide();
        $('#pay_info').show();
        $('html,body').animate({
            scrollTop: $('.pay_info').offset().top
        }, 900);
        $('#top_step ul.contents_box').find('li').removeClass('btn_in');
        $('#top_step ul.contents_box').find('li.step-3').addClass('btn_in');
        $('.stepbtn1').hide();
        $('.stepbtn2').hide();
        $('.stepbtn3').show();

        $('.btnPay').removeClass('no_function');

        $('a.mobile_goto_step3').hide();
        $('a.mobileBtnPay').show();
    });
    return false;
});

$(function () {
    $('.the_switch a.show_info').click(function () {
        $(this).parent().removeClass('the_switch');
        $(this).hide();
        $(this).parent().find('a.hide_info').show();
    });
    return false;
});

$(function () {
    $('a.hide_info').click(function () {
        $(this).parent().addClass('the_switch');
        $(this).hide();
        $(this).parent().find('a.show_info').show();
    });
    return false;
});

//改變取貨方式
$(function(){
    $('#pickWay').on('change', function(){
        updateCart();

        var val = $(this).val();

        if(val == 'UNIMARTC2C' || val == 'FAMIC2C' || val == 'HILIFEC2C') {
            $('.trCvsMap').show();
            $(".trAddr").hide();
        }
        else {
            $('.trCvsMap').hide();
            $(".trAddr").show();
        }

        if(val == 'A') {
            $("#pickWayForAirport").show();
            $(".trAddr").hide();
        }
        else {
            $("#pickWayForAirport").hide();
        }

        $("#CVSStoreID").val("");
        $("#CVSStoreName").val("");
        $("#CVSAddress").val("");
    });
});

//改變還貨方式
$(function(){
    $('#returnWay').on('change', function(){
        updateCart();

        if($(this).val() == 'A') {
            $('#returnWayForAirport').show();
        }
        else {
            $('#returnWayForAirport').hide();
        }
    });
});

$(function () {
    $("#openMap").on("click", function () {
        var pickWay = $("#pickWay").val();
        popup = window.open(BASE_URL + '/map/choose/N' + '/' + pickWay, '_blank', 'width=1003,height=606');

    });
});
$("#userPhone").on("change", function () {
    var userPhone = $("#userPhone").val();
　　var re = /^[0-9]+$/;//判斷字串是否為數字//判斷正整數/[1−9] [0−9]∗]∗/ 
　　if (!re.test(userPhone)) { 
    alert('手機號碼資料有誤 ! 正確格式如：0909123456');
    $("#userPhone").val('');
    return ;
　　} 
});
function setData(CVSStoreID, CVSStoreName, CVSAddress) {
    $("#CVSStoreID").val(CVSStoreID);
    $("#CVSStoreName").val(CVSStoreName);
    $("#CVSAddress").val(CVSAddress);
}

var CART_CONTENT = null;
var googlePaySetting = {
	// Optional in sandbox, Required in production
	googleMerchantId: "1251-5071-2398",
	tappayGoogleMerchantId: "standardinfomation_CTBC",
	allowedCardAuthMethods: ["PAN_ONLY", "CRYPTOGRAM_3DS"],
	merchantName: "TapPay Test!",
	emailRequired: true, // optional
	shippingAddressRequired: true, // optional,
	billingAddressRequired: true, // optional
	billingAddressFormat: "MIN", // FULL, MIN

	allowPrepaidCards: true,
	allowedCountryCodes: ['TW'],

	phoneNumberRequired: true // optional
}

TPDirect.googlePay.setupGooglePay(googlePaySetting);  

var paymentRequest = {
	allowedNetworks: ["AMEX", "JCB", "MASTERCARD", "VISA"],
	price: "1", // optional
	currency: "TWD", // optional
}

TPDirect.googlePay.setupPaymentRequest(paymentRequest, function(err, result){
	console.log(result);
	if (result.canUseGooglePay) {
		canUseGooglePay = true
	}
});

function samsungPay(amt) {
	TPDirect.samsungPay.setup({
		country_code: 'tw'
	});

	var paymentRequest = {
		supportedNetworks: ['AMEX', 'MASTERCARD', 'VISA'],
		total: {
			label: 'TapPay',
			amount: {
				currency: 'TWD',
				value: String(amt)
			}
		}
	}
	TPDirect.samsungPay.setupPaymentRequest(paymentRequest);
}

function applePay(amt) {
	var data = {
		supportedNetworks: ['MASTERCARD', 'VISA', 'AMEX'],
		supportedMethods: ['apple_pay'],
		displayItems: [{
			label: 'iPhone8',
			amount: {
				currency: 'TWD',
				value: String(amt)
			}
		}],
		total: {
			label: '付給 TapPay',
			amount: {
				currency: 'TWD',
				value: String(amt)
			}
		}
	};

	TPDirect.paymentRequestApi.setupApplePay({
		// required, your apple merchant id
		// merchant.tech.cherri.global.test 是 DEMO 頁面專門使用的 merchant id
		// 如果要在自己頁面上使用 Apple Pay 請參考 Required 去申請 Apple Pay merchant id
		merchantIdentifier: 'merchant.com.standardinformation',
		// defaults to 'TW'
		countryCode: 'TW'
	});

	TPDirect.paymentRequestApi.setupPaymentRequest(data, function (result) {
		console.log('TPDirect.paymentRequestApi.setupPaymentRequest.result', result)
	
		// 代表瀏覽器支援 payment request api (或 apple pay)
		// 和 TPDirect.paymentRequestApi.checkAvailability() 的結果是一樣的
		// if (!result.browserSupportPaymentRequest) {
		//     return
		// }
	
		// 代表使用者是否有符合 supportedNetworks 與 supportedMethods 的卡片
		// paymentRequestApi ---> canMakePaymentWithActiveCard is result of canMakePayment
		// apple pay         ---> canMakePaymentWithActiveCard is result of canMakePaymentsWithActiveCard

		if(result.browserSupportPaymentRequest == false) {
			alert("您的系統不支援ApplePay");
			return;
		}
		
		// NOTE: apple pay 只會檢查使用者是否有在 apple pay 裡面綁卡片
		if (result.canMakePaymentWithActiveCard) {

		}
		else {
			// 如果有支援 basic-card 方式，仍然可以開啟 payment request sheet
			// 如果是 apple pay，會引導使用者去 apple pay 綁卡片
			alert("沒有可以支付的卡片");
			return;
		}
	
		if (window.ApplePaySession) {
			
		}
		
	});
}

var defaultCardViewStyle = {
    color: 'rgb(0,0,0)',
    fontSize: '15px',
    lineHeight: '24px',
    fontWeight: '300',
    errorColor: 'red',
    placeholderColor: ''
}

TPDirect.card.setup('#tappay-iframe', defaultCardViewStyle)
TPDirect.card.onUpdate(function (update) {
    var submitButton = document.querySelector('.btnPay')
    var cardViewContainer = document.querySelector('#tappay-iframe')
    if (update.canGetPrime) {
        //$(".btnPay").prop("disabled", false);
        $(".btnPay").removeClass("no_function");
    } else {
        //alert("請檢查信用卡資訊是否有填完整？(月/年、CVC)");
        //$(".btnPay").addClass("no_function");
    }
});

$(function(){
    var cartStr = $("#cartStr").val();
	if (cartStr != "") {
		CART_CONTENT = JSON.parse(cartStr);
	}
});

//刪除購物車
$(function(){
	$(".cartDel").on("click", function () {
		var $thisTable = $(this);
		var uuid = $(this).attr("uuid");
		var sellType = $(this).attr("sell-type");

		$.post(BASE_URL + '/delCart', {
			'uuid': uuid,
			'sellType': sellType
		},function (data) {
			if (data.status == "success") {
				$(".header-num-box").text(data.cartNum);

				$thisTable.parents("li").remove();
				CART_CONTENT = data.cartContent;

				var num = data.cartContent['rData'].length + data.cartContent['sData'].length;

				var prodType = [];
				prodType['S'] = 0;
				prodType['N'] = 0;
				prodType['W'] = 0;

				for (var i = 0; i < data.cartContent['sData'].length; i++) {
					if (data.cartContent['sData'][i]['prodType'] == 'S') {
						prodType['S']++;
					}

					if (data.cartContent['sData'][i]['prodType'] == 'N') {
						prodType['N']++;
					}
				}

				for (var i = 0; i < data.cartContent['rData'].length; i++) {
					prodType['W']++;
				}

				if (data.cartContent['rData'].length == 0) {
					$("#pickWayForAirport").hide();
					$("div[name='returnTr']").hide();

					if (prodType['N'] > 0 && prodType['S'] < 0) {
						$("#pickWay").val("");
					}
                    
                    $("#pickWay").val("B");
                    $("#pickWay").find("option[value='A']").hide();

					$("#rentDiscountCode").val("");
					$("#rentDiscountCode");

					$("#returnWay").val("");
				} else {
					$("div[name='returnTr']").show();
					$("#rentDiscountCode");
				}

				if (data.cartContent['sData'].length == 0) {
					$("#sellArea").hide();
					$("#pickWayForAirport").show();

					$("#sellDiscountCode").val("");
					$("#sellDiscountCode");
				} else {
					$("#pickWayForAirport").hide();
					$("#sellDiscountCode");
				}

				if (data.cartContent['sData'].length == 0 && data.cartContent['rData'] == 0) {
					$("#totalBox").hide();
					$("#showDescp").show();
					$(".invoice-box").hide();
					$(".policy").hide();
					$("#quickPayment").hide();
					$("#btnPay").hide();
				}

				if (num == 0) {
					$("#payForm").hide();
				}

				if (prodType['N'] == 0 && prodType['W'] > 0) {
					$("#pickWayForAirport").show();
				} else if (prodType['S'] > 0 && prodType['N'] == 0 && prodType['W'] == 0) {
                    $("#pickWay").val("");
					$("#pickWayArea").hide();
				} else if (prodType['N'] > 0 || prodType['W'] > 0) {
					$("#pickWayArea").show();
				} else {
					$("#pickWayForAirport").hide();
				}

				if(prodType['N'] == 0 && prodType['W'] == 0 && prodType['S'] > 0) {
					$(".trAddr").hide();

                }
                
                if(prodType['N'] == 0 && prodType['W'] == 0 && prodType['S'] == 0) {
					location.reload();
                }

                updateShipWay();

				updateCart();
			}
		});
	});
});

//取得會員資料
$(function(){
    $.get(BASE_URL + '/getUserData', {}, function (data) {
        $("#userName").val(data.name);
        $("#userPhone").val(data.cellphone);
        $("#dlvZip").val(data.dlvZip);
        $("#dlvCity").val(data.dlvCity);
        $("#dlvArea").val(data.dlvArea);
        $("#dlvAddr").val(data.dlvAddr);
        $("#email").val(data.email);
    
        var areaData = data.areaData;
        var str = '<option value="" selected>請選擇鄉鎮市區</option>';
        var sel = "";
        for (i in areaData) {
            if (areaData[i]["dist_nm"] == data.dlvArea) {
                sel = "selected";
            } else {
                sel = ""
            }
            str += '<option zip="' + areaData[i]["zip_f"] + '" value="' + areaData[i]["dist_nm"] + '" ' + sel + '>' + areaData[i]["dist_nm"] + '</option>';
        }
    
        $("#dlvArea").html(str);
    
    }, "JSON");
});


//改變城市連動區域
$(function(){
    $("#dlvCity").on("change", function () {
		var val = $(this).val();

		$.get(BASE_URL + '/getAreaByCity', {
			'dlv_city': val
		}, function (data) {
			if (data.status == "success") {
				var areaData = data.areaData;
				var str = '<option value="" selected>請選擇鄉鎮市區</option>';
				for (i in areaData) {
					if (dlvArea == areaData[i]["dist_nm"]) {
						str += '<option zip="' + areaData[i]["zip_f"] + '" value="' + areaData[i]["dist_nm"] + '" selected>' + areaData[i]["dist_nm"] + '</option>';
					} else {
						str += '<option zip="' + areaData[i]["zip_f"] + '" value="' + areaData[i]["dist_nm"] + '">' + areaData[i]["dist_nm"] + '</option>';
					}

				}

				$("#dlvArea").html(str);
			}
		});
    });
    
    $("#dlvArea").on("change", function () {
		var zip = $("#dlvArea option:selected").attr("zip");
		$("#dlvZip").val(zip);
	});
});

//改變付款方式時
$(function(){
    $("#payWay").on("change", function(){
		var val = $(this).val();
        var ttlAmt = parseInt($("input[name='ttlAmt']").val());
        var cvsAmt = parseInt($("input[name='cvsAmt']").val());
        var addcvspay = $("input[name='checkisCVS']").val();
		if(val == "Credit" || val == "Instalment") {

            showCreditCardArea();

            if(val == "Instalment") {
                showInstalmentArea();
                $("#periodsselect").trigger("change");
                if( $('#periods').val()==""){
                    $('#periods').val(PERIODS[Object.keys(PERIODS)[0]].periods);
                    $('#periodsselect').val(PERIODS[Object.keys(PERIODS)[0]].cd);
                }
            }
            else {
                hideInstalmentArea();
                $("#periodsselect").trigger("change");
            }
        }
		else {
            hideCreditCardArea();
            hideInstalmentArea();
        }
        if(val == "CVS") {
            $('.CVS-note').show();
            $("input[name='checkisCVS']").val('Y');
            addcvspay = "Y";
        }
        else {
            $('.CVS-note').hide();
        }
        if(val == "ApplePay") {
            $('.apple-note').show();
        }
        else {
            $('.apple-note').hide();
        }
        console.log("nick");
        if(val == "CVS" && addcvspay =="Y"){
            $("input[name='ttlAmt']").val(ttlAmt+cvsAmt);
            $(".ttlAmt").text(numberWithCommas(ttlAmt+cvsAmt));
        }else if(val != "CVS" && addcvspay =="Y"){
            $("input[name='ttlAmt']").val(ttlAmt-cvsAmt);
            $(".ttlAmt").text(numberWithCommas(ttlAmt-cvsAmt));
            $("input[name='checkisCVS']").val('N');
        }else{

        }
	});
});

//勾選或取消安心險
$(function(){
    $("input[name='insuranceCheckbox']").on("change", function () {
		var prodDetailId = $(this).attr('prodDetailId');
		var uuid = $(this).attr('uuid');
		var num = parseInt($(this).attr("num"));
		var day = parseInt($(this).attr("day"));
		var price = parseInt($(this).attr("price"));
		var insuranceFee = 50 * day * num;

		var ttlAmt = parseInt($("input[name='ttlAmt']").val());
		var rSumAmt = parseInt($("input[name='rSumAmt']").val());

		if ($(this).prop('checked')) {
			rSumAmt = rSumAmt + insuranceFee;
			ttlAmt = ttlAmt + insuranceFee;
			price = price + insuranceFee;

			chkInsuranceForCookie(uuid, "Y");
		} else {
			rSumAmt = rSumAmt - insuranceFee;
			ttlAmt = ttlAmt - insuranceFee;
			price = price - insuranceFee;

			chkInsuranceForCookie(uuid, "N");
		}
    });
    
    $(".chxLabel").on("click", function(){
        var id = $(this).attr("href");

        if($(id).prop("checked") == true) {
            $(id).prop("checked", false).trigger("change");
        }
        else {
            $(id).prop("checked", true).trigger("change");
        }
    });
});

$(function(){
    $('#invoiceType').on('change', function(){
        var val = $(this).val();

        switch(val) {
            case 'a1':
            case 'a2':
                $('.a3').hide();
                break;
            case 'a3':
                $('.a3').show();
                break;
        }
    });
});

//輸入優惠碼
$(function(){
    $('input[name="discount_code[]"]').on("change", async function(){
        var code = $(this).val();
        var sellType = $(this).attr('selltype');
        var uuid = $(this).attr('uuid');
        var prodDetailId = $(this).attr('prodDetailId');
        code = code.replace(/ /g, "");
        $(this).val(code);
        var discountObj = await getDiscount(code, sellType, uuid, prodDetailId);
        if(discountObj.num > 0) {
            setDiscountSuccess(uuid, discountObj.num, discountObj.discountWay);
        }
        else if(code != "") {
            setDiscountError(uuid);
        }
        else {
            setDiscountEmpty(uuid);
        }
        
    });
});

//變更期數
$("#periodsselect").on("change", function(){
    if($(this).val()) {
        $("#periods").val(PERIODS[$(this).val()]['periods']);
    }   
    
    updateCart();
});

function showCreditCardArea() {
    var hasToken = $('input[name="oldQuickPay"]').val();
    if(hasToken == "YES") {
        $('input[name="useQuickPay"]').val("YES");
        $("#quickPayment").show();
    }
    else {
        $(".creditCardArea").show();
        //$(".btnPay").addClass("no_function");
    }
}

function showInstalmentArea() {
    $('.instalmentArea').show();
}

function hideInstalmentArea() {
    $('.instalmentArea').hide();
    $('#periods').val('');
    $('#periodsselect').val('');
}

function hideCreditCardArea() {
    $(".creditCardArea").hide();
    $("#quickPayment").hide();
    $("input[name='useQuickPay']").val("NO");
    $(".btnPay").removeClass("no_function");
}


function chkInsuranceForCookie(uuid, insurance) {
    $.post(BASE_URL + '/updateInsuranceToCart', {
        'uuid': uuid,
        'insurance': insurance
    }, function (data) {
        CART_CONTENT = data.cartContent;
        updateCart();
        if (data.status != "success") {

            alert(data.message);
        }
    });
}

function updateNumForCookie(uuid, num, $elem) {
    
    $.post(BASE_URL + '/updateNumToCart', {
        'uuid': uuid,
        'num': num
    }, function (data) {
        if (data.status != "success") {
            var num = parseInt($elem.val());
            num = num - 1;
            $elem.val(num);
            alert(data.message);
        }
        else {
            CART_CONTENT = data.cartContent;
            
            updateCart();
        }
    });
}

async function updateCart() {
    //var cartObj = CART_CONTENT;
    var cartObj = await getCart();
    console.log(cartObj);
    var rAmt = 0;
    var sAmt = 0;
    var ttlAmt = 0;
    var isDiscount = false;
    var discountNum = 0;
    var discountPrice = 0;

    for (let i in cartObj["rData"]) {
        var num       = parseInt(cartObj["rData"][i].num);
        var startDate = cartObj["rData"][i].startDate;
        var endDate   = cartObj["rData"][i].endDate;
        var dPrice    = parseInt(cartObj["rData"][i].dPrice);
        var sDT       = new Date(startDate);
        var eDT       = new Date(endDate);
        var day       = sDT.dateDiff("d", eDT) + 1;
        var insurance = cartObj["rData"][i].insurance;
        var uuid      = cartObj["rData"][i].uuid;
        var thisAmt   = 0;
        if (day >= 5) {
            isDiscount = true;
            for (var j = 0; j < num; j++) {
                discountNum++;
            }
        }

        //rAmt = rAmt + (num * day * dPrice);
        thisAmt = num * day * dPrice;
        if (insurance == "Y") {
            //rAmt = rAmt + (50 * day * num);
            thisAmt = thisAmt + (50 * day * num);
        }

        var dObj = await getDiscount(cartObj["rData"][i]["discountCode"], "R", cartObj["rData"][i]["uuid"], cartObj["rData"][i]["prodDetailId"]);

        updateDiscount(dObj, thisAmt, cartObj["rData"][i]["uuid"]);

        thisAmt = calDiscountByItem(uuid, thisAmt);
        rAmt = rAmt + thisAmt;

        $("text[uuid='" + uuid + "']").text(numberWithCommas(thisAmt));
    }



    var discountObj = {
        '11': 0,
        '10': 0,
    };
    for (i in cartObj["sData"]) {
        var prodId = cartObj["sData"][i].prodId;
        var num    = parseInt(cartObj["sData"][i].num);
        
        if (prodId == 11) {
            discountObj['11'] = discountObj['11'] + num;
        }

        if (prodId == 10) {
            discountObj['10'] = discountObj['10'] + num;
        }
    }

    for (i in cartObj["sData"]) {
        var num = parseInt(cartObj["sData"][i].num);
        var dPrice = parseInt(cartObj["sData"][i].dPrice);
        var uuid   = cartObj["sData"][i]["uuid"];
        var thisAmt   = 0;

        thisAmt = num * dPrice;

        var dObj = await getDiscount(cartObj["sData"][i]["discountCode"], "S", cartObj["sData"][i]["uuid"], cartObj["sData"][i]["prodDetailId"]);

        updateDiscount(dObj, thisAmt, cartObj["sData"][i]["uuid"]);

        thisAmt = calDiscountByItem(uuid, thisAmt);
        sAmt = sAmt + thisAmt;
        
        $("text[uuid='" + uuid + "']").text(numberWithCommas(thisAmt));
    }

    var disSum = discountObj['11'] + discountObj['10'];
    for (var j = 0; j < discountObj['11']; j++) {
        if (isDiscount === true) {
            if (discountNum > 0 && disSum > 0) {
                discountPrice = discountPrice + 980;
                discountNum--;
                disSum--;
            }
        }
    }

    for (var j = 0; j < discountObj['10']; j++) {
        if (isDiscount === true) {
            if (discountNum > 0 && disSum > 0) {
                discountPrice = discountPrice + 1380;
                discountNum--;
                disSum--;
            }
        }
    }


    ttlAmt = rAmt + sAmt;

    ttlAmt = ttlAmt - discountPrice;

    var cartObj = CART_CONTENT;

    var shipFee = getShipFee();
    const shipFree = parseInt($("input[name='shipFree']").val());
    if(ttlAmt >= shipFree) {
        shipFee = 0;
    }
    ttlAmt += shipFee;

    $("input[name='shipFee']").val(shipFee);
    $(".shipFee").text(numberWithCommas(shipFee));

    
    const oTtlAmt = ttlAmt;
    if($("#payWay").val() == "Instalment") {
        ttlAmt = updateTtlAmtForInstalment(ttlAmt);
    }

    $("input[name='ttlAmt']").val(ttlAmt);
    $(".ttlAmt").text(numberWithCommas(ttlAmt));

    calTtlDiscount();
    updateInstalmentList(oTtlAmt);
    //$("#discountPrice").text(numberWithCommas(discountPrice))
}

function updateTtlAmtForInstalment(ttlAmt) {
    var rate = parseFloat(PERIODS[$("#periodsselect").val()]['value'] / 100);
    
    return ttlAmt = ttlAmt + Math.round(ttlAmt * rate);

}

function calDiscountByItem(uuid, thisAmt) {
    var itemDiscountAmt = parseInt($("span[name='dicountSubTotal'][uuid='"+uuid+"']").text());

    if(itemDiscountAmt > 0) {
        if( thisAmt - itemDiscountAmt < 0) {
            $('span[name="dicountSubTotal"][uuid="'+uuid+'"]').text(0);
            return thisAmt;
        }
        return thisAmt -= itemDiscountAmt;
    }    

    return thisAmt;
}

//計算總優惠金額
function calTtlDiscount() {
    var ttlDiscountAmt = 0;
    $("span[name='dicountSubTotal']").each(function(){
        ttlDiscountAmt = ttlDiscountAmt + parseInt($(this).text());
    });

    $('.discountCodePrice').text(ttlDiscountAmt);
}

function getShipFee() {
    var pickWay   = $("#pickWay").val();
    var returnWay = $('#returnWay').val();

    var amt = 0;
    
    // if(typeof pickWay != 'undefined' && $("#pickWayArea").css('display') != "none") {
    //     const dataset1 = await $.get( BASE_URL + "/shipfee/get/" + pickWay);
    //     const dataset2 = await $.get( BASE_URL + "/shipfee/get/" + returnWay);
    
    //     return dataset1.amt + dataset2.amt;
    // }

    const amt1 = typeof SHIP_FEE[pickWay] == 'number' ? SHIP_FEE[pickWay] : 0;
    const amt2 = typeof SHIP_FEE[returnWay] == 'number' ? SHIP_FEE[returnWay] : 0;

    amt = amt1 + amt2;

    return amt;
    
}

async function getDiscount(code, sellType, uuid, prodDetailId) {
    var dataset = {
        'num': 0,
        'discountWay': null
    };
    
    try {
        dataset = await $.post(BASE_URL + "/getDiscountCode", {'code': code, 'sellType': sellType, 'uuid': uuid, 'prodDetailId': prodDetailId});
    }
    catch(e) {
        console.log('error');
    }
    

    //console.log(dataset);

    return {'num':dataset.num, 'discountWay': dataset.discountWay};
}

async function getShipWayFromCart() {
    const dataset = await $.get(BASE_URL + "/get/shipway", {});

    return dataset.shipWay;
}

async function updateShipWay() {
    var shipWayArray = await getShipWayFromCart();
    var str = "";

    for(var i=0; i<shipWayArray.length; i++) {
        str += '<option value="'+shipWayArray[i]['key']+'">'+shipWayArray[i]['value']+'</option>';
    }

    $('#pickWay').html(str);

    if($('#pickWay').val() == "A") {
        $('#pickWayForAirport').show();
    }
    else {
        $('#pickWayForAirport').hide();
    }
}


Date.prototype.dateDiff = function (interval, objDate) {
    var dtEnd = new Date(objDate);
    if (isNaN(dtEnd)) return undefined;
    switch (interval) {
        case "s":
            return parseInt((dtEnd - this) / 1000);
        case "n":
            return parseInt((dtEnd - this) / 60000);
        case "h":
            return parseInt((dtEnd - this) / 3600000);
        case "d":
            return parseInt((dtEnd - this) / 86400000);
        case "w":
            return parseInt((dtEnd - this) / (86400000 * 7));
        case "m":
            return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - this.getFullYear()) * 12) - (this.getMonth() + 1);
        case "y":
            return dtEnd.getFullYear() - this.getFullYear();
    }
}

$(function(){
    updateCart();
});

function setDiscountSuccess(uuid, discountAmt, discountWay) {
    var oldAmt = parseInt($('text[uuid="'+uuid+'"]').text());
    $('input[name="discount_code[]"][uuid="'+uuid+'"]').removeClass('error');
    $('input[name="discount_code[]"][uuid="'+uuid+'"]').addClass('correct');
    $('.code_note[uuid="'+uuid+'"]').find('span').text('已成功折抵優惠碼');
    $('.code_note[uuid="'+uuid+'"]').show();

    var amt = 0;

    if(discountWay == "C") {
        amt = discountAmt;
    }
    else if(discountWay == "P") {
        amt = oldAmt - (Math.round(oldAmt * (discountAmt/100)));
    }

    $('span[name="dicountSubTotal"][uuid="'+uuid+'"]').text(amt);

    updateCart();
}

function setDiscountError(uuid) {
    $('input[name="discount_code[]"][uuid="'+uuid+'"]').removeClass('correct');
    $('input[name="discount_code[]"][uuid="'+uuid+'"]').addClass('error');
    $('input[name="discount_code[]"][uuid="'+uuid+'"]').val('');
    $('.code_note[uuid="'+uuid+'"]').find('span').text('您的優惠碼無效或已過期或商品金額未達折扣標準');
    $('.code_note[uuid="'+uuid+'"]').fadeIn(500).delay(1500).fadeOut(500);  
    
    $('span[name="dicountSubTotal"][uuid="'+uuid+'"]').text(0);
    updateCart();
}

function setDiscountEmpty(uuid) {
    $('input[name="discount_code[]"][uuid="'+uuid+'"]').removeClass('error');
    $('input[name="discount_code[]"][uuid="'+uuid+'"]').removeClass('correct');
    $('.code_note[uuid="'+uuid+'"]').hide();

    $('span[name="dicountSubTotal"][uuid="'+uuid+'"]').text(0);
    updateCart();
}

$(function() {
    $('.addBtn').on("click", function(){
        var num = parseInt($(this).siblings('input[name="num[]"]').val());
        var uuid = $(this).siblings('input[name="num[]"]').attr('uuid');
        num = num + 1;
        if(num > 10) {
            alert("一筆數量不能超過10個");
            return;
        }
        $(this).siblings('input[name="num[]"]').val(num);
        updateNumForCookie(uuid, num, $(this).siblings('input[name="num[]"]'));

        $(this).prop('disabled', true);
        $('.lessBtn').prop('disabled', true);
        setTimeout(function(){
            $('.addBtn').prop('disabled', false);
            $('.lessBtn').prop('disabled', false);
        },1000);
    });
});

$(function() {
    $('.lessBtn').on("click", function(){
        var num = parseInt($(this).siblings('input[name="num[]"]').val());
        var uuid = $(this).siblings('input[name="num[]"]').attr('uuid');
        num = num - 1;

        if(num < 0 || num == 0) {
            num = 1;
        }
        $(this).siblings('input[name="num[]"]').val(num);
        updateNumForCookie(uuid, num, $(this).siblings('input[name="num[]"]'));
        $(this).prop('disabled', true);
        $('.addBtn').prop('disabled', true);
        setTimeout(function(){
            $('.lessBtn').prop('disabled', false);
            $('.addBtn').prop('disabled', false);
        },1000);
    });
});

$(function(){
    $('.btnPay').on('click', function(){
        $('#payForm').submit();
    });
});

$(function(){
    $('#payForm').on('submit', function(){
        var payWay = $("#payWay").val();
        var ttlAmt = parseInt($("input[name='ttlAmt']").val());
		if ($("#policy").prop("checked") == false) {
			alert("請勾選同意隱私權政策");
			return false;
		}

		if ($("#agreeReturn").prop("checked") == false) {
			alert("請勾選同意退貨條款");
			return false;
        }
        
        $("#refreshed").val("yes");

		$this = this;
        $(".btnPay").addClass("no_function");
        
        if (payWay == "CVS") {
			if (ttlAmt >= 20000) {
				alert("超商付款金額不能大於兩萬元");
				return false;
			}
		}
		
		if(payWay == "Credit" || payWay == "Instalment") {
			
			//$(".btnPay").text("與信用卡中心連結中...請勿重新整理");
			if($("input[name='useQuickPay']").val() == "YES") {
                payProcess();
				this.submit();	
			}
			else {
				TPDirect.card.getPrime(function(result) {
					if(result.status == 0) {
						$("input[name='prime']").val(result.card.prime);
                        $("input[name='agreePayRemeber']").val($("input[name='creditRemember']:checked").val());
                        payProcess();
						$this.submit();
					}
					else {
                        alert('請完整填寫卡號、月/年、CVC(卡片背面三碼)');
                        return false;
					}
					
				});
			}
		}
		else if(payWay == "GooglePay") {
			//googlePay();
			
            TPDirect.googlePay.getPrime(function(err, prime){
				if(prime != null) {
                    $("input[name='prime']").val(prime);
                    payProcess();
					$this.submit();
				}
				else {
					alert("無法使用Google Pay");
					return false;
				}
            });
		}
		else if(payWay == "LinePay") {
			TPDirect.linePay.getPrime(function(result) {
				if(result.prime != null) {
                    $("input[name='prime']").val(result.prime);
                    payProcess();
					$this.submit();
				}
				else {
					alert("無法使用Line Pay");
					return false;
				}
			})
		}
		else if(payWay == "SamsungPay") {
			samsungPay(ttlAmt);
			TPDirect.samsungPay.getPrime(function (result) {
				if (result.status !== 0) {
					return console.log('getPrime failed: ' + result.msg)
				}
			
				// 把 prime 傳到您的 server，並使用 Pay by Prime API 付款
				var prime = result.prime;
                $("input[name='prime']").val(result.prime);
                payProcess();
				$this.submit();
			})
		}
		else if(payWay == "ApplePay") {
			if(TPDirect.paymentRequestApi.checkAvailability()) {
				applePay(ttlAmt);

				TPDirect.paymentRequestApi.getPrime(function(result) {
					console.log('paymentRequestApi.getPrime result', result);
					var prime = result.prime;

                    $("input[name='prime']").val(result.prime);
                    payProcess();
					$this.submit();
				});

				return false;
			}
			else {
				alert("您的瀏覽器不支援Apple Pay");
				return false;
			}
		}
		else {
            payProcess();
			this.submit();
		}
		

		return false;
        
    });
});

$(function(){
    $("#btnChangeCard").on("click", function(){
		$(".creditCardArea").show();
		$("#quickPayment").hide();
		$("#btnPay").prop("disabled", true);
        $("input[name='useQuickPay']").val("NO");
	});
});

function chkForm() {
    var returnWay = $("select[name='returnWay']").val();
    var pickWay = $("select[name='pickWay']").val();
    var userName = $("#userName").val();
    var userPhone = $("#userPhone").val();
    var email = $("#email").val();
    var dlvAddr = $("#dlvAddr").val();
    var invoiceType = $("select[name='invoiceType']").val();
    var ttlAmt = parseInt($("input[name='ttlAmt']").val());
    var payWay = $("#payWay").val();
    var dlvCity = $("#dlvCity").val();
    var dlvArea = $("#dlvArea").val();
    var dlvZip = $("#dlvZip").val();

    var rentNum = CART_CONTENT["rData"].length;
    if(rentNum > 0) {
        if((pickWay == "B" || pickWay == "UNIMARTC2C" || pickWay == "FAMIC2C" || pickWay == "HILIFEC2C") && returnWay != "B") {
            alert("宅配取機，只能宅配還機");
            return false;
        }
    }

    if(rentNum == 0) {
        $("select[name='returnWay']").val("");
        returnWay = "";
    }

    var phoneLen = chkPhone(pickWay, userPhone);
    if(phoneLen != "") {
        alert(phoneLen);
        return false;
    }

    if (!validateEmail(email)) {
        alert('email格式不正確，請檢查！！！');
        return false;
    }

    if($(".trAddr").css("display") != "none") {
        if (userName == "" || userPhone == "" || dlvAddr == "" || email == "" || dlvZip == "" || dlvArea == "" || dlvCity == "") {
            alert("請檢查姓名、手機、郵地區號、城市、區域、住址、email是否有正確填寫");
            return false;
        }
    }
    else {
        if (userName == "" || userPhone == "" || email == "") {
            alert("請檢查姓名、手機、email是否有正確填寫");
            return false;
        }
    }

    if (pickWay == "A") {
        var departureTerminal = $("select[name='departureTerminal']").val();
        var departureHour = $("select[name='departureHour']").val();
        var departureMin = $("select[name='departureMin']").val();

        if (departureHour == "" || departureTerminal == "" || departureMin == "") {
            alert("請填寫取機航廈與航班大約時間");
            return false;
        }

        if (returnWay != "A") {
            alert("請選取「機場還貨」及填寫航廈與航班大約時間");
            return false;
        }
    }

    if (pickWay == "UNIMARTC2C" || pickWay == "FAMIC2C" || pickWay == "HILIFEC2C") {
        if($("#CVSStoreID").val() == "" || $("#CVSStoreID").val() == null) {
            alert("請選擇門市");
            return false;
        }

        if (userName.length > 10) {
            alert("超商取貨名字不可大於十個字");
            return false;
        }

        if (ttlAmt >= 20000) {
            alert("超商取貨金額不能大於兩萬元");
            return false;
        }
    }

    if (payWay == "CVS") {
        if (ttlAmt >= 20000) {
            alert("超商付款金額不能大於兩萬元");
            return false;
        }
    }
    
    if (returnWay == "A") {
        var returnTerminal = $("select[name='returnTerminal']").val();
        var returnHour = $("select[name='returnHour']").val();
        var returnMin = $("select[name='returnMin']").val();

        if (returnHour == "" || returnTerminal == "" || returnMin == "") {
            alert("請填寫航廈與航班大約時間");
            return false;
        }
    }

    if (invoiceType == "a3") {
        var customerIdentifier = $("#customerIdentifier").val();
        var customerTitle = $("#customerTitle").val();

        if (customerIdentifier == "" || customerTitle == "") {
            alert("請填寫統一編號與發票抬頭");
            return false;
        }
    }
    
    return true;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function chkPhone(pickWay, phone) {
    var msg = "";
    if(pickWay == "B") {
        if(phone.length > 20) {
            msg = "選擇宅配電話不可超過20碼";
        }
    }
    else {
        if(phone.length > 10) {
            msg = "選擇超商取貨電話只能輸09開頭的手機號碼，共10碼";
        }
    }

    return msg;
}

function payProcess() {
    Swal.fire({
        type: 'info',
        title: '付款連線中...請勿關掉或重新整理',
        showConfirmButton: false,
        closeOnClickOutside: false,
        allowOutsideClick: false
    });
}

function updateDiscount(obj, oldAmt, uuid) {
    var amt = 0;

    if(obj.discountWay == "C") {
        amt = obj.num;
    }
    else if(obj.discountWay == "P") {
        amt = oldAmt - (Math.round(oldAmt * (obj.num/100)));
    }
    else {
        $('input[name="discount_code[]"][uuid="'+uuid+'"]').val("");
        $('.code_note[uuid="'+uuid+'"]').fadeOut(500);  
    }

    $('span[name="dicountSubTotal"][uuid="'+uuid+'"]').text(amt);
}

async function getCart() {
    const dataset = await $.get(BASE_URL + "/get/cart");

    //console.log(dataset);

    return dataset;
}

function updateInstalmentList(ttlAmt) {
    const periods = $("#periods").val();
    console.log("nick");
    $.get(BASE_URL + '/updateInstalmentList', {ttlAmt: ttlAmt}, function(res){
        PERIODS = res.data;
        var count = 0;
        var str = '';
        var str_new = '';
        $.each(PERIODS, function(i, v){
            if(periods == PERIODS[i]['periods']) {
                str += '<option value="'+PERIODS[i]['cd']+'" selected>'+PERIODS[i]['cd_descp']+'</option>';
                str_new = PERIODS[i]['value3'];
                count ++;
            }
            else {
                str += '<option value="'+PERIODS[i]['cd']+'">'+PERIODS[i]['cd_descp']+'</option>';
                count++;
            }
            
        })

        $('#periodsselect').html(str);
        if(count > 0) {
            $('a[href="#bank"]').attr('bank',str_new);
        }
    }, 'JSON');

    return;
}