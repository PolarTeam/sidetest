$(document).ready(function () {
    var navbar = $('#navbar-menu-mobile').slideMenu({
        position: 'left',
        submenuLinkAfter: ' <i class="pull-right fa fa-angle-right"></i>',
        backLinkBefore: '<i class="fa fa-angle-left"></i> '
    });

    $("nav.navbar.bootsnav .attr-nav").each(function(){  
        $("li.search > a", this).on("click", function(e){
            e.preventDefault();
            $(".top-search").slideToggle();
        });
    });
    $(".input-group-addon.close-search").on("click", function(){
        $(".top-search").slideUp();
    });
    $('.dropdown-menu:not(a)').on("click.bs.dropdown", function (events) { 
        var events = $._data(document, 'events') || {};
        
        events = events.click || [];
        for(var i = 0; i < events.length; i++) {
            if(events[i].selector) {

                //Check if the clicked element matches the event selector
                if($(event.target).is(events[i].selector)) {
                    events[i].handler.call(event.target, event);
                }

                // Check if any of the clicked element parents matches the 
                // delegated event selector (Emulating propagation)
                $(event.target).parents(events[i].selector).each(function(){
                    events[i].handler.call(this, event);
                });
            }
        }
        event.stopPropagation(); //Always stop propagation
    });
    $('#navbar-menu .dropdown').on("shown.bs.dropdown", async function () { 
        const cateId = $(this).attr('cate-id');
        await getHotProductForMenu(cateId);
        //$('.responsive-slider-nav').slick('refresh');
    });
    $('.menu-col>li>a[class="multinext"], .menu-col>li>a[class="multiprev"]').click(function(){
        $(this).parent().parent().parent().parent().removeClass('show');
        $(this).parent().parent().parent().parent().addClass('noshow');
        $('#'+$(this).attr('target')).removeClass('noshow');
        $('#'+$(this).attr('target')).addClass('show');
    });

    // $('.mobile-menu').on('click', function(){
    //     alert($(this).attr('cate-id'));
    //     getMobileNextMenu($(this).attr('cate-id'), $(this));
    // })
});

function getHotProductForMenu(cateId) {
    $.get(BASE_URL + '/getHotProductForMenu/' + cateId, {}, function(res){
        const len = res.data.length;
        const data = res.data;
        var html = '';
        //$(".slick-list").remove();
        for(var i=0; i<len; i++) {
            html += '<div class="col-md-3 product-item hot-product-item">\
                        <a href="'+ BASE_URL + '/productDetail/' + data[i]['id'] +'">\
                            <div class="pro-img-box">\
                                <div class="cover"></div>\
                                <img src="/storage/'+ data[i]['img1'].replace('public/', '') +'">\
                            </div>\
                            <h6 class="pro-list-name">'+data[i]['title']+'</h6>\
                            <h6 class="pro-money"><span>TWD</span> '+parseInt(data[i]['f_price'])+'</h6>\
                        </a>\
                    </div>';
            
        }
        $('li[cate-id="'+cateId+'"]').find('li.hot-prod').html('<div class="row responsive-slider-nav hot-prod-div" id="hot-cate-'+cateId+'"></div>');
        
        $('li[cate-id="'+cateId+'"]').find('div.hot-prod-div').html(html);
        setTimeout(() => {

            $('#hot-cate-'+cateId+'').slick({
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 4,
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            });
            
        }, 300);
        
        
        const hotKey = '<li class="nav-keyword">\
                            <h5>熱門關鍵字</h5>\
                        </li>'

        //$('li[cate-id="'+cateId+'"]').find('ul.hot-prod').append(hotKey);

        const nextMenu = res.nextMenu;
        const mLen = res.nextMenu.length;
        const thisId = 'menu-' + cateId;
        var html = '';
        for(var i=0; i<mLen; i++) {
            
            const nextId = 'menu-' + nextMenu[i]['id'];
            if(nextMenu[i]['has_next'] == 'Y') {
                html += '<li class="" onclick="showNextMenu(\''+thisId+'\', \''+nextId+'\', '+nextMenu[i]['id']+',  \''+nextMenu[i]['name']+'\')"><a class="multinext" target="menu-'+nextMenu[i]['id']+'">'+nextMenu[i]['name']+'</a></li>'

                if($('#menu-' + nextMenu[i]['id']).length == 0) {
                    const next = '<div class="col-menu col-md-3 noshow" id="menu-'+nextMenu[i]['id']+'">\
                                <div class="content">\
                                    <ul class="menu-col next-menu">\
                                    </ul>\
                                </div>\
                            </div>';

                            $('li[cate-id="'+cateId+'"]').find('div.menubox').append(next);
                }
                
            }
            else {
                html += '<li class=""><a href="' + BASE_URL + '/product?cateId='+nextMenu[i]['id']+'">'+nextMenu[i]['name']+'</a></li>';
            }
        }
        $("#menu-" + cateId).find('ul.next-menu li:eq(0)').siblings().remove();
        $("#menu-" + cateId).find('ul.next-menu li:eq(0)').after(html);

        $('.pro-img-box').imagefill();
    }, 'JSON');
}

function getNextMenu(lastId, cateId, name) {
    const firtId = $('#' + lastId).parents('li[level="first"]').attr('cate-id');

    $.get(BASE_URL + '/getNextMenu/' + cateId, {}, function(res){

        const len = res.data.length;
        const nextMenu = res.data;
        const thisId = 'menu-' + cateId;
        
        if($("#menu-" + cateId).find('ul.next-menu li').length == 0) {
            $("#menu-" + cateId).find('ul.next-menu').append('<li class="" onclick="backMenu(\''+thisId+'\', \''+lastId+'\')"><a  class="multiprev" target="menu1">'+name+'</a></li>');
        }
       
        var html = '<li class=""><a href="' + BASE_URL + '/product?cateId='+cateId+'">'+'所有' + name +'</a></li>';
        for(var i=0; i<len; i++) {
            const nextId = 'menu-' + nextMenu[i]['id'];
            if(nextMenu[i]['has_next'] == 'Y') {
                html += '<li class="" onclick="showNextMenu(\''+thisId+'\', \''+nextId+'\', '+nextMenu[i]['id']+',  \''+nextMenu[i]['name']+'\')"><a class="multinext" target="menu-'+nextMenu[i]['id']+'">'+nextMenu[i]['name']+'</a></li>'

                const next = '<div class="col-menu col-md-3 noshow" id="menu-'+nextMenu[i]['id']+'">\
                                <div class="content">\
                                    <ul class="menu-col next-menu">\
                                    </ul>\
                                </div>\
                            </div>';

                //$('li.next-menu-' + cateId).append(next);
                $('li[cate-id="'+firtId+'"]').find('div.menubox').append(next);
            }
            else {
                html += '<li class=""><a href="' + BASE_URL + '/product?cateId='+nextMenu[i]['id']+'">'+nextMenu[i]['name']+'</a></li>';
            }

            //$("#menu-" + cateId).find('ul.next-menu li:eq(0)').after(html);
            
        }

        if($("#menu-" + cateId).find('ul.next-menu li').length == 1) {
            $("#menu-" + cateId).find('ul.next-menu li:eq(0)').after(html);
        }
        
    }, 'JSON');
}

async function showNextMenu(thisId, nextId, cateId, name) {
    await getNextMenu(thisId, cateId, name);
    $('#'+thisId).removeClass('show');
    $('#'+thisId).addClass('noshow');
    $('#'+ nextId).removeClass('noshow');
    $('#'+ nextId).addClass('show');
}

function backMenu(thisId, nextId) {
    $('#'+thisId).removeClass('show');
    $('#'+thisId).addClass('noshow');
    $('#'+ nextId).removeClass('noshow');
    $('#'+ nextId).addClass('show');
}

async function getMobileNextMenu(elem) {
    window.event? window.event.cancelBubble = true : e.stopPropagation();
    const cateId = $(elem).attr('cate-id');
    const $elem = $(elem);

    $.get(BASE_URL + '/getNextMenu/' + cateId, {}, function(res){
        const len = res.data.length;
        const nextMenu = res.data;
        var li = '';
        for(var i=0; i<len; i++) {

            if(nextMenu[i]['has_next'] == 'Y') {
                li += '<li class="dropdown megamenu-fw mobile-menu" cate-id="'+nextMenu[i]['id']+'" onclick="getMobileNextMenu(this)"><a href="#" class="dropdown-toggle" data-toggle="dropdown">'+nextMenu[i]['name']+'<i class="pull-right fa fa-angle-right"></i></a><ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp"><li><a href="" class="slide-menu-control" data-action="back"><i class="fa fa-angle-left"></i> 測試商品1</a></li></ul></li>'
            }
            else {
                li += '<li class="dropdown megamenu-fw mobile-menu" cate-id="'+nextMenu[i]['id']+'"><a href="#">'+nextMenu[i]['name']+'</a>/li>';
            }
        }

        if($elem.find('ul li').length == 1) {
            $elem.find('ul').append(li);
        }

        $('.slider').css("transform","translate(-200%)");
        
    }, 'JSON');
}


