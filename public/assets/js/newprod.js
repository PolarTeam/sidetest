$(function () {
    // $("#datepicker-start").datepicker({ 
    //     autoclose: true, 
    //     todayHighlight: false,
    //     startDate: '+1d',
    //     endDate: '+90d'
    // });

    // $("#datepicker-end").datepicker({ 
    //     autoclose: true, 
    //     todayHighlight: false,
    //     startDate: '+3d',
    //     endDate: '+120d'
    // });

    $("#num").on("change", function(){
        var val = $(this).val();

        if(val > 10) {
            alert("一筆數量不能超過10個");
            $(this).val(10);
        }

    });
	
    var num = 1;	// 數量
    $("#number-picker input").val(num);
    $(".add-up").on('click', function(e) {
        num++;
        if(num > 10) {
            num = 10;
            alert("一筆數量不能超過10個");
        }
        $("input[name='quantity']").val(num);        
    });

    $(".add-down").on('click', function(e) {
        if(num>1) {
            num--;
            $("input[name='quantity']").val(num);
        }

    });

    $(".prodDetailLink").on("click", function(){
        var startDate = $("#datepicker-start input[name='startDate']").val();
        var endDate = $("#datepicker-end input[name='endDate']").val();
        var instalment_rate  = $("#periods-hidden").val();
        var instalment_num  = $("#periods-hidden_num").val();
        var instalment_id  = $(".abc").length;
        var newamt = 0; 
        if(startDate == "" || endDate == "") {
            alert("請先選擇您的旅遊期間，再選擇型號");
            return;
        }
        $(this).siblings().removeClass("is-active");
        $(this).addClass("is-active");
        var oPrice = ($(this).attr("o-price") != "")?$(this).attr("o-price"):0;
        var dPrice = ($(this).attr("d-price") != "")?$(this).attr("d-price"):0;

        $(".price").text(Math.round(parseFloat(dPrice)));
        $(".o-price").text("NTD " + Math.round(parseFloat(oPrice)));
        instalment_rate  = JSON.parse(instalment_rate);
        instalment_num  = JSON.parse(instalment_num);
        for(i=0 ;i<instalment_id;i++) {
            var rate = instalment_rate[$(".abc")[i].id];
            var testnum = instalment_num[$(".abc")[i].id];
            var newamt = dPrice * (rate/100 + 1) / testnum; 
            testnode = $(".abc")[i].id;
            $("#"+testnode).text(newamt.toFixed(0));
        }
        if($(this).attr("prod-img") != "") {
            $("#prodImg").attr("src", $(this).attr("prod-img"));
        }
        
    });

    $("#addCart").on("click", function(){
        var num          = $("#num").val();
        if(num > 10) {
            $("#num").val(10);
            alert("一筆數量不能超過十個");
            return;
        }
        $.get(BASE_URL + '/chkAuth', {}, function(data){
            if(data.status == true) {
                //var cartContent = $.cookie('cartContent');
                var cartContent = data.cartContent;

                if(typeof cartContent == "undefined" || cartContent == null) {
                    cartContent = {
                        "rData": [],
                        "sData": []
                    };
                }
                else {
                    cartContent = JSON.parse(cartContent);
                }

                var $prodData = $(".prodDetailLink.is-active");

                var num          = $("#num").val();
                var prodId       = $prodData.attr("prod-id");
                var prodNo       = $prodData.attr("prod-no");
                var prodType     = $prodData.attr("prod-type");
                var prodDetailId = $prodData.attr("prod-detail-id");
                var prodNm       = $prodData.attr("prod-nm");
                var startDate    = $("#datepicker-start input[name = 'startDate']").val();
                var endDate      = $("#datepicker-end input[name='endDate']").val();
                var dPrice       = ($prodData.attr("d-price") == "")?0:$prodData.attr("d-price");
                var sellType     = $prodData.attr("sell-type");
                var prodImg      = $("#prodImg").attr('src');

                var today     = new Date();
                var sDT       = new Date(startDate);
                var eDT       = new Date(endDate);
                var day       = sDT.dateDiff("d",eDT) + 1;
                var dDay      = today.dateDiff("d", sDT) + 1;
                
                if(dDay == 1) {
                    alert("如果為隔日出國必需在機場進行取貨");
                }

                if(sellType == "R") {
                    if(day < 4) {
                        alert("租用天數必需四天以上！！");
                        return;
                    }
                }
                

                if(typeof prodDetailId == "undefined") {
                    alert("請先選擇型號，再加入購物車");
                    return;
                }

                var cartObj = {"uuid": _uuid(), "num": num, "prodId": prodId, "prodNo": prodNo, "prodType": prodType, "prodDetailId": prodDetailId, "prodNm": prodNm, "startDate": startDate, "endDate": endDate, "dPrice": dPrice, "insurance": "N", "discountCode": "", "discountAmt": 0, "prodImg": prodImg};
                if(sellType == "R") {
                    cartContent["rData"].push(cartObj);
                }
                else {
                    cartContent["sData"].push(cartObj);
                }
                
                var cookieStr = JSON.stringify(cartContent);
                
                //$.cookie('cartContent', cookieStr, { path:'/', expires: 3 }); 
                
                var cartNum = cartContent['rData'].length + cartContent['sData'].length;

                
                $.post(BASE_URL + '/addCart', {"cartContent": cookieStr,"prodDetailId": prodDetailId,"num": num,"prodId": prodId, "prodNo": prodNo}, function(data){
                    if(data.status == "success") {
                        $.cookie('cartNum', cartNum, { path:'/', expires: 14 }); 
                        $(".header-num-box").text(cartNum);
                        Swal.fire({
                            type: 'success',
                            text: '商品已加入購物車',
                        })
                        //alert('商品已加入購物車');
                    }
                    else {
                        alert(data.message);
                    }
                    
                }, 'JSON');

                
            }
            else {
                alert("請先登入，再加人購物車");
            }
        });

        return false;
    });

    function chkCartIsExist(prodDetailId, obj) {
        for(i in obj) {
            var id = obj[i].prodDetailId;

            if(id == prodDetailId) {
                return true;
            }
        }

        return false;
    }

    function _uuid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    Date.prototype.dateDiff = function(interval,objDate){
        var dtEnd = new Date(objDate);
        if(isNaN(dtEnd)) return undefined;
        switch (interval) {
        case "s":return parseInt((dtEnd - this) / 1000);
        case "n":return parseInt((dtEnd - this) / 60000);
        case "h":return parseInt((dtEnd - this) / 3600000);
        case "d":return parseInt((dtEnd - this) / 86400000);
        case "w":return parseInt((dtEnd - this) / (86400000 * 7));
        case "m":return (dtEnd.getMonth()+1)+((dtEnd.getFullYear()-this.getFullYear())*12) - (this.getMonth()+1);
        case "y":return dtEnd.getFullYear() - this.getFullYear();
        }
      }
    
      $("input[name='pick_way']").on("change", function(){
        updateCart();
      });
    
      $("input[name='return_way']").on("change", function(){
        updateCart();
      });
});