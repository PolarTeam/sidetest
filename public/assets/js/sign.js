$(document).ready(function(){

	$('.tabs-box .btn-box li:eq(0)').on('click', function(e){
		var target = $(e.target);
		$('.tabs-box .btn-box li').removeClass('active');
		$('.tabs-box .content-box .box').removeClass('active');
		target.addClass('active');
		$('.tabs-box .content-box .box:eq(0)').addClass('active');
	});

	$('.tabs-box .btn-box li:eq(1)').on('click', function(e){
		var target = $(e.target);
		$('.tabs-box .btn-box li').removeClass('active');
		$('.tabs-box .content-box .box').removeClass('active');
		target.addClass('active');
		$('.tabs-box .content-box .box:eq(1)').addClass('active');
	})
})
