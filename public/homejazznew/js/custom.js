// JavaScript Document

function sliderAnimation() {
  $(".slider").not(".slick-initialized").slick({
    autoplay: true,
    speed: 2000,
    autoplaySpeed: 4000,
    fade: true,
    dots: false,
    arrows: false,
    pauseOnFocus: false,
    pauseOnHover: false
  });
  $(".slider").on({
    beforeChange: function (b, a, d, c) {
      $(".slick-slide", this).eq(d).addClass("preve-slide");
      $(".slick-slide", this).eq(c).addClass("slide-animation")
    },
    afterChange: function () {
      $(".preve-slide", this).removeClass("preve-slide slide-animation")
    }
  });
  $(".slider").find(".slick-slide").eq(0).addClass("slide-animation")
}

function openingAnimation() {
  $(".wrap").removeClass("on");
  $(".slogan").addClass("play");	
}

$(window).on("load", function () {
  sliderAnimation();
  openingAnimation();	
});


$(document).ready(function() {

    $(".navbar a, a.team-link, a.news-link, a.btn-custom").click(function(event){
      event.preventDefault();
      redirectLink = this.href;
      $(".wrap").addClass('on');
      setTimeout(function(){
        redirectToLink(redirectLink);
      }, 500);
    });

    function redirectToLink(url) {
      window.location = url;
    }
});

$(document).ready(function(){
  $('.project-show').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
	fade: true,
	arrows: true,
	dots:true,
	pauseOnHover: true,
	autoplay: true,
    speed: 1000,
    autoplaySpeed: 7000,  
  });
});