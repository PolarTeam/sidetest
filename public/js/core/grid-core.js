var initGrid = function (gridId,fieldData,getDataUrl,gridOpt, fieldObj) {
    var statusCode = {};
    $.grep(fieldData[1], function(e){
        if(e.datafield == "status") {
            statusCode = e.statusCode;
            var statusSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: statusCode
            };

            var statusAdapter = new $.jqx.dataAdapter(statusSource, {
                autoBind: true
            });

            $.grep(fieldData[0], function(e){
                if(e.name == "status") {
                    e['values'] = { source: statusAdapter.records, value: 'value', name: 'label' }
                }
            });
        }

        if(e.datafield == "trs_mode") {
            trsMode = e.trsMode;
            var trsModeSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: trsMode
            };

            var trsModeAdapter = new $.jqx.dataAdapter(trsModeSource, {
                autoBind: true
            });

            $.grep(fieldData[0], function(e){
                if(e.name == "trs_mode") {
                    e['values'] = { source: trsModeAdapter.records, value: 'value', name: 'label' }
                }
            });
        }

        if(e.datafield == "car_type") {
            carType = e.carType;
            var carTypeSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: carType
            };

            var carTypeAdapter = new $.jqx.dataAdapter(carTypeSource, {
                autoBind: true
            });

            $.grep(fieldData[0], function(e){
                if(e.name == "car_type") {
                    e['values'] = { source: carTypeAdapter.records, value: 'value', name: 'label' }
                }
            });
        }

        if(e.datafield == "dlv_type") {
            dlvType = e.dlvType;
            var dlvTypeSource =
            {
                    datatype: "array",
                    datafields: [
                        { name: 'label', type: 'string' },
                        { name: 'value', type: 'string' }
                    ],
                    localdata: dlvType
            };

            var dlvTypeAdapter = new $.jqx.dataAdapter(dlvTypeSource, {
                autoBind: true
            });

            $.grep(fieldData[0], function(e){
                if(e.name == "dlv_type") {
                    e['values'] = { source: dlvTypeAdapter.records, value: 'value', name: 'label' }
                }
            });
        }
    });

    
    var source =
    {
        datatype: "json",
        datafields: fieldData[0],
        root:"Rows",
        pagenum: 0,
        beforeprocessing: function (data) {
            source.totalrecords = data[0].TotalRows;
            if(data[0].StatusCount.length > 0) {
                data[0].StatusCount.push({'count': data[0].TotalRows, 'status': 'ALL','statustext': 'ALL'});
            }

            if($("#statusList").find('a').length == 0) {
                genStatus(data, gridId);
            }
            /*else{

                $.each($("#statusList a span.badge"),function(k,v){
                    v.innerHTML=0
                    $.each(data[0].StatusCount, function(i, item) {
                        if($($("#statusList a span.text")[k]).attr("id") == item.status){
                            v.innerHTML=item.count;
                        }
                    });
                });
              
            }*/
            
        },
        filter: function () {
            // update the grid and send a request to the server.
            $("#"+gridId).jqxGrid('updatebounddata', 'filter');
        },
        sort: function () {
            // update the grid and send a request to the server.
            $("#"+gridId).jqxGrid('updatebounddata', 'sort');
        },
        cache: false,
        pagesize: 50,
        url: getDataUrl
    }

    if(typeof gridOpt.selectionmode === "undefined"){
        gridOpt.selectionmode = "checkbox";
    }
    var dataAdapter = new $.jqx.dataAdapter(source, { 
        async: false, 
        loadError: function (xhr, status, error) { 
            alert('Error loading "' + source.url + '" : ' + error); 
        },
        loadComplete: function() {
            $.get(gridOpt.getStatusCount, {}, function(res){
                res[0].StatusCount.push({'count': res[0].TotalRows, 'status': 'ALL','statustext': 'ALL'});
                genStatus(res, gridId);
            }, 'JSON');
        }
    });

    var h = 350;

    if(gridOpt.enabledStatus == false) {
        h = 250;
    }

    var winHeigt = $( window ).height() - h;
    if(typeof gridOpt.who !== "undefined" && gridOpt.who == "lookup") {
        winHeigt = 500;
    }
    $("#"+gridId).jqxGrid(
    {
        width: '100%',
        height: winHeigt,//'100%',//(typeof gridOpt.height == 'undefined')?800:gridOpt.height,
        //autoheight: true,
        source: dataAdapter,
        sortable: true,
        filterable: (typeof gridOpt.searchOpt !== "undefined")? gridOpt.searchOpt: false,
        altrows: true,
        showfilterrow: (typeof gridOpt.searchOpt !== "undefined")? gridOpt.searchOpt: false,
        pageable: true,
        virtualmode: true,
        autoshowfiltericon: true,
        columnsreorder: true,
        columnsresize: true,
        columnsautoresize: true,
        //autoloadstate: true,
        clipboard: true,
        selectionmode: gridOpt.selectionmode,
        enablebrowserselection: (typeof gridOpt.enablebrowserselection !== "undefined")? gridOpt.enablebrowserselection: true,
        pagesizeoptions:[50, 100, 500, 9999],
        rendergridrows: function (params) {
            //alert("rendergridrows");
            return params.data;
        },
        ready: function () {
            $('#' + gridId).jqxGrid('autoresizecolumns');
        },
        updatefilterconditions: function (type, defaultconditions) {
            var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
            var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
            var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
            var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
            switch (type) {
                case 'stringfilter':
                    return stringcomparisonoperators;
                case 'numericfilter':
                    return numericcomparisonoperators;
                case 'datefilter':
                    return datecomparisonoperators;
                case 'booleanfilter':
                    return booleancomparisonoperators;
            }
        },
        columns: fieldData[1]
    });

    if(gridOpt.who == 'lookup')
    {
        $('#'+gridOpt.gridId).on('rowdoubleclick', function (event) 
        { 
            var args = event.args;
            // row's bound index.
            var boundIndex = args.rowindex;
            // row's visible index.
            var visibleIndex = args.visibleindex;
            // right click.
            var rightclick = args.rightclick; 
            // original event.
            var ev = args.originalEvent;
      
      
            var rowindexes = $('#'+gridOpt.gridId).jqxGrid('getselectedrowindexes');
            if(gridOpt.selectionmode === "checkbox" && rowindexes.length > 0){
                
            }else{
                var datarow = $('#'+gridOpt.gridId).jqxGrid('getrowdata', boundIndex);
                console.log(datarow);
                var fieldMapping = fieldObj.info4.split(";");
            
                $.each(fieldMapping,function(k,v){
                k = v.split("=");
                $("#"+k[1]).val(datarow[k[0]]);
                if(datarow['area_nm']!=undefined){
                    if(k[1] =="pick_info" || k[1] == "dlv_info"|| k[1] == "custer_info"){
                    $("#"+k[1]).val(datarow['city_nm']+datarow['area_nm']);
                    }
                }else{
                    if(k[1] =="pick_info" || k[1] == "dlv_info"|| k[1] == "custer_info"){
                    $("#"+k[1]).val(datarow['city_nm']+datarow['dist_nm']);
                    }
                }
                });
            }
            $("#lookupEvent").trigger(fieldObj.triggerfunc,[rowindexes]);
            if(typeof callBackFunc !== 'undefined') {
                callBackFunc(datarow);
            }
            
            $('#lookupModal').modal('hide');
        });
    }
    

    $("#"+gridId).on('bindingcomplete', function (event) {
        var statusHeight = 0;
        if(typeof gridOpt.enabledStatus !== "undefined") {
            if(gridOpt.enabledStatus == false) {
                statusHeight = 50;
            }
        }
        var winHeight = $( window ).height() - 350 + statusHeight;
        $("#"+gridId).jqxGrid('height', winHeight+'px');
        //$("#"+gridId).jqxGrid("hideloadelement"); 
    });
    
    if(typeof fieldData[2] !== "undefined") {
        $("#"+gridId).jqxGrid('loadstate', fieldData[2]);
    }

    if(typeof gridOpt.getState != "undefined" && gridOpt.getState != null){
        gridOpt.getState();
    }

    var localizationobj = {};
    localizationobj.pagergotopagestring              = "到   : ";
    localizationobj.pagershowrowsstring              = "顯示筆數: ";
    localizationobj.pagerrangestring                 = " 的 ";
    // localizationobj.pagernextbuttonstring         = "voriger";
    // localizationobj.pagerpreviousbuttonstring     = "nächster";
    localizationobj.sortascendingstring              = "升冪";
    localizationobj.sortdescendingstring             = "降冪";
    localizationobj.sortremovestring                 = "移除排序";
    localizationobj.filterstringcomparisonoperators  = ['包含','不包含'];
    localizationobj.filternumericcomparisonoperators = ['小於','大於'];
    localizationobj.filterdatecomparisonoperators    = ['小於','大於'];
    localizationobj.filterbooleancomparisonoperators = ['等於','不等於'];
    localizationobj.filterclearstring                = "清除";
    localizationobj.filterstring                     = "篩選";
    localizationobj.filterchoosestring               = "請選擇：";
    localizationobj.filtershowrowstring              = "顯示資料於：";
    localizationobj.filterandconditionstring         = "且";
    localizationobj.filterorconditionstring          = "或";
    // apply localization.
    $("#" + gridId).jqxGrid('localizestrings', localizationobj);

    //$("#"+gridId).jqxGrid('selectionmode', 'multiplecellsextended');
}

function genStatus(data, gridId) {
    $("#statusList").html("");
    $.each(data[0].StatusCount, function(i, item) {
        $("#statusList").append(' <a class="statusBtn btn btn-app"><span class="badge bg-purple"> '+item.count+'</span><i class="fa fa-bullhorn"></i><span class="text" id="'+item.status+'">'+item.statustext+'</span></a>');
    });

    $('.statusBtn').off().on('click', function(){
        $('#statusList').find('a').removeClass('active');
        $(this).addClass('active');

        var statusGroup = new $.jqx.filter();
        var filter_or_operator = 1;
        var filtervalue = $(this).find('span.text').attr("id");
        if(filtervalue == "ALL") {
            $("#" + gridId).jqxGrid('clearfilters');
        }
        else {
            var filtercondition = 'contains';
            var statusFilter1 = statusGroup.createfilter('stringfilter', filtervalue, filtercondition);
            statusGroup.addfilter(filter_or_operator, statusFilter1);
            $("#" + gridId).jqxGrid('addfilter', 'status', statusGroup);
            $("#" + gridId).jqxGrid('applyfilters');
        }
        
    });
}