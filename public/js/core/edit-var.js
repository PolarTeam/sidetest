var formOpt = {};
formOpt.formId = "";
formOpt.editObj = null;
formOpt.fieldObj = null;
formOpt.editUrl = "";
formOpt.fieldsUrl = "";
formOpt.saveUrl = "";

formOpt.initFieldCustomFunc = function (){
    //console.log("todo custom func on field init");
};
formOpt.setFormDataFunc = function (){
    //console.log("todo custom func for set form data");
};
formOpt.addFunc = function (){
    //console.log("todo custom func for add & copy ");
};
formOpt.editFunc = function (){
    //console.log("todo custom func for edit");
};
formOpt.beforesaveFunc = function (){
    //console.log("todo custom func for edit");
    return true;
};
formOpt.copyFunc = function (){
    //console.log("todo custom func for copy after add func ");
};
formOpt.cancelFunc = function (){
    //console.log("todo custom func for cencel");
};
formOpt.saveErrorFunc = function (){
    //console.log("todo custom func for save error");
};
formOpt.saveSuccessFunc = function (){
    //console.log("todo custom func for save success");
};
formOpt.beforeDelFunc = function (){
    //console.log("todo custom func on iDel click check ");
    return true;
};

var PNotifyOpt = {};
PNotifyOpt.saveSuccess = {
    title: 'Save Message',
    text: "The item has been added successfully.",
    type: "success",
    icon: false
};
PNotifyOpt.saveError = {
    title: 'Save Message',
    text: "The item has been an error Your item might not have been saved.",
    type: "warning",
    icon: false
};
PNotifyOpt.delSuccess = {
    title: "Item Deleted",
    text: "The item has been deleted successfully.",
    type: "success"
};
PNotifyOpt.delError = {
    title: "NOT deleted",
    text: "There&#039;s been an error. Your item might not have been deleted.",
    type: "warning"
}
PNotifyOpt.delCancel = {
    title: "Not deleted",
    text: "Nothing happened. Your item is safe.",
    type: "info"
}

function notifyMsg(title, content, type) {
    new PNotify({
        title: title,
        text: content,
        type: type,
        icon: false
    });
}