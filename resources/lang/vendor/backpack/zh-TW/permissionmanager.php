<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Permission Manager Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Laravel Backpack - Permission Manager
    | Author: Lúdio Oliveira <ludio.ao@gmail.com>
    |
    */
    'name'                  => '名稱',
    'role'                  => '角色',
    'roles'                 => '角色',
    'roles_have_permission' => '角色擁有的權限',
    'permission_singular'   => ' 權限',
    'permission_plural'     => '權限',
    'user_singular'         => '使用者',
    'user_plural'           => '使用者',
    'email'                 => '電子郵件',
    'extra_permissions'     => '額外權限',
    'password'              => '密碼',
    'password_confirmation' => '確認密碼',
    'user_role_permission'  => '使用者角色權限',
    'user'                  => '使用者',
    'users'                 => '使用者',
    'gkey'                 => '集團',
    'ckey'                 => '公司',
    'skey'                 => '站別',
    'dkey'                 => '部門',

];
