<?php 

 return [
    "id" => "id",
    "cateId" => "類型",
    "title" => "標題",
    "subTitle" => "副標題",
    "addr" => "位置",
    "planning" => "規劃",
    "updatedAt" => "修改時間",
    "updatedBy" => "修改人",
    "createdAt" => "創建時間",
    "createdBy" => "創建人",
    "img1" => "圖片",
    "content1" => "左邊內容",
    "content2" => "右邊內容",
    "finishAt" => "年份",
    "area" => "地區",
    "link" => "網站連結",
    "structure" => "建案資訊",
    "structureNew" => "構造"
];