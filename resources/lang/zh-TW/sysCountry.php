<?php 

 return [
    "cKey" => "公司",
    "cntryCd" => "國家代碼",
    "cntryNm" => "國家名稱",
    "createdAt" => "創建時間",
    "createdBy" => "創建人",
    "dKey" => "部門",
    "gKey" => "集團",
    "id" => "id",
    "sKey" => "站別",
    "updatedAt" => "修改時間",
    "updatedBy" => "修改人",
    "titleName" => "國家資料彙總",
    "titleAddName" => "國家建檔"
];