<?php
return [
    "id"                => "id",
    "userId"            => "使用者id",
    "ordNo"             => "訂單號",
    "status"            => "狀態",
    "oPrice"            => "原價",
    "dPrice"            => "特價",
    "ordNm"             => "訂購人",
    "dlvNm"             => "收貨人",
    "dlvZip"            => "郵地區號",
    "dlvCity"           => "城市",
    "dlvArea"           => "區域",
    "dlvAddr"           => "地址",
    "shipFee"           => "運費",
    "payWay"            => "付款方式",
    "shipWay"           => "運送方式",
    "shipNo"            => "物流號",
    "payNo"             => "付款號",
    "delayPayNo"        => "展延付款號",
    "discountNo"        => "促銷碼",
    "createdBy"         => "建立人",
    "updatedBy"         => "修改人",
    "gKey"              => "集團",
    "cKey"              => "公司",
    "sKey"              => "站別",
    "dKey"              => "部門",
    "createdAt"         => "建立時間",
    "updatedAt"         => "修改時間",
    "STATUS_A"          => "未付款",
    "STATUS_B"          => "已付款",
    "STATUS_C"          => "運送中",
    "STATUS_D"          => "已送達",
    "STATUS_E"          => "已歸還",
    "STATUS_F"          => "通知取消",
    "STATUS_G"          => "已取消",
    "STATUS_I"          => "商品出貨",
    "dlvPhone"          => "收貨人電話",
    "pickUpDate"        => "取貨日期",
    "email"             => "email",
    "addr"              => "收貨地址",
    "pickWay"           => "取貨方式",
    "returnWay"         => "歸還方式",
    "departureTerminal" => "出發航廈",
    "returnTerminal"    => "歸國航廈",
    "payDate"           => "付款日期",
    "detailDescp"       => "明細租用商品",
    "STATUS_未付款"        => "未付款",
    "STATUS_已付款"        => "已付款",
    "STATUS_運送中"        => "運送中",
    "STATUS_已送達"        => "已送達",
    "STATUS_已歸還"        => "已歸還",
    "STATUS_通知取消"       => "通知取消",
    "STATUS_確認取消"       => "確認取消",
    "STATUS_已取消"        => "已取消",
    "STATUS_已刷退"        => "已刷退",
    "STATUS_商品出貨"        => "商品出貨",
    "returnDate"        => "回國時間",
    "remark" => "備註",
    "custRemark" => "客戶備註",
    "storeType" => "超商",
    "storeId" => "超商號",
    "storeName" => "超商名稱",
    "storeAddr" => "超商住址",
    "storePhone" => "超商電話",
    "rCode" => "優惠碼(租)",
    "sCode" => "優惠碼(賣)",
    "shipmentNo" => "客人查詢單號/客人查詢單號",
    "cVSPaymentNo" => "寄件單號",
    "isPrint" => "列印",
    "payBankCode" => "銀行代碼",
    "payerAccount5code" => "銀行卡號末五碼",
    "shipDate" => "出貨時間"
    ];