<?php 

 return [
    "add"         => "新增",
    "back"        => "返回",
    "cancel"      => "取消",
    "clear"       => "清除",
    "close"       => "關閉",
    "delete"      => "刪除",
    "deleteMsg"   => "刪除成功",
    "excelImport" => "Excel 匯入",
    "save"        => "儲存",
    "search"      => "搜尋",
    "gridOption"  => "欄位調整",
    "addNewRow"   => "新增資料",
    "deleteSelectRow" => "刪除所選資料",
    "msg1"        => "請至少選一筆記錄",
    "copy"        => "複製",
    "edit"        => "編輯",
    "msg2"		  => "確定刪除此筆資料",
    "saveChange"  => "保存",
    "browse"      => "瀏覽檔案",
    "exportExcel" => "匯出Excel",
    "fail"        => "作廢",
    "search"      => "搜尋"
];