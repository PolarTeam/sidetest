<?php 

 return [
    "id" => "id",
    "baseInfo" => "代碼類別資料",
    "detail" => "代碼明細資料",
    "cdType" => "代碼類別",
    "updatedBy" => "修改人",
    "updatedAt" => "修改時間",
    "cKey" => "公司",
    "createdBy" => "創建人",
    "createdAt" => "創建時間",
    "cdDescp" => "名稱",
    "titleName" => "基本代碼彙總",
    "sKey" => "站別",
    "dKey" => "部門",
    "gKey" => "集團",
    "titleAddName" => "基本代碼建檔"
];