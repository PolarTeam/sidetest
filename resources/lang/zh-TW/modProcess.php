<?php 

 return [
    "id" => "id",
    "cateId" => "類型",
    "title" => "標題",
    "addr" => "位置",
    "planning" => "規劃",
    "updatedAt" => "修改時間",
    "updatedBy" => "修改人",
    "createdAt" => "創建時間",
    "createdBy" => "創建人",
    "img1" => "圖片",
    "years" => "年份",
    "structure" => "構造"
];