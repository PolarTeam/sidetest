<?php 

 return [
    "id" => "id",
    "phone" => "電話",
    "name" => "姓名",
    "subject" => "主旨",
    "message" => "內容",
    "updatedAt" => "修改時間",
    "updatedBy" => "修改人",
    "createdAt" => "創建時間",
    "sendMail" => "是否已發送",
    "createdBy" => "創建人",
];