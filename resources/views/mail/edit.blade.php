@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
	編輯<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url(config('backpack.base.route_prefix'),'mail') }}">總覽</a></li>
		<li class="active">編輯</li>
	</ol>
</section>
<script>
	var single_shipment = "";
    var vendor_nm = "";
    var vendor_cd = "";
    var action_sort =0;
    var sort =0;
    @if(isset($newData))
        single_shipment = "N";
        sort = 99;
        action_sort = 99;
        vendor_cd = "{{$newData->email}}";
        vendor_nm = "{{$newData->name}}";
	@endif

</script>
@endsection 

@section('content')
	@include('backpack::template.toolbar')
	<div class="row">
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4>{{ trans('backpack::crud.please_fix') }}</h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">基本資料</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="box-body">
								<div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="name">名稱</label>
                                        <input type="text" class="form-control" id="name" name="name">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="phone">電話</label>
                                        <input type="text" class="form-control" id="phone" name="phone">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="type">諮詢項目</label>
                                        <input type="text" class="form-control" id="type" name="type">
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="cust_email">e-mail</label>
                                        <input type="text" class="form-control" id="cust_email" name="cust_email">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="created_at">客人反應時間</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-9" id="action_date" >
                                        <label for="message">諮詢內容</label>
                                        <textarea class="form-control" rows="3" name="message"></textarea>
                                        {{-- <input type="text" class="form-control" id="content1" name="content1" rows="3"> --}}
                                    </div>
                                </div>


								@if(isset($id))
								<input type="hidden" name="id" value="{{$id}}" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								@endif
                            </div>
                        </div>                        
                    </div>
				</div>
			</form>
		</div>	
    </div>


@endsection 
@include('backpack::template.lookup') 
@section('after_scripts')
<script src="{{ asset('vendor/backpack/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/backpack/ckeditor/adapters/jquery.js') }}"></script>
	<script>
        var mainId = "";
        var prodNo = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/mail') }}";

		var fieldData = null;
        var fieldObj = null;
        
        var imgPath = "{{Storage::url('/')}}";

		@if(isset($crud -> create_fields))
		fieldData = '{{!! json_encode($crud->create_fields) !!}}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		@endif

		@if(isset($id))
		mainId   = "{{$id}}";
		editData = '{{!! $entry !!}}';
		editData = editData.substring(1);
		editData = editData.substring(0, editData.length - 1);
        var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
        console.log(editData);
		//editObj  = JSON.parse(objJson);
		@endif

		$(function () {

            function prodTypeSwitch(prodType) {
                if(prodType == "S") {
                    $("#countryArea").hide();
                    $("#tPriceArea").hide();
                }
                else {
                    $("#countryArea").show();
                    $("#tPriceArea").show();
                }
            }


            $("#prod_type").on("change", function(){
                if($(this).val()=="S"){
                    $("#create_inv").val("N");
                }else{
                    $("#create_inv").val("Y");
                }
                prodTypeSwitch($(this).val());
            });

			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/mail') }}";
			formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_message') }}/" + mainId;
			formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/mail') }}";
			formOpt.afterInit = function() {
				if(mainId != null) {
                    $("input[type='file']").not('[grid="true"]').prop("disabled", true);
                    setTimeout(function(){
                        //CKEDITOR.instances['descp'].setReadOnly(true);
                        //CKEDITOR.instances['slogan'].setReadOnly(true);
                        //CKEDITOR.instances['other_content'].setReadOnly(true);
                        
                    }, 500);                    
                }                    
                
                // var prodType = $("#prod_type").val();
                // prodNo = $("#prod_no").val();

                // if(prodType == "S") {
                //     $("[name='snDetail']").show();
                //     $("[name='prodDetail']").hide();
                //     $("[name='prodGift']").hide();
                //     $("[href='#tab_5']").click();
                // }
                // else {
                //     $("[name='snDetail']").hide();
                //     $("[name='snDetail_T']").hide();
                //     $("[name='snDetail_N']").hide();
                //     $("[name='prodDetail']").show();
                //     $("[href='#tab_4']").click();
                // }
                $("[name='prodDetail']").show();
                $("[href='#tab_4']").click();
                getDetailGrid();
                // prodTypeSwitch(prodType);
			}

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.afterDel = function() {

			}

			formOpt.addFunc = function() {
                setTimeout(function(){
                    $("#single_shipment").val(single_shipment);
                    $("#vendor_nm").val(vendor_nm);
                    $("#action_sort").val(action_sort);
                    $("#sort").val(sort);
                    CKEDITOR.instances['descp'].setReadOnly(false);
                    CKEDITOR.instances['slogan'].setReadOnly(false);
                    CKEDITOR.instances['other_content'].setReadOnly(false);

                    CKEDITOR.instances['descp'].setData("");
                    CKEDITOR.instances['slogan'].setData("");
                    CKEDITOR.instances['other_content'].setData("");
                }, 500);
                
                @if(isset($prodType))
                $("#prod_type").val("{{$prodType}}");
                $("#prod_type").trigger("change");
                @endif
			}

			formOpt.editFunc = function() {
                setTimeout(function(){
                    CKEDITOR.instances['descp'].setReadOnly(false);
                    CKEDITOR.instances['slogan'].setReadOnly(false);
                    CKEDITOR.instances['other_content'].setReadOnly(false);
                }, 500);
                
			}

			formOpt.copyFunc = function() {
                $('#jqxGrid').jqxGrid('clear');
                setTimeout(function(){
                    CKEDITOR.instances['descp'].setReadOnly(false);
                    CKEDITOR.instances['slogan'].setReadOnly(false);
                    CKEDITOR.instances['other_content'].setReadOnly(false);

                    CKEDITOR.instances['descp'].setData(editObj.descp);
                    CKEDITOR.instances['slogan'].setData(editObj.slogan);
                    CKEDITOR.instances['other_content'].setData(editObj.other_content);
                }, 500);
            }

            formOpt.beforeSave = function() {

            }

			formOpt.saveSuccessFunc = function(data) {
                var result = data.data;
                console.log(result);
                if(result.img1 != "" && result.img1 != null) {
                    $("img[name='img1']").attr("src", result.img1).show();
                    $("a[name='img1']").attr("href", result.img1);
                }

                // if(result.img2 != null) {
                //     $("img[name='img2']").attr("src", result.img2).show();
                //     $("a[name='img2']").attr("href", result.img2);
                // }

                // if(result.img3 != null) {
                //     $("img[name='img3']").attr("src", result.img3).show();
                //     $("a[name='img3']").attr("href", result.img3);
                // }

                // if(result.img4 != null) {
                //     $("img[name='img4']").attr("src", result.img4).show();
                //     $("a[name='img4']").attr("href", result.img4);
                // }

                // if(result.img5 != null) {
                //     $("img[name='img5']").attr("src", result.img5).show();
                //     $("a[name='img5']").attr("href", result.img5);
                // }

                // CKEDITOR.instances['descp'].setReadOnly(true);
                // CKEDITOR.instances['slogan'].setReadOnly(true);
                // CKEDITOR.instances['other_content'].setReadOnly(true);
                $("input[type='file']").not('[grid="true"]').prop("disabled", true);
                $("input[type='file']").val("");
			}

			var btnGroup = [

			];

            initBtn(btnGroup);


            $('a[href="#tab_3"]').on("click", function(){
                var descp = CKEDITOR.instances['descp'].getData();
                var slogan = CKEDITOR.instances['slogan'].getData();
                var other_content = CKEDITOR.instances['other_content'].getData();

                if(descp == "") {
                    CKEDITOR.instances['descp'].setData(editObj.descp);
                }

                if(slogan == "") {
                    CKEDITOR.instances['slogan'].setData(editObj.slogan);
                }

                if(other_content == "") {
                    CKEDITOR.instances['other_content'].setData(editObj.other_content);
                }
                
            });
            
        });

        function getDetailGrid() {
            var imagerenderer = function (row, datafield, value) {
                if(value != "") {
                    return '<img style="margin-left: 5px;" height="60" width="50" src="'+ROOT_URL+'/storage/' + value.replace('public/', '') + '"/>';
                }

                return "";
            }
            // var countryCode = [];

            // if(fieldObj != null) {
            //     for(i in fieldObj["country"]["options"]) {
            //         var obj = {value: fieldObj["country"]["options"][i]["code"], label: fieldObj["country"]["options"][i]["descp"]};
            //         countryCode.push(obj);
            //     }
            // }

            // var countrySource =
            // {
            //         datatype: "array",
            //         datafields: [
            //             { name: 'label', type: 'string' },
            //             { name: 'value', type: 'string' }
            //         ],
            //         localdata: countryCode
            // };

            // var countryAdapter = new $.jqx.dataAdapter(countrySource, {
            //     autoBind: true
            // });
            //console.log(countryAdapter.records);
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "process_id", type: "string"},
                    {name: "cate_nm", type: "string"},
                    // {name: "img1", type: "string"},
                    {name: "img_descp1", type: "string"},
                    {name: "img_descp2", type: "string"},
                ],
                [
                    {text: "id", datafield: "id", width: 100},
                    {text: "process_id", datafield: "process_id", width: 150, hidden: true},
                    // {text: "圖片1", datafield: "img1", width: 150, cellsrenderer: imagerenderer},
                    {text: "敘述", datafield: "img_descp1", width: 150},
                    {text: "內容", datafield: "img_descp2", width: 150},
                ]
            ];
            var opt = {};
            if(mainId==""){
                mainId="0";
            }
            opt.gridId = "jqxGrid";
            opt.fieldData = col;
            opt.formId = "subForm";
            opt.saveId = "Save";
            opt.cancelId = "Cancel";
            opt.showBoxId = "subBox";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/processDetail') }}"+ "/"+ mainId;;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/processDetail') }}" + "/detailStore";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/processDetail') }}" + "/update/"+ mainId;
            opt.delUrl  = "{{ url(config('backpack.base.route_prefix', 'admin') . '/processDetail') }}" + "/delete/";
            opt.defaultKey = {'process_id': mainId};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.rowsheight = 60;
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                console.log(data);
            }
            console.log("nick");
            $("[name='prodDetail']").show();
            $("[href='#tab_4']").click();
            genDetailGrid(opt);
        }
        

    </script>
@endsection