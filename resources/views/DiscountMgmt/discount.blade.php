@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
		折扣總覽
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">折扣總覽</li>
	</ol>
</section>
@endsection 
@section('before_scripts')


<script>
    var gridOpt = {};
    var userAngent = getMobileOperatingSystem();
    gridOpt.pageId        = "modDiscountMemberView";
    gridOpt.enabledStatus = true;
    gridOpt.fieldsUrl     = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_discount_member_view') }}";
    gridOpt.dataUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_discount_member_view') }}";
    gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl     = false;
    gridOpt.editUrl       = false;
    gridOpt.height        = 800;
    if(userAngent == "Android") {
        gridOpt.selectionmode = "multiplecellsadvanced";
    }
    else {
        gridOpt.selectionmode = "checkbox";
    }
    
    gridOpt.searchOpt     = true;
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick = false;
    statusGridObj = [{"key":"SEND","val":"123"}];
    var btnGroup = [
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
    ];


    $("#jqxGrid").on('cellselect', function (event) 
    {
        // event arguments.
        var args = event.args;
        // get the column's text.
        var column = $("#jqxGrid").jqxGrid('getcolumn', event.args.datafield).text;
        // column data field.
        var dataField = event.args.datafield;
        // row's bound index.
        var rowBoundIndex = event.args.rowindex;
        // cell value
        var value = $('#jqxGrid').jqxGrid('getcellvalue', rowBoundIndex, 'cd');

        if(column == "折扣碼") {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(value).select();
            document.execCommand("copy");
            $temp.remove();
        }
        
    });

    function getMobileOperatingSystem() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;

            // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            return "iOS";
        }

        return "unknown";
    }
</script>
@endsection 
@include('backpack::template.search')