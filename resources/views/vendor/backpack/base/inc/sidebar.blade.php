@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel" style="height: 44px">
          <div class="pull-left image">
            {{-- <img src="https://placehold.it/160x160/00a7d0/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" class="img-circle" alt="User Image"> --}}
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            {{--  <a href="#" id="onlineStatus"><i class="fa fa-circle text-success"></i> Online</a>  --}}
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">{{ trans('backpack::base.administration') }}</li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->

          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind') }}"><i class="fa fa-file"></i> <span>基本資訊</span></a></li>

          <li><a href="{{  url(config('backpack.base.route_prefix', 'admin') . '/menu') }}"><i class="fa fa-tag"></i> <span>首頁</span></a></li>  
          <li><a href="{{  url(config('backpack.base.route_prefix', 'admin') . '/projects') }}"><i class="fa fa-tag"></i> <span>作品集</span></a></li>  
          {{-- <li><a href="{{  url(config('backpack.base.route_prefix', 'admin') . '/mail') }}"><i class="fa fa-tag"></i> <span>客人反應訊息</span></a></li>   --}}

        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
