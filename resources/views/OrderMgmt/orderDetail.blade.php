@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
		一般商品訂單明細總覽
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">一般商品訂單明細</li>
	</ol>
</section>
@endsection 
@section('before_scripts')


<script>
    var gridOpt = {};
    gridOpt.pageId        = "modOrderDetailView";
    gridOpt.enabledStatus = false;
    gridOpt.fieldsUrl     = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order_detail_view') }}";
    gridOpt.dataUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_detail_view') }}";
    gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl     = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt/create') }}";
    gridOpt.editUrl       = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt') }}" + "/{id}/edit";
    gridOpt.height        = 800;
    gridOpt.selectionmode = "checkbox";
    gridOpt.searchOpt     = true;
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick = function(row) {
        var data = $("#jqxGrid").jqxGrid('getrowdata', row);
        if(data.status == "出貨") {
            swal("已出貨商品，無法再修改", "", "warning");
            return;
        }
        if(data.ship_flag == "Z大") {
            swal("訂單號：" + data.ord_no + "，這筆是出給z大的！！", "", "warning");
            return;
        }

        $("#myModal").modal("show");
    };
    statusGridObj = [{"key":"SEND","val":"123"}];
    var btnGroup = [
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "{{ trans('common.exportExcel') }}",
            btnFunc: function () {
            //$("#jqxGrid").jqxGrid('exportdata', 'xls', '{{ trans("modOrder.titleName") }}');
            $("#jqxGrid").jqxGrid('exportdata', 'json', '一般商品訂單明細', true, null, false, BASE_API_URL+'/admin/export/data');
            }
        },
    ];

    $(function() {
        $("#submitBtn").on("click", function(){
            var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
            var ids = new Array();
            for (var m = 0; m < rows.length; m++) {
                var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                if(typeof row != "undefined") {
                    ids.push(row.id);
                }
            }

            var shipper = $("#shipper").val();
            var shipNo = $("#shipNo").val();

            if(shipNo == "") {
                swal("請填寫物流單號", "", "warning");
            }

            $.post(BASE_URL + '/prodShipping', {'ids': ids, 'shipper': shipper, 'shipNo': shipNo}, function(data){
                if(data.msg == "success") {
                    swal("操作成功", "", "success");
                    $("#jqxGrid").jqxGrid('updatebounddata');
                    $('#jqxGrid').jqxGrid('clearselection');
                }
                else{
                    swal("操作失敗", "", "error");
                }
                $("#cssLoading").hide();
            });
        });
    });
</script>
<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">出貨確認-訂單號：<span id="ordNo"></span></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="shipper">物流業者</label>
                    <select class="form-control" id="shipper" name="shipper">
                        @foreach($shipper as $row)
                        <option value="{{$row->cd}}">{{$row->cd_descp}}</option>
                        @endforeach
                        <option value="OTHER">其它</option>
                    </select>
                    <input type="hidden" name="ordId" id="ordId">
                </div>
                <div class="form-group">
                    <label for="shipNo">物流單號</label>
                    <input type="text" class="form-control" id="shipNo" placeholder="" name="shipNo">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
                <button type="button" class="btn btn-primary" id="submitBtn">送出</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection 
@include('backpack::template.search')