@extends('backpack::layout') 
@section('header')
<style>
.lmask {
position: absolute;
height: 100%;
width: 100%;
background-color: #000;
bottom: 0;
left: 0;
right: 0;
top: 0;
z-index: 9999;
opacity: 0.4;
}
.lmask.fixed {
position: fixed;
}
.lmask:before {
content: '';
background-color: rgba(0, 0, 0, 0);
border: 5px solid rgba(0, 183, 229, 0.9);
opacity: .9;
border-right: 5px solid rgba(0, 0, 0, 0);
border-left: 5px solid rgba(0, 0, 0, 0);
border-radius: 50px;
box-shadow: 0 0 35px #2187e7;
width: 50px;
height: 50px;
-moz-animation: spinPulse 1s infinite ease-in-out;
-webkit-animation: spinPulse 1s infinite linear;
margin: -25px 0 0 -25px;
position: absolute;
top: 50%;
left: 50%;
}
.lmask:after {
content: '';
background-color: rgba(0, 0, 0, 0);
border: 5px solid rgba(0, 183, 229, 0.9);
opacity: .9;
border-left: 5px solid rgba(0, 0, 0, 0);
border-right: 5px solid rgba(0, 0, 0, 0);
border-radius: 50px;
box-shadow: 0 0 15px #2187e7;
width: 30px;
height: 30px;
-moz-animation: spinoffPulse 1s infinite linear;
-webkit-animation: spinoffPulse 1s infinite linear;
margin: -15px 0 0 -15px;
position: absolute;
top: 50%;
left: 50%;
}

@-moz-keyframes spinPulse {
0% {
-moz-transform: rotate(160deg);
opacity: 0;
box-shadow: 0 0 1px #2187e7;
}
50% {
-moz-transform: rotate(145deg);
opacity: 1;
}
100% {
-moz-transform: rotate(-320deg);
opacity: 0;
}
}
@-moz-keyframes spinoffPulse {
0% {
-moz-transform: rotate(0deg);
}
100% {
-moz-transform: rotate(360deg);
}
}
@-webkit-keyframes spinPulse {
0% {
-webkit-transform: rotate(160deg);
opacity: 0;
box-shadow: 0 0 1px #2187e7;
}
50% {
-webkit-transform: rotate(145deg);
opacity: 1;
}
100% {
-webkit-transform: rotate(-320deg);
opacity: 0;
}
}
@-webkit-keyframes spinoffPulse {
0% {
-webkit-transform: rotate(0deg);
}
100% {
-webkit-transform: rotate(360deg);
}
}

</style>
<section class="content-header">
	<h1>
		WIFI訂單總覽
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">WIFI訂單</li>
	</ol>
</section>
<div class='lmask' id="cssLoading" style="display:none"></div>
@endsection 
@section('before_scripts')


<script>
    var gridOpt = {};
    gridOpt.pageId        = "wifiOrderView";
    gridOpt.enabledStatus = true;
    gridOpt.fieldsUrl     = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/wifi_order_view') }}";
    gridOpt.dataUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/wifi_order_view') }}";
    gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl     = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt/create') }}";
    gridOpt.editUrl       = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt') }}" + "/{id}/edit";
    gridOpt.height        = 800;
    gridOpt.selectionmode = "checkbox";
    gridOpt.searchOpt     = true;
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick = true;
    statusGridObj = [{"key":"SEND","val":"123"}];
    var btnGroup = [
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "{{ trans('common.exportExcel') }}",
            btnFunc: function () {
            //$("#jqxGrid").jqxGrid('exportdata', 'xls', '{{ trans("modOrder.titleName") }}');
            $("#jqxGrid").jqxGrid('exportdata', 'json', '訂單匯總', true, null, false, BASE_API_URL+'/admin/export/data');
            }
        },
        {
            btnId: "btnExportPelican",
            btnIcon: "fa fa-cloud-download",
            btnText: "宅配通匯出",
            btnFunc: function () {
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                $.fileDownload(BASE_URL + "/exportPelican?ids="+ids);
            }
        },
        {
            btnId: "btnAdd",
            btnIcon: "fa fa-edit",
            btnText: "{{ trans('common.add') }}",
            btnFunc: function () {
            window.open(gridOpt.createUrl);
            }
        },
        {
            btnId:"btnDelete",
            btnIcon:"fa fa-trash-o",
            btnText:"{{ trans('common.delete') }}",
            btnFunc:function(){
                searchMultiDel("jqxGrid", BASE_URL + '/order/multi/del');
            }
        },
        {
            btnId:"btnExportDetail",
            btnIcon:"fa fa-arrow-circle-down",
            btnText:"匯出機場訂單資訊",
            btnFunc:function(){
                var idx = $('#jqxGrid').jqxGrid('getselectedrowindexes');
                if(idx.length ==  0) {
                    swal("請選擇一筆資料", "", "warning");
                    return;
                }

                var ids = '';
                for(i in idx) {
                    var rowData = $('#jqxGrid').jqxGrid('getrowdata', idx[i]);
                    ids += rowData.id + ';';
                }
                ids = ids.substring(0, ids.length - 1);
                location.href = BASE_URL + '/export/detail?ids=' + ids;
            }
        },
        {
            btnId:"btnSend",
            btnIcon:"fa fa-truck",
            btnText:"出貨",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                $("#cssLoading").show();
                $.post(BASE_URL + '/shipping', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $('#jqxGrid').jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                    $("#cssLoading").hide();
                });

                
            }
        },
        {
            btnId:"btnReturnConfirm",
            btnIcon:"fa fa-ban",
            btnText:"申請退款",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var id = "";
                if(rows.length > 1) {
                    swal("退款作業一次只能操作一筆資料", "", "warning");
                    return;
                }

                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        id = row.id;
                    }
                }

                if(id.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                $.post(BASE_URL + '/cancelPay/' + id, {}, function(data){
                    console.log(data);
                    if(data.Status == "SUCCESS") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $('#jqxGrid').jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", data.Message, "error");
                    }
                });

                
            }
        },
        {
            btnId:"btnPaymentConfirm",
            btnIcon:"fa fa-credit-card",
            btnText:"付款確認",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();

                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        
                        if(row.status != "未付款") {
                            swal("未付款的訂單才能做付款確認", "", "warning");
                            return;
                        }
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                $.post(BASE_URL + '/paymentConfirm', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $('#jqxGrid').jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                });

                
            }
        },
        {
            btnId:"btnSendQrCode",
            btnIcon:"fa fa-qrcode",
            btnText:"QR Code",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        if(row.pick_way != "機場取機") {
                            alert("只有機場取機才能發送QR Code");
                            return;
                        }
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                $.post(BASE_URL + '/sendQrCode', {'ids': ids}, function(data){
                    if(data.msg == "success") {
                        swal("操作成功", "", "success");
                        $("#jqxGrid").jqxGrid('updatebounddata');
                        $('#jqxGrid').jqxGrid('clearselection');
                    }
                    else{
                        swal("操作失敗", "", "error");
                    }
                });

                
            }
        },
        {
            btnId:"btnPrintBill",
            btnIcon:"fa fa-print",
            btnText:"列印小白單",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var id = "";
                if(rows.length > 1) {
                    swal("列印作業一次只能操作一筆資料", "", "warning");
                    return;
                }

                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        id = row.id;

                        if(row.status == "未付款") {
                            swal("已付款才能列印", "", "warning");
                            return;
                        }

                        if(row.CVSPaymentNo == "" || row.CVSPaymentNo == null) {
                            swal("請先去生成物流單!!!!!!!!!!", "", "warning");
                            return;
                        }
                    }
                }

                if(id.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                window.open(BASE_URL + '/PrintC2CBill/' + id);

                
            }
        },
        {
            btnId:"btnCancelWifi",
            btnIcon:"fa fa-ban",
            btnText:"取消機場WIFI機",
            btnFunc:function(){
                var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
                var ids = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
                    if(typeof row != "undefined") {
                        ids.push(row.id);
                    }
                }
                if(ids.length == 0) {
                    swal("請至少選擇一筆資料", "", "warning");
                    return;
                }

                swal({
                    title: "確認視窗",
                    text: "您確定寶島刪除wifi訂單嗎？",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#dc3545',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: "確定",
                    cancelButtonText: "取消"
                }).then((result) => {
                    if (result.value) {
                        $.post(BASE_URL + '/cancelWifi', {'ids': ids}, function(data){
                            if(data.msg == "success") {
                                swal("操作成功", "", "success");
                                $("#jqxGrid").jqxGrid('updatebounddata');
                                $('#jqxGrid').jqxGrid('clearselection');
                            }
                            else{
                                swal("操作失敗", "", "error");
                            }
                        });
                    } else {
                    
                    }
                });
            }
        },
    ];

</script>
@endsection 
@include('backpack::template.search')