@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
		wifi訂單明細總覽
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active">wifi訂單明細</li>
	</ol>
</section>
@endsection 
@section('before_scripts')


<script>
    var gridOpt = {};
    gridOpt.pageId        = "modOrderRentDetailView";
    gridOpt.enabledStatus = false;
    gridOpt.fieldsUrl     = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_order_rent_detail_view') }}";
    gridOpt.dataUrl       = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_order_rent_detail_view') }}";
    gridOpt.fieldsUrl     = gridOpt.fieldsUrl + "?key=" + gridOpt.dataUrl;
    gridOpt.createUrl     = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt/create') }}";
    gridOpt.editUrl       = "{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt') }}" + "/{id}/edit";
    gridOpt.height        = 800;
    gridOpt.selectionmode = "checkbox";
    gridOpt.searchOpt     = true;
    gridOpt.enablebrowserselection = true;
    gridOpt.rowdoubleclick = true;
    statusGridObj = [{"key":"SEND","val":"123"}];
    var btnGroup = [
        {
            btnId: "btnOpenGridOpt",
            btnIcon: "fa fa-table",
            btnText: "{{ trans('common.gridOption') }}",
            btnFunc: function () {
                $('#gridOptModal').modal('show');
            }
        },
        {
            btnId: "btnExportExcel",
            btnIcon: "fa fa-cloud-download",
            btnText: "{{ trans('common.exportExcel') }}",
            btnFunc: function () {
            //$("#jqxGrid").jqxGrid('exportdata', 'xls', '{{ trans("modOrder.titleName") }}');
            $("#jqxGrid").jqxGrid('exportdata', 'json', 'wifi訂單明細', true, null, false, BASE_API_URL+'/admin/export/data');
            }
        },
    ];

</script>
@endsection 
@include('backpack::template.search')