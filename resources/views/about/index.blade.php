@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        關於景都<small></small>
      </h1>
      <ol class="breadcrumb">
      <li class="active">關於景都</li>
      </ol>
    </section>
@endsection

@section('before_scripts')


<script>
var gridOpt = {};
gridOpt.pageId        = "about";
gridOpt.enabledStatus = false;
gridOpt.searchOpt     = true;
gridOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_about') }}";
gridOpt.dataUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_about') }}";
gridOpt.createUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/about/create') }}";
gridOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/about') }}" + "/{id}/edit";

var btnGroup = [
  {
    btnId: "btnSearchWindow",
    btnIcon: "fa fa-search",
    btnText: "{{ trans('common.search') }}",
    btnFunc: function () {
        $('#searchWindow').jqxWindow('open');
    }
  },
  {
    btnId: "btnExportExcel",
    btnIcon: "fa fa-cloud-download",
    btnText: "{{ trans('common.exportExcel') }}",
    btnFunc: function () {
      $("#jqxGrid").jqxGrid('exportdata', 'xls', '關於景都');
    }
  },
  {
    btnId: "btnOpenGridOpt",
    btnIcon: "fa fa-table",
    btnText: "{{ trans('common.gridOption') }}",
    btnFunc: function () {
      $('#gridOptModal').modal('show');
    }
  },
  // {
  //   btnId:"btnAdd",
  //   btnIcon:"fa fa-edit",
  //   btnText:"{{ trans('common.add') }}",
  //   btnFunc:function(){
  //     location.href= gridOpt.createUrl;
  //   }
  // },
];
</script>
@endsection

@include('backpack::template.search')
