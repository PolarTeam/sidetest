@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
	編輯<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url(config('backpack.base.route_prefix'),'about') }}">總覽</a></li>
		<li class="active">編輯</li>
	</ol>
</section>
<script>
	var single_shipment = "";
    var vendor_nm = "";
    var vendor_cd = "";
    var action_sort =0;
    var sort =0;
    @if(isset($newData))
        single_shipment = "N";
        sort = 99;
        action_sort = 99;
        vendor_cd = "{{$newData->email}}";
        vendor_nm = "{{$newData->name}}";
	@endif

</script>
@endsection 

@section('content')
	@include('backpack::template.toolbar')
	<div class="row">
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4>{{ trans('backpack::crud.please_fix') }}</h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">基本資料</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
                            <div class="box-body">
								<div class="row">
                                <div class="form-group col-md-12">
                                    <label for="content">內容</label>
                                    <textarea class="form-control iEditor" rows="3" id ='content' name="content"></textarea>
                                </div>




								@if(isset($id))
								<input type="hidden" name="id" value="{{$id}}" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								@endif
                            </div>


                        </div>                        
                    </div>
				</div>
			</form>
		</div>	
    </div>


@endsection 
@include('backpack::template.lookup') 
@section('after_scripts')
<script src="{{ asset('vendor/backpack/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/backpack/ckeditor/adapters/jquery.js') }}"></script>
	<script>
        var mainId = "";
        var prodNo = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/about') }}";

		var fieldData = null;
        var fieldObj = null;
        
        var imgPath = "{{Storage::url('/')}}";

		@if(isset($crud -> create_fields))
		fieldData = '{{!! json_encode($crud->create_fields) !!}}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		@endif

		@if(isset($id))
		mainId   = "{{$id}}";
		editData = '{{!! $entry !!}}';
		editData = editData.substring(1);
		editData = editData.substring(0, editData.length - 1);
        var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
        console.log(editData);
		//editObj  = JSON.parse(objJson);
		@endif

		$(function () {

            function prodTypeSwitch(prodType) {
                if(prodType == "S") {
                    $("#countryArea").hide();
                    $("#tPriceArea").hide();
                }
                else {
                    $("#countryArea").show();
                    $("#tPriceArea").show();
                }
            }


            $("#prod_type").on("change", function(){
                if($(this).val()=="S"){
                    $("#create_inv").val("N");
                }else{
                    $("#create_inv").val("Y");
                }
                prodTypeSwitch($(this).val());
            });

			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/about') }}";
			formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_about') }}/" + mainId;
			formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/about') }}";
			formOpt.afterInit = function() {
                menuBtnFunc.disabled(['iAdd', 'iCopy', 'iDel']);
				if(mainId != null) {
                    $("input[type='file']").not('[grid="true"]').prop("disabled", true);
                    setTimeout(function(){
                        //CKEDITOR.instances['descp'].setReadOnly(true);
                        //CKEDITOR.instances['slogan'].setReadOnly(true);
                        //CKEDITOR.instances['other_content'].setReadOnly(true);
                        
                    }, 500);                    
                }                    
                
                // var prodType = $("#prod_type").val();
                // prodNo = $("#prod_no").val();

                // if(prodType == "S") {
                //     $("[name='snDetail']").show();
                //     $("[name='prodDetail']").hide();
                //     $("[name='prodGift']").hide();
                //     $("[href='#tab_5']").click();
                // }
                // else {
                //     $("[name='snDetail']").hide();
                //     $("[name='snDetail_T']").hide();
                //     $("[name='snDetail_N']").hide();
                //     $("[name='prodDetail']").show();
                //     $("[href='#tab_4']").click();
                // }
                $("[name='prodDetail']").show();
                $("[href='#tab_4']").click();
                // getDetailGrid();
                // prodTypeSwitch(prodType);
			}

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.afterDel = function() {

			}

			formOpt.addFunc = function() {
                setTimeout(function(){
                    $("#single_shipment").val(single_shipment);
                    $("#vendor_nm").val(vendor_nm);
                    $("#action_sort").val(action_sort);
                    $("#sort").val(sort);
                    CKEDITOR.instances['content'].setReadOnly(false);
                    // CKEDITOR.instances['slogan'].setReadOnly(false);
                    // CKEDITOR.instances['other_content'].setReadOnly(false);

                    CKEDITOR.instances['content'].setData("");
                    // CKEDITOR.instances['slogan'].setData("");
                    // CKEDITOR.instances['other_content'].setData("");
                }, 500);
                
                @if(isset($prodType))
                $("#prod_type").val("{{$prodType}}");
                $("#prod_type").trigger("change");
                @endif
			}

			formOpt.editFunc = function() {
                setTimeout(function(){
                    CKEDITOR.instances['content'].setReadOnly(false);
                    // CKEDITOR.instances['slogan'].setReadOnly(false);
                    // CKEDITOR.instances['other_content'].setReadOnly(false);
                }, 500);
                
			}

			formOpt.copyFunc = function() {
                $('#jqxGrid').jqxGrid('clear');
                setTimeout(function(){
                    CKEDITOR.instances['content'].setReadOnly(false);
                    // CKEDITOR.instances['slogan'].setReadOnly(false);
                    // CKEDITOR.instances['other_content'].setReadOnly(false);

                    CKEDITOR.instances['content'].setData(editObj.descp);
                    // CKEDITOR.instances['slogan'].setData(editObj.slogan);
                    // CKEDITOR.instances['other_content'].setData(editObj.other_content);
                }, 500);
            }

            formOpt.beforeSave = function() {

            }

			formOpt.saveSuccessFunc = function(data) {
                var result = data.data;
                console.log(result);

			}

			var btnGroup = [

			];

            initBtn(btnGroup);


            $('a[href="#tab_3"]').on("click", function(){
                // var descp = CKEDITOR.instances['descp'].getData();
                // var slogan = CKEDITOR.instances['slogan'].getData();
                // var other_content = CKEDITOR.instances['other_content'].getData();

                // if(descp == "") {
                //     CKEDITOR.instances['descp'].setData(editObj.descp);
                // }

                // if(slogan == "") {
                //     CKEDITOR.instances['slogan'].setData(editObj.slogan);
                // }

                // if(other_content == "") {
                //     CKEDITOR.instances['other_content'].setData(editObj.other_content);
                // }
                
            });
            
        });

    </script>
@endsection