<html>

<style>
    table, th, td {
        border: 2px solid #000
    }
</style>
<table>
    <thead>
    <tr>
        <th colspan="20" style="text-align: center; background-color: #DCE6F1;">主檔資料</th>
        <th colspan="8" style="text-align: center; background-color: #EBF1DE;">明細資料</th>
    </tr>
    <tr>
        <th width="10" style="background-color: #DCE6F1;">商品編號</th>
        <th width="10" style="background-color: #DCE6F1;">銷售方式</th>
        <th width="10" style="background-color: #DCE6F1;">商品類別</th>
        <th style="background-color: #DCE6F1;">商品名稱</th>
        <th style="background-color: #DCE6F1;">副標題</th>
        <th width="12" style="background-color: #DCE6F1;">價格區間(起)</th>
        <th width="12" style="background-color: #DCE6F1;">價格區間(迄)</th>
        <th width="10" style="background-color: #DCE6F1;">付款方式</th>
        <th width="10" style="background-color: #DCE6F1;">送貨方式</th>
        <th width="20" style="background-color: #DCE6F1;">上架時間</th>
        <th width="20" style="background-color: #DCE6F1;">商品簡述</th>
        <th width="20" style="background-color: #DCE6F1;">商品描述</th>
        <th width="20" style="background-color: #DCE6F1;">支援地區</th>
        <th style="background-color: #DCE6F1;">折扣</th>
        <th style="background-color: #DCE6F1;">活動日期</th>
        <th style="background-color: #DCE6F1;">熱門商品</th>
        <th style="background-color: #DCE6F1;">主檔順序</th>
        <th style="background-color: #DCE6F1;">品牌</th>
        <th style="background-color: #DCE6F1;">商品圖片</th>
        <th style="background-color: #DCE6F1;">刪除主檔</th>
        <th style="background-color: #EBF1DE;">型號編號</th>
        <th style="background-color: #EBF1DE;">名稱</th>
        <th style="background-color: #EBF1DE;">庫存</th>
        <th style="background-color: #EBF1DE;">順序</th>
        <th style="background-color: #EBF1DE;">原價</th>
        <th style="background-color: #EBF1DE;">特價</th>
        <th style="background-color: #EBF1DE;">明細圖片</th>
        <th style="background-color: #EBF1DE;">刪除明細</th>
    </tr>
    </thead>
    <tbody>
        @foreach($prod as $row)
        <tr>
        <td>{{$row->prod_no}}</td>
        <td>{{$row->sell_type}}</td>
        <td>{{$row->cate_id}}</td>
        <td>{{$row->m_title}}</td>
        <td>{{$row->sub_title}}</td>
        <td>{{$row->f_price}}</td>
        <td>{{$row->t_price}}</td>
        <td>{{$row->pay_way}}</td>
        <td>{{$row->ship_way}}</td>
        <td>{{$row->added_on}}</td>
        <td>{{$row->slogan}}</td>
        <td>{{$row->descp}}</td>
        <td>{{$row->other_content}}</td>
        <td>{{$row->d_percent}}</td>
        <td>{{$row->action_date}}</td>
        <td>{{$row->is_hot}}</td>
        <td>{{$row->sort}}</td>
        <td>{{$row->brand}}</td>
        <td>{{$row->img1}}</td>
        <td>N</td>
        <td>{{$row->d_id}}</td>
        <td>{{$row->title}}</td>
        <td>{{$row->stock}}</td>
        <td>{{$row->seq}}</td>
        <td>{{$row->o_price}}</td>
        <td>{{$row->d_price}}</td>
        <td>{{$row->d_img1}}</td>
        <td>N</td>
        </tr>
        @endforeach
    </tbody>
</table>
</html>