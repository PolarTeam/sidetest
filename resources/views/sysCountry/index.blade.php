@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
      {{ trans('sysCountry.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">
          <li class="active">{{ trans('sysCountry.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('before_scripts')


<script>
var gridOpt = {};
gridOpt.enabledStatus = false;
gridOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/sys_country') }}";
gridOpt.dataUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/sys_country') }}";
gridOpt.createUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysCountry/create') }}";
gridOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/sysCountry') }}" + "/{id}/edit";
gridOpt.height = 800;

var btnGroup = [
  {
    btnId: "btnExportExcel",
    btnIcon: "fa fa-cloud-download",
    btnText: "{{ trans('common.exportExcel') }}",
    btnFunc: function () {
      $("#jqxGrid").jqxGrid('exportdata', 'xls', '{{ trans("sysCountry.titleName") }}');
    }
  },
  {
    btnId: "btnOpenGridOpt",
    btnIcon: "fa fa-table",
    btnText: "{{ trans('common.gridOption') }}",
    btnFunc: function () {
      $('#gridOptModal').modal('show');
    }
  },
  {
    btnId:"btnAdd",
    btnIcon:"fa fa-edit",
    btnText:"{{ trans('common.add') }}",
    btnFunc:function(){
      location.href= gridOpt.createUrl;
    }
  }, 
  {
    btnId:"btnDelete",
    btnIcon:"fa fa-trash-o",
    btnText:"{{ trans('common.delete') }}",
    btnFunc:function(){
      alert("Delete");
    }
  }
];
</script>
@endsection

@include('backpack::template.search')
