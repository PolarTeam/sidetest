@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
	工程進度<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url(config('backpack.base.route_prefix'),'OrderMgmt') }}">工程進度</a></li>
		<li class="active">工程進度</li>
	</ol>
</section>
<script>
	var single_shipment = "";
    var vendor_nm = "";
    var vendor_cd = "";
    var action_sort =0;
    var sort =0;
    @if(isset($newData))
        single_shipment = "N";
        sort = 99;
        action_sort = 99;
        vendor_cd = "{{$newData->email}}";
        vendor_nm = "{{$newData->name}}";
	@endif

</script>
@endsection 

@section('content')
	@include('backpack::template.toolbar')
	<div class="row">
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4>{{ trans('backpack::crud.please_fix') }}</h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">銷售個案</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="box-body">
								<div class="row">
                                <div class="form-group col-md-3">
                                            <label for="title">名稱</label>
                                            <input type="text" class="form-control" id="title" name="title">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="addr">縣市區域/位置</label>
                                            <input type="text" class="form-control" id="addr" name="addr">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="years">年份</label>
                                            <input type="text" class="form-control" id="years" name="years">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="structure">建案資訊</label>
                                            <select class="form-control select2" data-placeholder="請選擇" style="width: 100%;" name="structure[]"></select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="img1">列表縮圖</label>
                                            <input type="file" name="img1" id="img1"> 
                                        </div>
                                    </div>
                                <div class="row">
                                    <div class="form-group col-md-3">

                                        @if(isset($id) && $crud->entry['original']['img1'])
                                        <a name="img1" href="{{Storage::url($crud->entry['original']['img1'])}}" target="_blank">
                                            <img name="img1" src="{{Storage::url($crud->entry['original']['img1'])}}" alt="" height="100">
                                        </a>
                                        @else
                                            <a name="img1" href="#" target="_blank">
                                                <img name="img1" src="#" alt="" height="100" style="display:none">
                                            </a>
                                        @endif
                                    </div>
                                </div>

								@if(isset($id))
								<input type="hidden" name="id" value="{{$id}}" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								@endif
                            </div>
                        </div>                        
                    </div>
				</div>
			</form>
		</div>	
    </div>


    <div class="row">

            <div class="col-md-12">
                <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
                    <ul class="nav nav-tabs">
                        <li name="prodDetail" class="active">
                            <a href="#tab_4" data-toggle="tab" aria-expanded="false">年份</a>
                        </li>
                    </ul>
                    
                    <div class="tab-content">
                        <!-- /.tab-pane -->
                        <div class="tab-pane active" id="tab_4" name="prodDetail">
                            <div class="box box-primary" id="subBox" style="display:none">
                                <div class="box-header with-border">
                                <h3 class="box-title">內容</h3>
                                </div>
                                <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                            <div class="form-group col-md-3">
                                                <label for="img_descp1">敘述</label>
                                                <input type="text" class="form-control" name="img_descp1" grid="true">
                                            </div> 
                                    </div>
                                </div>
                                
                                <!-- /.box-body -->
        
                                <div class="box-footer">
                                    <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                                    <button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
                                </div>
                                </form>
                            </div>
                            
                            <div id="jqxGrid"></div>
                        </div>

                        
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
            
        </div>
@endsection 
@include('backpack::template.lookup') 
@section('after_scripts')
<script src="{{ asset('vendor/backpack/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/backpack/ckeditor/adapters/jquery.js') }}"></script>
	<script>
        var mainId = "";
        var prodNo = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/process') }}";

		var fieldData = null;
        var fieldObj = null;
        
        var imgPath = "{{Storage::url('/')}}";

		@if(isset($crud -> create_fields))
		fieldData = '{{!! json_encode($crud->create_fields) !!}}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		@endif

		@if(isset($id))
		mainId   = "{{$id}}";
		editData = '{{!! $entry !!}}';
		editData = editData.substring(1);
		editData = editData.substring(0, editData.length - 1);
        var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
        console.log(editData);
		//editObj  = JSON.parse(objJson);
		@endif

		$(function () {

            function prodTypeSwitch(prodType) {
                if(prodType == "S") {
                    $("#countryArea").hide();
                    $("#tPriceArea").hide();
                }
                else {
                    $("#countryArea").show();
                    $("#tPriceArea").show();
                }
            }


            $("#prod_type").on("change", function(){
                if($(this).val()=="S"){
                    $("#create_inv").val("N");
                }else{
                    $("#create_inv").val("Y");
                }
                prodTypeSwitch($(this).val());
            });

			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/process') }}";
			formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_process') }}/" + mainId;
			formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/process') }}";
			formOpt.afterInit = function() {
                $("#years").datepicker({
                    format: "yyyy",
                    viewMode: "years", 
                    minViewMode: "years",
                    autoclose:true //to close picker once year is selected
                });
				if(mainId != null) {
                    // $("input[type='file']").not('[grid="true"]').prop("disabled", true);
                    setTimeout(function(){
                        //CKEDITOR.instances['descp'].setReadOnly(true);
                        //CKEDITOR.instances['slogan'].setReadOnly(true);
                        //CKEDITOR.instances['other_content'].setReadOnly(true);
                        
                    }, 500);                    
                }                    
                
                // var prodType = $("#prod_type").val();

                getDetailGrid();
                // prodTypeSwitch(prodType);
			}

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.afterDel = function() {

			}

			formOpt.addFunc = function() {
                setTimeout(function(){
                }, 500);
                
                @if(isset($prodType))
                $("#prod_type").val("{{$prodType}}");
                $("#prod_type").trigger("change");
                @endif
			}

			formOpt.editFunc = function() {
                setTimeout(function(){

                }, 500);
                
			}

			formOpt.copyFunc = function() {
                $('#jqxGrid').jqxGrid('clear');
                setTimeout(function(){
                }, 500);
            }

            formOpt.beforeSave = function() {

            }

			formOpt.saveSuccessFunc = function(data) {
                var result = data.data;
                console.log("nick");
                console.log(result);
                if(result.img1 != "" && result.img1 != null) {
                    $("img[name='img1']").attr("src", ROOT_URL+"/uploads/"+result.img1).show();
                    $("a[name='img1']").attr("href", ROOT_URL+"/uploads/"+result.img1);
                }

                // if(result.img2 != null) {
                //     $("img[name='img2']").attr("src", result.img2).show();
                //     $("a[name='img2']").attr("href", result.img2);
                // }

                // if(result.img3 != null) {
                //     $("img[name='img3']").attr("src", result.img3).show();
                //     $("a[name='img3']").attr("href", result.img3);
                // }

                // if(result.img4 != null) {
                //     $("img[name='img4']").attr("src", result.img4).show();
                //     $("a[name='img4']").attr("href", result.img4);
                // }

                // if(result.img5 != null) {
                //     $("img[name='img5']").attr("src", result.img5).show();
                //     $("a[name='img5']").attr("href", result.img5);
                // }

                // CKEDITOR.instances['descp'].setReadOnly(true);
                // CKEDITOR.instances['slogan'].setReadOnly(true);
                // CKEDITOR.instances['other_content'].setReadOnly(true);
                $("input[type='file']").not('[grid="true"]').prop("disabled", true);
                $("input[type='file']").val("");
			}

			var btnGroup = [

			];

            initBtn(btnGroup);


            $('a[href="#tab_3"]').on("click", function(){
                var descp = CKEDITOR.instances['descp'].getData();
                var slogan = CKEDITOR.instances['slogan'].getData();
                var other_content = CKEDITOR.instances['other_content'].getData();

                if(descp == "") {
                    CKEDITOR.instances['descp'].setData(editObj.descp);
                }

                if(slogan == "") {
                    CKEDITOR.instances['slogan'].setData(editObj.slogan);
                }

                if(other_content == "") {
                    CKEDITOR.instances['other_content'].setData(editObj.other_content);
                }
                
            });
            
        });
        function getDetailGrid() {
            var imagerenderer = function (row, datafield, value) {
                if(value != "") {
                    return '<img style="margin-left: 5px;" height="60" width="50" src="'+ROOT_URL+'/uploads/' + value.replace('public/', '') + '"/>';
                }

                return "";
            }

            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "process_id", type: "string"},
                    {name: "cate_nm", type: "string"},
                    {name: "img_descp1", type: "string"},
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true},
                    {text: "process_id", datafield: "process_id", width: 150, hidden: true},
                    {text: "敘述", datafield: "img_descp1", width: 150},
                ]
            ];
            var opt = {};
            if(mainId==""){
                mainId="0";
            }
            opt.gridId = "jqxGrid";
            opt.fieldData = col;
            opt.formId = "subForm";
            opt.saveId = "Save";
            opt.cancelId = "Cancel";
            opt.showBoxId = "subBox";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/processDetail') }}"+ "/"+ mainId;;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/processDetail') }}" + "/detailStore";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/processDetail') }}" + "/update/"+ mainId;
            opt.delUrl  = "{{ url(config('backpack.base.route_prefix', 'admin') . '/processDetail') }}" + "/delete/";
            opt.defaultKey = {'process_id': mainId};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.rowsheight = 60;
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                console.log(data);
            }

            $("[name='prodDetail']").show();
            $("[href='#tab_4']").click();
            genDetailGrid(opt);
        }
        

    </script>
<script>
    function tab_4() {
        document.getElementById('jqxGrid').style.display = 'block';
        document.getElementById('jqxGrid1').style.display = 'none';
    }
    function tab_5() {
        document.getElementById('jqxGrid').style.display = 'none';
        document.getElementById('jqxGrid1').style.display = 'block';
    }
</script>
@endsection