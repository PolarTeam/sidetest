@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
	首頁<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url(config('backpack.base.route_prefix'),'menu') }}">首頁</a></li>
		<li class="active">首頁</li>
	</ol>
</section>
<script>
	var single_shipment = "";
    var vendor_nm = "";
    var vendor_cd = "";
    var action_sort =0;
    var sort =0;
    @if(isset($newData))
        single_shipment = "N";
        sort = 99;
        action_sort = 99;
        vendor_cd = "";
        vendor_nm = "";
	@endif

</script>
@endsection 

@section('content')
	@include('backpack::template.toolbar')
	<div class="row">
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4>{{ trans('backpack::crud.please_fix') }}</h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">首頁</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="box-body">
								<div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="title">標題</label>
                                        <input type="text" class="form-control" id="title" name="title">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="tags">標籤</label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select a Type" style="width: 100%;" name="tags[]">
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="tags">類別</label>
                                        <select class="form-control" id="" name="type">
                                            <option value="img">圖片</option>
                                            <option value="video">影片</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="sorted">排序</label>
                                        <input type="number" class="form-control input-sm" name="sorted" grid="sorted" >
                                    </div> 
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="img1">圖片(盡量200k以下) / 影片(盡量5mb以下)</label>
                                        <input type="file" name="img1" id="img1"> 
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        @if(isset($id) && $crud->entry['original']['img1'])
                                        <a name="img1" href="{{Storage::url($crud->entry['original']['img1'])}}" target="_blank">
                                            <img name="img1" src="{{Storage::url($crud->entry['original']['img1'])}}" alt="" height="100">
                                        </a>
                                        @else
                                            <a name="img1" href="#" target="_blank">
                                                <img name="img1" src="#" alt="" height="100" style="display:none">
                                            </a>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-3">
                                        @if(isset($id) && $crud->entry['original']['img2'])
                                        <a name="img2" href="{{Storage::url($crud->entry['original']['img2'])}}" target="_blank">
                                            <img name="img2" src="{{Storage::url($crud->entry['original']['img2'])}}" alt="" height="100">
                                        </a>
                                        @else
                                            <a name="img2" href="#" target="_blank">
                                                <img name="img2" src="#" alt="" height="100" style="display:none">
                                            </a>
                                        @endif
                                    </div>
                                </div>

                                @if(isset($id))
                                <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                <input type="hidden" name="_method" value="PUT" class="form-control"> 
                                @endif
                            </div>
                    </div>
                </div>
            </div>
        </form>
    </div>	
</div>


<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
            <ul class="nav nav-tabs">
                <li name="prodDetail" class="active">
                    <a href="#tab_4" data-toggle="tab" aria-expanded="false">明細</a>
                </li>
            </ul>
            
            <div class="tab-content">
                <!-- /.tab-pane -->
                <div class="tab-pane active" id="tab_4" name="prodDetail">
                    <div class="box box-primary" id="subBox" style="display:none">
                        <div class="box-header with-border">
                        <h3 class="box-title">明細</h3>
                        </div>
                        <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="intro">介紹</label>
                                    <input class="form-control input-sm" name="intro" grid="intro" >
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="title">標題</label>
                                    <input class="form-control input-sm" name="title" grid="title" >
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="type">類別</label>
                                    <select class="form-control" id="type" name="type">
                                        <option value="img">圖片</option>
                                        <option value="video">影片</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="rank">排序</label>
                                    <input type="number" class="form-control input-sm" name="rank" grid="true" >
                                </div> 
                                <div class="form-group col-md-3">
                                    <label for="img1">圖片(盡量200k以下) / 影片(盡量5mb以下)</label>
                                    <input type="file" name="img1" id='image' grid="true"> 
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="tags">標籤</label>
                                    <select class="form-control select2" multiple="multiple" style="width: 100%;" name="tags" id="detailtags">
                                        @foreach($tags as $row)
                                            <option value="{{$row->code}}">{{$row->descp}}</option>
                                        @endforeach	
                                    </select>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <input type="hidden" class="form-control input-sm" name="fortest" id = "fortest" grid="true" >
                            <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                            <button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
                            <button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
                        </div>
                        </form>
                    </div>
                    
                    <div id="jqxGrid"></div>
                </div>
                <!-- /.tab-pane -->

                <!-- /.tab-pane -->
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
    </div>
</div>

@endsection 
@include('backpack::template.lookup') 
@section('after_scripts')
<script src="{{ asset('vendor/backpack/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/backpack/ckeditor/adapters/jquery.js') }}"></script>
	<script>
        var mainId = "";
        var prodNo = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/menu') }}";

		var fieldData = null;
        var fieldObj = null;
        
        var imgPath = "{{Storage::url('/')}}";

		@if(isset($crud -> create_fields))
		fieldData = '{{!! json_encode($crud->create_fields) !!}}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		@endif

		@if(isset($id))
		mainId   = "{{$id}}";
		editData = '{{!! $entry !!}}';
		editData = editData.substring(1);
		editData = editData.substring(0, editData.length - 1);
        var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
        console.log(editData);
		//editObj  = JSON.parse(objJson);
		@endif

		$(function () {

			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/menu') }}";
			formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_team') }}/" + mainId;
			formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/menu') }}";
			formOpt.afterInit = function() {
				if(mainId != null) {
                    // $("input[type='file']").not('[grid="true"]').prop("disabled", true);
                    setTimeout(function(){
                        //CKEDITOR.instances['descp'].setReadOnly(true);
                        //CKEDITOR.instances['slogan'].setReadOnly(true);
                        //CKEDITOR.instances['other_content'].setReadOnly(true);
                        
                    }, 500);                    
                }                    
                
                getDetailGrid();
                getDetailGrid_type();
                // getBaseinfoGrid();
                // prodTypeSwitch(prodType);
			}

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.afterDel = function() {

			}

			formOpt.addFunc = function() {
                setTimeout(function(){
                    // $("#single_shipment").val(single_shipment);
                    // $("#vendor_nm").val(vendor_nm);
                    // $("#action_sort").val(action_sort);
                    // $("#sort").val(sort);
                }, 500);
			}

			formOpt.editFunc = function() {

                
			}

			formOpt.copyFunc = function() {
                $('#jqxGrid').jqxGrid('clear');
                setTimeout(function(){
                }, 500);
            }

            formOpt.beforeSave = function() {

            }

			formOpt.saveSuccessFunc = function(data) {
                var result = data.data;
                console.log(result);
                if(result.img1 != "" && result.img1 != null) {
                    $("img[name='img1']").attr("src", ROOT_URL+"/uploads/"+result.img1).show();
                    $("a[name='img1']").attr("href", ROOT_URL+"/uploads/"+result.img1);
                }
                if(result.img2 != "" && result.img2 != null) {
                    $("img[name='img2']").attr("src", ROOT_URL+"/uploads/"+result.img2).show();
                    $("a[name='img2']").attr("href", ROOT_URL+"/uploads/"+result.img2);
                }
                $("input[type='file']").not('[grid="true"]').prop("disabled", true);
                $("input[type='file']").val("");
			}

			var btnGroup = [

			];

            initBtn(btnGroup);
            
        });

        function getDetailGrid_type() {
            var imagerenderer = function (row, datafield, value) {
                if(value != "") {
                    return '<img style="margin-left: 5px;" height="60" width="50" src="'+ROOT_URL+'/uploads/' + value.replace('public/', '') + '"/>';
                }
                return "";
            }

        }

        function getDetailGrid() {
            var imagerenderer = function (row, datafield, value) {
                if(value != "") {
                    return '<img style="margin-left: 5px;" height="60" width="50" src="'+ROOT_URL+'/uploads/'+ value.replace('public/uploads/', 'uploads/') + '"/>';
                }

                return "";
            }
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "menu_id", type: "string"},
                    {name: "tags", type: "string"},
                    {name: "img1", type: "string"},
                    {name: "intro", type: "string"},
                    {name: "type", type: "string"},
                    {name: "title", type: "string"},
                    {name: "rank", type: "number"},
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true},
                    {text: "team_id", datafield: "team_id", width: 150, hidden: true},
                    {text: "圖片(盡量200k以下) / 影片(盡量5mb以下)", datafield: "img1", width: 150, cellsrenderer: imagerenderer},
                    {text: "標題", datafield: "title", width: 150},
                    {text: "介紹", datafield: "intro", width: 150},
                    {text: "排序", datafield: "rank", width: 150},
                    {text: "標籤", datafield: "tags", width: 150},
                ]
            ];
            var opt = {};
            if(mainId==""){
                mainId="0";
            }
            opt.gridId = "jqxGrid";
            opt.fieldData = col;
            opt.formId = "subForm";
            opt.saveId = "Save";
            opt.cancelId = "Cancel";
            opt.showBoxId = "subBox";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/menuDetail') }}"+ "/"+ mainId;;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/menuDetail') }}" + "/detailStore";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/menuDetail') }}" + "/update/"+ mainId;
            opt.delUrl  = "{{ url(config('backpack.base.route_prefix', 'admin') . '/menuDetail') }}" + "/delete/";
            opt.defaultKey = {'menu_id': mainId};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.rowsheight = 60;
            opt.beforeSave = function(formData) {
                var temp = $('#detailtags').val().toString();
                 $('#fortest').val(temp);
            }

            opt.afterSave = function(data) {
                console.log(data);
            }
            genDetailGrid(opt);

            $('#detailtags').select2();
        }
        
    </script>

<script>

$(function(){
    formOpt.initFieldCustomFunc();
    $.get( formOpt.fieldsUrl , function( data ) {
        if(typeof formOpt.afterInit === "function") {
            if(data.img1 != undefined) {
                $("img[name='img1']").attr("src", ROOT_URL+"/uploads/"+data.img1).show();
                $("a[name='img1']").attr("href", ROOT_URL+"/uploads/"+data.img1);
            }
            if(data.img2 != undefined) {
                $("img[name='img2']").attr("src", ROOT_URL+"/uploads/"+data.img2).show();
                $("a[name='img2']").attr("href", ROOT_URL+"/uploads/"+data.img2);
            }
        }
    });
});
</script>
@endsection