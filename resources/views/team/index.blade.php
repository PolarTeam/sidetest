@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        首頁<small></small>
      </h1>
      <ol class="breadcrumb">
      <li class="active">首頁</li>
      </ol>
    </section>
@endsection

@section('before_scripts')


<script>
var gridOpt = {};
gridOpt.pageId        = "menu";
gridOpt.enabledStatus = false;
gridOpt.searchOpt     = true;
gridOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_team') }}";
gridOpt.dataUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_team') }}";
gridOpt.createUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/menu/create') }}";
gridOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/menu') }}" + "/{id}/edit";

var btnGroup = [
  {
    btnId: "btnSearchWindow",
    btnIcon: "fa fa-search",
    btnText: "{{ trans('common.search') }}",
    btnFunc: function () {
        $('#searchWindow').jqxWindow('open');
    }
  },
  {
    btnId:"btnAdd",
    btnIcon:"fa fa-edit",
    btnText:"{{ trans('common.add') }}",
    btnFunc:function(){
      location.href= gridOpt.createUrl;
    }
  }, 
  {
    btnId:"btnDelete",
    btnIcon:"fa fa-trash-o",
    btnText:"{{ trans('common.delete') }}",
    btnFunc:function(){
          var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
          var ids = new Array();
          for (var m = 0; m < rows.length; m++) {
          var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
          if(typeof row != "undefined") {
              ids.push(row.id);
          }
          }
          if(ids.length == 0) {
          swal("請至少選擇一筆資料", "", "warning");
          }
          $.post(BASE_URL + '/menu/multi/del', {'ids': ids}, function(data){
              if(data.msg == "success") {
                  swal("刪除成功", "", "success");
                  $("#jqxGrid").jqxGrid('updatebounddata');
              }
              else{
                  swal("操作失敗", "", "error");
              }
          });
      }
  }
];
</script>
@endsection

@include('backpack::template.search')
