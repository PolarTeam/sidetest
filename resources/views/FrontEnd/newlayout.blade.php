<html itemscope itemtype="http://schema.org/Article">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>{{$seo_title or ""}}-ZI 你愛的生活品味</title>
    <meta name="description" content="{{$seo_desc or "ZI 你愛的生活品味" }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{$seo_title or ""}}-ZI 你愛的生活品味">
    <meta itemprop="description" content="{{$seo_desc or "ZI 你愛的生活品味"}}">
    <meta itemprop="image" content="{{ $seo_img or "ZI 你愛的生活品味"}}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@beautyz_net">
    <meta name="twitter:title" content="{{$seo_title or ""}}-ZI 你愛的生活品味">
    <meta name="twitter:description" content="{{$seo_desc or "ZI 你愛的生活品味"}}">
    <meta name="twitter:creator" content="@beautyz_net">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="{{ $seo_img or ""}}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{$seo_title or ""}}-ZI 你愛的生活品味" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:image" content="{{ $seo_img or ""}}" />
    <meta property="og:description" content="{{$seo_desc or "ZI 你愛的生活品味"}}" />
    <meta property="og:site_name" content="ZI 你愛的生活品味" />
    <meta property="article:published_time" content="2018-04-15T05:59:00+01:00" />
    <meta property="article:modified_time" content="2018-04-15T05:59:00+01:00" />
    <meta property="article:section" content="{{ $seo_section or  "ZI 你愛的生活品味"}}" />
    <meta property="article:tag" content="{{ $seo_tag or  "ZI 你愛的生活品味"}}" />
    <meta property="fb:admins" content="100000307128936" />
    <link rel="shortcut icon" href="{{ url('assets/images/zi.png') }}">
    @yield('after_style')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

    <script>
        var BASE_URL = "{{url('/')}}";
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117435537-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-117435537-1');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script>

</head>

<body>
    @yield('before_scripts')
        
    @yield('header')
    
    @yield('content')

    @yield('footer')
        
    <script type="text/javascript" src="{{url('assets/js/common.js')}}"></script>
    @yield('after_scripts')
    
</body>
</html>
