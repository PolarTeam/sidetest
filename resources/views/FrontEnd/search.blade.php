<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-TW">
<head>
	<meta charset="UTF-8">
	<title>抽獎序號 / 中獎資訊 查詢</title>
	<meta name='description' content=""/>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name='keywords' content=""/>
	<meta name="MobileOptimized" content="width">
	<meta name="HandheldFriendly" content="true">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{url('assets/css/style.css?')}}">
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="{{url('assets/js/goto.js')}}"></script>
	<script src=’https://www.google.com/recaptcha/api.js’></script>
</head>
<body>
	<header class="header-box">
		<div class="content-box inside-box clearfix">
			<a class="home" href="{{url('/')}}"title="回首頁"></a>
			<h3 class="title">抽獎序號 / 中獎資訊 查詢</h3>
		</div>
	</header>
	<main class="main-box">
		<section class="info-box form-box">
			<ul class="day-select">
				<li>
					<input id="option" type="radio" name="field" value="2019-05-20 到 2019-07-20期 福袋" checked >
					<label for="option">2019-05-20 到 2019-07-20期 福袋</label>
				</li>
				<!-- <li>
					<input id="option" type="radio" name="field" value="">
					<label for="option">2019-05-01 到 2019-07-31期 福袋</label>
				</li> -->
			</ul>
			@if(!$dlv_nm)
			<div class="content-box">
				<form class="shoppingcar_form_box" action="{{url('search_list')}}" method="post" id="contentform">
				{{ csrf_field() }}
					<ul class="fillin">
						<li>
							<label for="name">購買人姓名</label>
							<input id="name" type="text" name="dlv_nm" value="" placeholder="請輸入購買人姓名">
						</li>
						<li>
							<label for="phone">購買人手機號碼</label>
							<input id="phone" type="text" name="dlv_phone" value="" placeholder="請輸入購買人手機號碼">
						</li>
						<li>
							<!-- <label for="code">驗證碼</label>
							<input id="code" type="text" name="" value="" placeholder="請輸入右方的4位數字">
							<div class="code-box"><img src="images/rand.png"></div> -->
							<div class="verificationcode-box">
							<div class="form-group">
								<small>你是機器人嗎?</small>
								<div class="g-recaptcha" data-sitekey="6LdfdKEUAAAAABiXJMUCKpuIbF0vmqsusilHwWXP"></div>
							</div>
					</div>
						</li>
					</ul>
					<input class="btn" type="submit" value="送出查詢">
				</form>
			</div>
			@endif
			@if($dlv_nm)
			<div class="content-box" id = "result" >
				<form class="shoppingcar_form_box" method="post" id="check">
					{{ csrf_field() }}
					
					<p>{{$dlv_nm}}您好你所購買的福袋資訊如下</p>
					@foreach($snData as $key=>$row)
					<table width="100%">
					　<tr>
					　<td align="left">{{($row->prod_nm)}}</td>
					　<td align="right" width>{{($row->sn_no)}}</td>
					　</tr>
					</table>
					@endforeach
				</form>
			</div>
			@endif
		</section>
		<footer class="footer_box none_pad">
			<ul class="link clearfix">
				<li><a href="{{url('search')}}"><p class="icon_01">抽獎序號/<br>中獎查詢</p></a></li>
				<li><a href="https://www.facebook.com/%E6%99%B6%E8%8F%AF%E9%A4%8A%E7%94%9F%E6%9C%83%E9%A4%A8-348842531964050/?__tn__=kC-R&eid=ARCLVy2JVRLEqld-VCgaq2BKWer4N5cnKHJ2fdVsIvTPUN3Qy14R0EtWIz-aD5eTs7NN4OaNTPIDusUg&hc_ref=ARQGaf0hVNpKUbAs6VfmEtKQ-S6PVdP11d7GXt1dPB7L5NaehZ0QN87d1QnRL9WOiGc&fref=tag&__xts__%5B0%5D=68.ARBZlFscLop11qD0bjFn8_1RmiCfMUrS7C3-HmxP0gm4VsC8S-cze9gDYtMgQkxxjDyrw8z38fs_BRLJHyqQ338eeMsDU_iMOCw-xxVcFOC_WOxtKl7KiNAhCCRkr_ttNpt_NkicxP4C6HIoHcEm4RyYpuKNFt1TSErdtDoPEhd6MKvURp8mFn3cT15pzYbzjr8X2DX4y2P2NBYBGivpY11q7LT0407vihRavzb_Qmh5P7o5KO11l_xyulK1_WE9S1mmR3Cq04UWs1rO8xeIMocbVNbdhriFNLyW" target="_blank"><p class="icon_02">facebook<br>粉絲團</p></a></li>
				<li><a href="{{url('/')}}#ruleinfo"><p class="icon_03">隱私權條款</p></a></li>
			</ul>
			<p class="Copyright">本活動由林懿君 律師 為第三方公證人<br>
				© 2019 Copyright      客服電話 03-357-9199#510
			</p>
		</footer>
	</main>
</body>
</html>



