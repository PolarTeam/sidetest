@extends('FrontEnd.layout',[
	"seo_title" => "會員專區",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("會員專區")),
	"seo_img" => null
])
@section('after_style')
    <link rel="stylesheet" href="{{url('assets/fonts/FontAwesome/font-awesome.css')}}">
    <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="{{url('assets/css/bootsnav.css?')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/newstyle.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/rwd.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/orderList.css')}}">

    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script src="{{url('assets/js/bootstrap.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
    @if(Session::get('message'))
    <script>
        $(function(){
            Swal.fire({
                type: 'warning',
                title: '{{Session::get('message')}}',
            });
        })
        
    </script>
    @endif

    @if($errors->any())
    <script>
        $(function(){
            var errorMsg = "{{$errors->all()[0]}}";
            Swal.fire({
                type: 'warning',
                title: errorMsg,
            });
        })
    </script>
    @endif
@endsection
@section('header')
    @include('FrontEnd.layouts.newHeader')
@endsection
@section('content')
<main class="main_center">
    <section class="top_item">
        <ul class="contents_box clearfix">
            <li class=""><p><a href="{{url('center')}}">我的訂單</a></p></li>
            <li class="btn_in"><p><a href="{{url('memberInfo')}}">會員資料</a></p></li>
        </ul>
    </section>
    <!-- 基本資料 - 內容 -->
    <!-- <div class="tab-content" id="nav-tabContent"> -->
    <div class="tab-pane active" id="nav-1" role="tabpanel" aria-labelledby="nav-1-tab">
    <section class="main_center" id="user_info" style="padding-top: 0px; min-height: 0%">
        <form class="userinfo_form_box" role="form" method="POST" action="{{ url('member/update') }}">
            {{ csrf_field() }}
            <h2 class="top_title">基本資料</h2>
            <div class="group clearfix">
                <label class="left_label">姓名</label>
                <div class="right_fillbox">
                    <input class="fbox w_02" type="text" name="name" value="{{$userData->name}}" placeholder="姓名">
                </div>
            </div>
            <div class="group clearfix">
                <label class="left_label">手機號碼</label>
                <div class="right_fillbox">
                    <input class="fbox w_02" type="text" name="cellphone" value="{{$userData->cellphone}}" required>
                </div>
            </div>
            <div class="group clearfix">
                <label class="left_label">Email</label>
                <div class="right_fillbox">
                    <p>{{$userData->email}}</p>
                </div>
            </div>
            <div class="group clearfix">
                <label class="left_label">收件地址</label>
                <div class="right_fillbox">
                    <div class="box3 clearfix">
                        <input class="fbox w_00" type="text" name="dlv_zip" placeholder="區碼" value="{{$userData->dlv_zip}}">
                        <div class="styled-select w_01">
                            <select class="md-input form-control" name="dlv_city" id="dlv_city">
                                <option value="" selected="">請選擇縣市</option>
                                @foreach($cityData as $row)
                                <option value="{{$row->city_nm}}" @if($userData->dlv_city == $row->city_nm) selected @endif>{{$row->city_nm}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="styled-select w_01">
                            <select class="md-input form-control" name="dlv_area" id="dlv_area">
                                <option value="" selected="">請選擇鄉鎮市區</option>
                                @foreach($areaData as $row)
                                    <option zip="{{$row->zip_f}}" value="{{$row->dist_nm}}" @if($userData->dlv_area == $row->dist_nm) selected @endif>{{$row->dist_nm}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input class="fbox" type="text" name="dlv_addr" placeholder="請輸入街道地址" value="{{$userData->dlv_addr}}" required>
                    <!-- <p class="p_note">您尚未輸入街道地址！</p> -->
                </div>
            </div>
            <h2 class="top_title">變更密碼</h2>
             <!-- <div class="group clearfix">
                <label class="left_label">舊密碼</label>
                <div class="right_fillbox">
                    <input class="fbox w_02" type="text" name="" placeholder="請輸入舊密碼">
                </div>
            </div> -->
            <div class="group clearfix">
                <label class="left_label">新密碼</label>
                <div class="right_fillbox">
                    <div class="both">
                        <input class="fbox w_02" type="password" name="password" placeholder="請輸入新密碼">
                        <input class="fbox w_02" type="password" name="confirm_password" placeholder="請再次輸入新密碼">
                    </div>
                    <!-- <p class="p_note p_gray">最少需輸入6個字符。</p> -->
                </div>
            </div>
        </form>
    </section>
    <!-- </div> -->
    <p class="bt_btn"><a class="btn_org goto_step1 submit">儲存修改</a></p>
    </div>
</main>
@endsection

@section('footer')
    @include('FrontEnd.layouts.newFooter')
@endsection


@section('after_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
    crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
<script>
    $(function(){
        $("#dlv_city").on("change", function(){
            var val = $(this).val();

            $.get(BASE_URL + '/getAreaByCity', {'dlv_city': val}, function(data){
                if(data.status == "success") {
                    var areaData = data.areaData;
                    var str = '<option value="" selected>請選擇鄉鎮市區</option>';
                    for(i in areaData) {
                        str += '<option zip="'+areaData[i]["zip_f"]+'" value="'+areaData[i]["dist_nm"]+'">'+areaData[i]["dist_nm"]+'</option>';
                    }

                    $("#dlv_area").html(str);
                }
            });
        });

        $("#dlv_area").on("change", function(){
            var zip = $("#dlv_area option:selected").attr("zip");
            $("#dlv_zip").val(zip);
        });

        $(".reSendSn").on("click", function(){
            $("#snModal").modal('show');
            var orderDetailId = $(this).attr('order-detail-id');
            var ordNo = $(this).attr("ordNo");

            $("#snOrdNo").text(ordNo);

            $.get(BASE_URL + '/getSn/' + orderDetailId, function(data){
                if(data.status == "success") {
                    var sn = "";
                    for(var i=0; i < data.sn.length; i++) {
                        sn += data.sn[i] + "\n";
                    }
                    $("#sn").val(sn);
                    return;
                }
            }, 'JSON');
        });

        $(".submit").click(function(){
            $(".userinfo_form_box").submit();
        });

        $(".userinfo_form_box").on("submit", function(){
            const dlvAddr = $("input[name='dlv_addr']").val();
            const phone = $("input[name='cellphone']").val();
            if(dlvAddr == "") {
                Swal.fire({
                    type: 'warning',
                    title: '請輸入地址',
                });

                return false;
            }

            if(phone == "") {
                Swal.fire({
                    type: 'warning',
                    title: '請輸入電話',
                });

                return false;
            }
        });
    });
</script>
@endsection




