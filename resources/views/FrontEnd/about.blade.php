@extends('FrontEnd.layout',[
	"seo_title" => "關於我們",
	"seo_desc" => preg_replace("/\n+/","",strip_tags("美z.人生旅遊網沒有「超低費率」這種產品。如果你要便宜，請勿選擇我們，我們的費率絕對不會是最便宜的!")),
	"seo_img" => null
])

@section('after_style')
<link rel="stylesheet" href="{{url('assets/css/reset.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('assets/libs/bootstrap-3.3.7.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/aboutus.css')}}" type="text/css" media="screen">    
    
    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/tether.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/bootstrap-3.3.7.min.js')}}"></script>
@endsection

@section('header')
    @include('FrontEnd.layouts.header')
@endsection

@section('content')

<div id="content-box">            
    <div class="video-box">
        <video id="movie" autoplay loop preload>
            <source src="./assets/mp4/2.mp4" type="video/mp4"/>
            您的瀏覽器不支援HTML 5影片播放標籤 video 格式。
            Your browser doesn't support the video tag.
        </video>
        <div class="desc-box">
            {!! nl2br($baseData->about_us) !!}
        </div>
    </div>
    
    <!--  <div class="desc-box">
        <p>美z.人生旅遊沒有「超低費率」這種產品。<br>如果你要便宜，請勿選擇我們，我們的費率絕對不會是最便且的!</p>
        <p>台灣各行各業已經受夠低薪帶來的影響，<br>我不希望我們公司也成為低薪兇手的一員。</p>
        <p>我們希望做到業界員工薪水最高，這很不容易，<br>但是我們可以一起努力。</p>
        <p>你們之中，可能有人一年之內已經出國數次。<br>
        但也可能有人辛苦一整年難得帶著家人出國走走散心。<br>
        這樣難得又美好的時光，<br>
        我們希望各位在使用我們的WIFI機出國時，<br>
        能讓大家回憶起這份美好!</p>
        <p>感受到我們優質強大的服務，<br>
        讓你隨時可以上傳直撥、拍照打卡，<br>
        紀錄豐富你的網路生活。</p>
    </div> -->
    
</div>

@endsection

@section('footer')
    @include('FrontEnd.layouts.footer')
@endsection

@section('after_scripts')
<script type="text/javascript" src="{{url('assets/js/aboutus.js')}}"></script>
<script type="text/javascript" src="{{url('assets/js/verificationcode.js')}}"></script>
@endsection
