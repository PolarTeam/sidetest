@extends('FrontEnd.layout')

@section('after_style')
    <link rel="stylesheet" href="{{url('assets/css/reset.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('assets/libs/bootstrap-3.3.7.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{url('assets/css/home.css')}}" type="text/css" media="screen">    
    
    <script type="text/javascript" src="{{url('assets/libs/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/tether.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/libs/bootstrap-3.3.7.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
@endsection

@section('header')
    @include('FrontEnd.layouts.header')
@endsection

@section('content')
<div id="content-box-1">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">               
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

            <div class="item active">
            <div class="video-box">
                <video id="movie" autoplay loop preload>
                    <source src="{{url('assets/mp4/1.mp4')}}" type="video/mp4"/>
                    您的瀏覽器不支援HTML 5影片播放標籤 video 格式。
                    Your browser doesn't support the video tag.
                </video>
            </div>
            <div class="carousel-caption carousel-caption-1">
                <h2>2018年最新款出國WIFI機型</h2>
                <h3>越南190、泰國150、新加坡190 <br>不限流量，通通讓你們4G吃到飽</h3>
                <br>
                <a href="#" class="btn btn-lg btn-orange">馬上預購</a>
            </div>
            </div>

            <div class="item">
            <img src="{{url('assets/images/banner/1.jpg')}}">
            <div class="carousel-caption carousel-caption-1">
                <h2>2018年最新款出國WIFI機型</h2>
                <h3>越南190、泰國150、新加坡190 <br>不限流量，通通讓你們4G吃到飽</h3>
                <br>
                <a href="#" class="btn btn-lg btn-orange">馬上預購</a>
            </div>
            </div>
        
            <div class="item">
            <img src="{{url('assets/images/banner/2.jpg')}}">
            <div class="carousel-caption carousel-caption-1">
                <h2>2018年最新款出國WIFI機型</h2>
                <h3>越南190、泰國150、新加坡190 <br>不限流量，通通讓你們4G吃到飽</h3>
                <br>
                <a href="#" class="btn btn-lg btn-orange">馬上預購</a>
            </div>
            </div>

            <div class="item">
            <img src="{{url('assets/images/banner/3.jpg')}}">
            <div class="carousel-caption carousel-caption-1">
                <h2>2018年最新款出國WIFI機型</h2>
                <h3>越南190、泰國150、新加坡190 <br>不限流量，通通讓你們4G吃到飽</h3>
                <br>
                <a href="#" class="btn btn-lg btn-orange">馬上預購</a>
            </div>
            </div>

            <div class="item">
            <img src="{{url('assets/images/banner/4.jpg')}}">
            <div class="carousel-caption carousel-caption-5">
                <h2>出國租用5天以上</h2>
                <h3>現折 $ xxx</h3>
                <br>
                <a href="#" class="btn btn-lg btn-orange">馬上預購</a>
            </div>
            </div>
        
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <i class="fas fa-angle-left"></i>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <i class="fas fa-angle-right"></i>
        </a>
    </div>           
</div>

<form><div id="content-box-2" class="search-box">
        <div class="right-box">
            
        </div>
        <div class="center-box">
            <div class="form-row">
                <div class="form-group col-md-2 col-xs-2">&nbsp;
                    <select class="form-control" id="">
                    <option selected>租賃分類</option>
                    <option>...</option>
                    </select>
                </div>
                <div class="form-group col-md-2 col-xs-2">&nbsp;
                    <select class="form-control" id="">
                    <option selected>地區</option>
                    <option>...</option>
                    </select>
                </div>

                <div class="form-group col-md-3 col-xs-3">取用(到貨)日期                            
                    <div id="datepicker-start" class="input-group date" data-date-format="mm-dd-yyyy">
                        <input class="form-control" type="text" readonly />
                        <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
                    </div>
                </div>
                <div class="form-group col-md-3 col-xs-3">歸還(寄還)日期                            
                    <div id="datepicker-end" class="input-group date" data-date-format="mm-dd-yyyy">
                        <input class="form-control" type="text" readonly />
                        <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
                    </div>
                </div>
                <div class="form-group col-md-2 col-xs-2">&nbsp;
                    <select class="form-control" id="">
                    <option selected>數量</option>
                    <option>...</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="left-box">
            <div class="col-md-2">&nbsp;
                <button class="btn btn-link btn-lg search-btn">
                    <i class="fas fa-play-circle"></i>&nbsp;&nbsp;查詢
                </button>
            </div>
        </div>
                        
</div></form>

<div id="content-box-3">
    <p class="slogan-1">美z.人生  要去那兒一國?</p>
    <p class="slogan-2">漫遊世界   美z.人生</p>
</div>

<div id="content-box-4">
    <div id="country-slide-box" class="country-slide-box">
        <div class="photos-box">
            <div class="photo-box photo-0-box">
                <div class="right-box">
                    <div class="triangle-box"></div>
                    <div class="slogan-bg">
                        <a class="btn btn-lg btn-orange">機種詳細介紹</a>
                    </div>
                </div>
                <div class="left-box">
                    <div class="black-box">
                        <div class="country"></div>
                        <p>業界最單一，一機全球通用</p>
                        <p>費率依想去的國家自動選</p>
                    </div>
                </div>
            </div>

            <div class="photo-box photo-1-box">
                <div class="right-box">
                    <div class="triangle-box"></div>
                    <div class="slogan-bg">
                        <a class="btn btn-lg btn-orange">機種詳細介紹</a>
                    </div>
                </div>
                <div class="left-box">
                    <div class="black-box">
                        <div class="country"></div>
                        <p>業界最單一，一機全球通用</p>
                        <p>費率依想去的國家自動選</p>
                    </div>
                </div>
            </div>

            <div class="photo-box photo-2-box">
                <div class="right-box">
                    <div class="triangle-box"></div>
                    <div class="slogan-bg">
                        <a class="btn btn-lg btn-orange">機種詳細介紹</a>
                    </div>
                </div>
                <div class="left-box">
                    <div class="black-box">
                        <div class="country"></div>
                        <p>業界最單一，一機全球通用</p>
                        <p>費率依想去的國家自動選</p>
                    </div>
                </div>
            </div>

            <div class="photo-box photo-3-box">
                <div class="right-box">
                    <div class="triangle-box"></div>
                    <div class="slogan-bg">
                        <a class="btn btn-lg btn-orange">機種詳細介紹</a>
                    </div>
                </div>
                <div class="left-box">
                    <div class="black-box">
                        <div class="country"></div>
                        <p>業界最單一，一機全球通用</p>
                        <p>費率依想去的國家自動選</p>
                    </div>
                </div>
            </div>

            <div class="photo-box photo-4-box">
                <div class="right-box">
                    <div class="triangle-box"></div>
                    <div class="slogan-bg">
                        <a class="btn btn-lg btn-orange">機種詳細介紹</a>
                    </div>
                </div>
                <div class="left-box">
                    <div class="black-box">
                        <div class="country"></div>
                        <p>業界最單一，一機全球通用</p>
                        <p>費率依想去的國家自動選</p>
                    </div>
                </div>
            </div>

            <div class="photo-box photo-5-box">
                <div class="right-box">
                    <div class="triangle-box"></div>
                    <div class="slogan-bg">
                        <a class="btn btn-lg btn-orange">機種詳細介紹</a>
                    </div>
                </div>
                <div class="left-box">
                    <div class="black-box">
                        <div class="country"></div>
                        <p>業界最單一，一機全球通用</p>
                        <p>費率依想去的國家自動選</p>
                    </div>
                </div>
            </div>

            <div class="photo-box photo-6-box">
                <div class="right-box">
                    <div class="triangle-box"></div>
                    <div class="slogan-bg">
                        <a class="btn btn-lg btn-orange">機種詳細介紹</a>
                    </div>
                </div>
                <div class="left-box">
                    <div class="black-box">
                        <div class="country"></div>
                        <p>業界最單一，一機全球通用</p>
                        <p>費率依想去的國家自動選</p>
                    </div>
                </div>
            </div>

            <div class="photo-box photo-7-box">
                <div class="right-box">
                    <div class="triangle-box"></div>
                    <div class="slogan-bg">
                        <a class="btn btn-lg btn-orange">機種詳細介紹</a>
                    </div>
                </div>
                <div class="left-box">
                    <div class="black-box">
                        <div class="country"></div>
                        <p>業界最單一，一機全球通用</p>
                        <p>費率依想去的國家自動選</p>
                    </div>
                </div>
            </div>                    
        </div>    
        <div class="btn-box">
            <div class="btn-0 btnitem active"><div class="img-box"></div></div>
            <div class="btn-1 btnitem"><div class="img-box"></div></div>
            <div class="btn-2 btnitem"><div class="img-box"></div></div>
            <div class="btn-3 btnitem"><div class="img-box"></div></div>
            <div class="btn-4 btnitem"><div class="img-box"></div></div>
            <div class="btn-5 btnitem"><div class="img-box"></div></div>
            <div class="btn-6 btnitem"><div class="img-box"></div></div>
            <div class="btn-7 btnitem"><div class="img-box"></div></div>                                        
        </div> 
    </div>                   
</div>

<div id="content-box-5">
    <div class="service-box service-box-0">
        <div class="bg-box">
            <div class="desc-box">
                <div class="box-1">辦公室與線上客服營業時間</div>
                <div class="box-2">
                    <h3>辦公室與線上客服營業時間</h3>
                    <hr>
                    <p>
                    週一至週五：早上10:00~晚上18:00<br>
                    週六、日：下午14:00~18:00<br>
                    例假日仍正常收貨及出貨，隨時有問題，請私訊客服，謝謝。</p>

                    <p>線上客服服務時間<br>
                    週一至週五:早上10:00~晚上19:00<br>
                    (這邊也可以設置三個客服的聯絡方式?)</p>

                    <p>台中NOVA停車超方便，現場門市取貨即將開放，敬請期待!</p>
                </div>
            </div>
        </div>
    </div> 
    <div class="service-box service-box-1">
        <div class="bg-box">
            <div class="desc-box">
                <div class="box-1">美z.人生出貨迅速</div>
                <div class="box-2">
                    <h3>美z.人生出貨迅速</h3>
                    <hr>
                    <p>急單處理Hen z害!</p> 
                    <p>粉絲或客戶只要於晚上6點半前下單，後台商品確認有貨，我們晚上8點還會有物流迅速出貨，讓您隔天下午1點左右拿到商品。</p>
                </div>
            </div>
        </div>
    </div>
    <div class="service-box service-box-2">
        <div class="bg-box">
            <div class="desc-box">
                <div class="box-1">只有北韓，很有距離</div>
                <div class="box-2">
                    <h3>只有北韓，很有距離</h3>
                    <hr>
                    <p>我們機器支援全球多個國家，橫跨歐洲、美洲、亞洲、澳洲、中東、非洲，讓你杜拜及土耳其轉機，也能輕鬆打卡報平安!</p> 
                    <p>接下來我們會陸續支援更多國家，敬請期待!</p>
                </div>
            </div>
        </div>
    </div>
    <div class="service-box service-box-3">
        <div class="bg-box">
            <div class="desc-box">
                <div class="box-1">機場取機超方便</div>
                <div class="box-2">
                    <h3>機場取機超方便</h3>
                    <hr>
                    <p>為了服務出國旅客，我們在熱門桃園機場一、二航廈，也有櫃台服務您。</p> 
                    <p>其他機場也會陸續洽談開放唷。</p>
                </div>
            </div>
        </div>
    </div>               
</div>

<div id="content-box-6">
    <div class="images-box">
        <div class="img img-0"></div>
        <div class="img img-1"></div>
        <div class="img img-2"></div>
        <div class="img img-3"></div>
        <div class="img img-4"></div>
        <div class="img img-5"></div>                
    </div>   
    <div class="mobile-box"></div> 
    <div class="fans-box">
        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fm.facebook.com%2FBeautyz.net&amp;tabs=timeline&amp;width=300&amp;height=500&amp;small_header=true&amp;adapt_container_width=false&amp;hide_cover=false&amp;show_facepile=true&amp;appId=1470026979698151" ;="" width="351" height="760" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
        <div class="mask"></div>
    </div>               
    <div class="qrcod-box-line">
        <p>LINE</p>
        <img src="{{url('assets/images/line.png')}}" width="150">
    </div>
    <div class="qrcod-box-wechat">
        <p>We Chat</p>
        <img src="{{url('assets/images/wehat.png')}}" width="150">
    </div> 
    <div class="desc-box">最新資訊和最新活動即時掌握<br>跟著 美z.人生 !一起去旅行!</div>              
</div>
@endsection

@section('footer')
    @include('FrontEnd.layouts.footer')
@endsection

@section('after_script')
    <script type="text/javascript" src="{{url('assets/js/home.js')}}"></script>
@endsection