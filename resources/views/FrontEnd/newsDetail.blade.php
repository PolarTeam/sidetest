<!DOCTYPE html>
<html lang="zh-Hans-TW">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>璟都建設</title>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-679VDJS0EK"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-679VDJS0EK');
</script>

<meta property="description" content="璟都建設創立於2003年，深耕桃園，永續建築生命力，回歸建築本質，不堆砌浮華表面，關照生態、厚植人本、創新永續，不隨市場起舞，成就世代相傳的宅居文明。" />
<meta property="og:type" content="website" />
<meta property="og:title" content="璟都建設" />
<meta property="og:description" content="璟都建設創立於2003年，深耕桃園，永續建築生命力，回歸建築本質，不堆砌浮華表面，關照生態、厚植人本、創新永續，不隨市場起舞，成就世代相傳的宅居文明。" />
<meta property="og:url" content="https://www.jing-du.com.tw/" />
<meta property="og:site_name" content="璟都建設" />
<meta property="og:locale" content="zh_TW" />
<meta property="og:image" content="https://www.jing-du.com.tw/img/thumb.png" />

<meta name="twitter:site" content="@" />
<meta name="twitter:url" content="https://www.jing-du.com.tw/" />
<meta name="twitter:title" content="璟都建設" />
<meta name="twitter:description" content="璟都建設創立於2003年，深耕桃園，永續建築生命力，回歸建築本質，不堆砌浮華表面，關照生態、厚植人本、創新永續，不隨市場起舞，成就世代相傳的宅居文明。" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:image:src" content="https://www.jing-du.com.tw/img/thumb.png" />



<!-- Bootstrap core CSS -->
<link href="jindu/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="jindu/css/custom.css" rel="stylesheet" />
<link rel="stylesheet" href="jindu/css/jquery.bxslider.css">

<!-- Custom Fonts -->
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@100;300;400;500;700;900&family=Noto+Serif+TC:wght@200;300;400;500;600;700;900&display=swap" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" rel="stylesheet">

</head>

<body id="body">

<!-- Loading -->
<div id="loading" class="h-100">
<div class="row justify-content-center align-self-center h-100">
<div class="lds-rolling my-auto">
<div></div>
</div>
</div>
</div>

<!-- Scroll Top Button -->
<button onclick="topFunction()" id="myBtn" title="回到頁首" class="js-scroll-trigger">
<i class="fas fa-chevron-up"></i>
</button>

<!-- Navigation -->
<nav class="navbar navbar-custom navbar-expand-lg fixed-top">
    <div class="inner nav-bg">
      <div class="container"> <a class="navbar-brand" href="{{url('/')}}"><img src="jindu/img/logo-w.svg" class="img-fluid d-block" alt="logo"></a>
        <div class="navbar-toggler" id="opNav"> <i class="fas fa-bars"></i> <span>menu</span> </div>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"> <a class="nav-link" href="{{url('intro')}}">關於璟都</a> </li>
            <li class="nav-item"> <a class="nav-link" href="{{url('promo')}}" >銷售個案</a> </li>
            <li class="nav-item"> <a class="nav-link" href="{{url('project')}}" >歷年業績</a> </li>
            <li class="nav-item"> <a class="nav-link" href="{{url('process')}}" >工程進度</a> </li>
            <li class="nav-item"> <a class="nav-link" href="{{url('news')}}">最新消息</a> </li>
            <li class="nav-item"> <a class="nav-link" href="{{url('contact')}}" >聯絡我們</a> </li>
            <li class="nav-item ml-4"> <a class="nav-link alt-link" href="https://lin.ee/BwadcVU" target="_blank"><i class="fab fa-line"></i></a> </li>
            <li class="nav-item ml-3"> <a class="nav-link alt-link" href="https://www.facebook.com/JingDouJianShe/" target="_blank"><i class="fab fa-facebook-square"></i></a> </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  
  <!-- Mobile Nav -->
  <div class="mobile-nav">
    <div class="navbar-toggler" id="csNav"> <i class="fas fa-times"></i> <span>close</span> </div>
    <div class="mobile-inner"> 
      <a href="{{url('/')}}">回首頁<span>HOME</span></a> 
      <a href="{{url('intro')}}">關於璟都<span>INTRODUCTION</span></a> 
      <a href="{{url('promo')}}">銷售個案<span>PROMOTION</span></a> 
      <a href="{{url('project')}}">歷年業績<span>CLASSICS</span></a> 
      <a href="{{url('process')}}">工程進度<span>PROCESSION</span></a> 
      <a href="{{url('news')}}">最新消息<span>NEWS</span></a> 
      <a href="{{url('contact')}}">聯絡我們<span>CONTACT US</span></a> 
      <a href="https://lin.ee/BwadcVU" target="_blank"><i class="fab fa-line mr-2"></i>加入璟都官方Line</a>
      <a href="https://www.facebook.com/JingDouJianShe/" target="_blank"><i class="fab fa-facebook-square mr-2"></i>璟都建設FB粉絲團</a> 
      </div>
  </div>

<!-- Title Top -->
<div class="page-title" style="background-image: url(img/page/t5.jpg)">
	<div class="overlay"></div>
	<div class="title">最新消息</div>
</div>


<!-- Page Content -->
<div class="main-content">
  <div class="parallax bg1" id="main1">
	  
	  <!-- Section -->
	  <div class="section">
          <div class="container">
			  <div class="row mb-5">
			  <div class="col-11 col-lg-12 mx-auto">

				  
				  <div class="row mb-4">
				       <div class="col-12">
						   <p class="post-meta"><span class="s2">公益活動</span>2021-11-23</p>
                            <h1 class="post-title">
                              璟都有愛，為孩子的生活創造更多美好
                            </h1>
					  </div>
				  </div>
				  
				  <div class="row mb-4">
				       <div class="col-12">
						   <img src="img/news/001/01.jpg" class="img-fluid d-block mx-auto" alt="pic">
					  </div>
				  </div>
				  
				  
				  <div class="row mb-5">
				       <div class="col-12 page-topic">
						   <h4 class="mb-3">愛心捐贈汽車</h4>
						   <p>今11月19日虹爵董事長黃國明愛心捐贈TOYOTA  wish汽車一輛予「財團法人國際單親兒童文教基金會」，由該基金會行政總監王惠敏、總管理處長劉姗姗代表接受，在場觀禮者：有愛基金會執行長/蔡萬來，璟都建設總經理/吳明亮、海悦廣告總經理/王俊傑、金鐘獎導演/李岳峰、知名藝人國民爸爸/檢場、主持人顧問/張月麗，、榮董會長/蘇家豐……等。</p>
					       <p>國際單親兒童基金會(簡稱)位於台南麻二甲之家安置弱勢兒童及少年，目前約20名，持續增加中，包含國小、國中、高中學生，每日需至學校上學、就醫或與家人會面……等需求，急需公務車一台，以解決學生接送等問題。</p>
                           <p>董事長黃國明獲悉後，即捐贈TOYOTA   wish汽車一輛，該車原是虹爵公務車，該車況佳、里程數祗3萬公里，以協助[麻二甲之家]交通問題。黃國明董事長，慷慨解囊、捐贈車輛，義行可嘉，令人敬佩！</p>
                      </div>
				  </div>
				  
				  				  
				  
				  <div class="row">
                      <div class="col-auto ml-auto mr-3 mr-lg-0">
                          <a href="news.html" class="more-btn ml-auto">
                              BACK<span><i class="fas fa-arrow-right"></i></span>
                          </a>
                      </div>
                  </div>
				  
				  
			  </div>
		      </div>
			</div>
		 </div> 
    
	  
	<!-- Footer Section -->
    <div class="section footer">
        <div class="container">
            <div class="row mb-4">
                <div class="col-lg-8 mx-auto">
                     <div class="row footer-link">
                          <div class="col-4 col-lg-2">
                              <a href="intro.html">關於璟都</a>
                          </div>
                         <div class="col-4 col-lg-2">
                              <a href="promo.html">銷售個案</a>
                          </div>
                         <div class="col-4 col-lg-2">
                              <a href="project.html">歷年業績</a>
                          </div>
                         <div class="col-4 col-lg-2">
                              <a href="process.html">工程進度</a>
                          </div>
                         <div class="col-4 col-lg-2">
                              <a href="news.html">最新消息</a>
                          </div>
                         <div class="col-4 col-lg-2">
                              <a href="contact.html">聯絡我們</a>
                          </div>
                      </div>
                </div>
            </div>
			  
			  <div class="row mb-3 d-flex align-items-center">
						        <div class="col-7 col-lg-2 mb-3 mb-lg-0 ml-auto mr-auto mr-lg-0">
                                     <img src="img/logo-w.svg" class="img-fluid d-block" alt="logo">
                                 </div>
                                 <div class="col-lg-auto ml-auto ml-lg-0 mr-auto footer-info">
                                     0800-639-999<span class="d-none d-lg-inline-block px-2">│</span><br class="d-block d-md-none">330 桃園市桃園區桃園市中正路1071號16樓之3
                                 </div>
			  </div>
			  
			  <div class="row">
				   <div class="col-auto mx-auto footer-copy">
					   Copyright © Jing-Du Construction Co., Ltd. All Rights Reserved.
				   </div>
			  </div>
			  
		  </div>
	  </div>
	<!-- Footer Section -->
	  
  </div>
</div>

<!-- Bootstrap core JS--> 
<script src="jindu/vendor/jquery/jquery.min.js"></script> 
<script src="jindu/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> 

<!-- Custom JS--> 
<script src="jindu/js/custom.js"></script>
	
	<!-- bxSlider JavaScript -->
    <script src="js/jquery.bxslider.js"></script>
    <script>
    $(document).ready(function(){
      $('.slider').bxSlider({
    adaptiveHeight: true,
	responsive: true,
	  });
    });	


    </script>
	
</body>
</html>
