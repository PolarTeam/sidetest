<?php 
    $cmpData = DB::table('mod_cmp')->where('id', 1)->first();
?>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="widget about-widget">
                    <img src="{{url('assets/images/logo.png')}}" alt="Simple Footer Logo">
                    <hr>
                    <p></p>
                    <address>
                        <label>地址:</label>
                        {{$cmpData->address}}
                    </address>
                    <label>電話:</label> {{$cmpData->phone}}<br>
                    <label>傳真:</label> {{$cmpData->fax}}<br>
                    <label>E-mail:</label> <a href="mailto:{{$cmpData->email}}">{{$cmpData->email}}</a>
                    <div class="social-icons">
                        <label>找到我們:</label>
                        <a href="https://www.facebook.com/standardinformation/?ref=bookmarks" class="social-icon" title="Facebook"><i class="fa fa-facebook"></i></a>
                        {{--  <a href="#" class="social-icon" title="Twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="social-icon" title="Github"><i class="fa fa-github"></i></a>
                        <a href="#" class="social-icon" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                        <a href="#" class="social-icon" title="Tumblr"><i class="fa fa-tumblr"></i></a>
                        <a href="#" class="social-icon" title="Flickr"><i class="fa fa-flickr"></i></a>  --}}
                    </div><!-- End .social-icons -->
                </div><!-- End .widget -->
            </div><!-- End .col-md-5 -->
            <div class="col-md-5">
                <img src="{{url('images/map.jpg')}}" alt="{{$cmpData->address}}" height="400" class="img-responsive">
            </div>
        </div><!-- End .row -->
    </div><!-- End .container -->

    <div class="footer-bottom">
        <div class="container">
            {{--  <div class="footer-right">
                <ul class="footer-menu">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Features</a></li>
                    <li><a href="#">FaQS</a></li>
                    <li><a href="#">Support</a></li>
                    <li><a href="#">Contact Us</a></li>
                </ul>
            </div><!-- End .footer-right -->  --}}

            <p class="copyright">台北國揚稅務會計記帳事務所 - All rights reserved - 2018 &copy; Made by <a href="https://standard-info.com" target="_blank" title="eonythemes">使丹達資訊</a>.</p>
        </div><!-- End .container -->
    </div><!-- End .footer-bottom -->
</footer><!-- End .footer -->