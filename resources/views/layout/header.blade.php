<?php 
    $cmpData = DB::table('mod_cmp')->where('id', 1)->first();
?>
<style>
    body {
        font-family: Microsoft JhengHei !important;
    }
</style>
<header class="header sticky-header">
    <div class="header-inner">
        <div class="container">
            <a href="{{url('/')}}" class="site-logo" title="Simple - Multipurpose Template">
                <img src="{{url('assets/images/logo.png')}}" alt="Simple Logo" width="490">
                <span class="sr-only">{{$cmpData->cname}}</span>
            </a>

            <div class="header-info-boxes">
                <div class="header-info-box">
                    <i class="fa fa-phone"></i>
                    <div class="info-box-title">聯絡我們</div>
                    <p><a href="tel:0227263006">{{$cmpData->phone}}</a></p>
                </div><!-- End .header-info-box -->

                <div class="header-info-box">
                    <i class="fa fa-fax"></i>
                    <div class="info-box-title">傳真</div>
                    <p>{{$cmpData->fax}}</p>
                </div><!-- End .header-info-box --> 
                <div class="header-info-box">
                    <a href="https://www.facebook.com/standardinformation/?ref=bookmarks">
                    <i class="fa fa-facebook-f"></i>
                    <div class="info-box-title">facebook</div>
                    <p>粉絲團</p>
                    </a>
                </div><!-- End .header-info-box --> 
            </div><!-- End header-info-boxes -->
        </div><!-- End .container -->
    </div><!-- End .header-inner -->

    <div class="header-bottom">
        <div class="container">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-container" aria-expanded="false">
                <span class="toggle-text">Menu</span>
                <span class="toggle-wrapper">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </span>
            </button>
            <div class="collapse navbar-collapse" id="main-nav-container">
                <ul class="nav navbar-nav">
                    <?php 
                        $headerData = DB::select('select id, name, slug from pages order by id asc');
                    ?>
                    @foreach($headerData as $row)
                    <li class="dropdown @if(url('/'.$row->slug) == Request::url()) active @endif">
                        <a href="{{url('/'.$row->slug)}}"  role="button" >{{$row->name}}</a>
                    </li>
                    @endforeach
                    <li class="dropdown @if(url('newsList') == Request::url()) active @endif">
                        {{--  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">最新消息<span class="angle"></span></a>  --}}
                        <a href="{{url('newsList')}}"  role="button" >最新消息</a>
                    </li>
                    <li class="dropdown @if(url('custmerService') == Request::url()) active @endif">
                        {{--  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">客戶服務<span class="angle"></span></a>  --}}
                        <a href="contact"  role="button" >客戶服務</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- End .container -->
    </div><!-- End .header-bottom -->
</header><!-- End .header -->