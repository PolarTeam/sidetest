<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>台北國揚稅務會計記帳事務所</title>
        <meta name="description" content="Multipurpose and creative template">

        <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Google Fonts -->
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700%7COpen+Sans:300,300i,400,400i,600,600i,700,800" rel="stylesheet">

        <link rel="stylesheet" href="{{url('assets/css/plugins.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/style.css')}}">
        
        <!-- Favicon -->
        <link rel="icon" type="image/png" href="{{url('assets/images/icons/favicon.png')}}">

        <!-- Modernizr -->
        <script src="{{url('assets/js/modernizr.js')}}"></script>
    </head>
    <body class="single">
        <div id="wrapper">
            @include('layout.header')
            <div class="main">
                <div class="page-header larger parallax custom" style="background-image:url(assets/images/page-header-bg.jpg)">
                    <div class="container">
                        <h1>{{$page->title}}</h1>
                        <ol class="breadcrumb">
                            <li><a href="index.html">首頁</a></li>
                            <li class="active">{{$page->title}}</li>
                        </ol>
                    </div><!-- End .container -->
                </div><!-- End .page-header -->

                <div class="container">
                    <article class="entry">
                        <div class="entry-content">
                            {!! $page->content !!}
                        </div><!-- End .entry-content -->
                        
                    </article>

                </div><!-- End .container -->
            </div><!-- End .main -->

            @include('layout.footer')

        </div><!-- End #wrapper -->
        <a id="scroll-top" href="#top" title="Scroll top"><i class="fa fa-angle-up"></i></a>

        <!-- End -->
        <script src="{{url('assets/js/plugins.min.js')}}"></script>
        <script src="{{url('assets/js/twitter/jquery.tweet.min.js')}}"></script>
        <script src="{{url('assets/js/main.js')}}"></script>
    </body>
</html>