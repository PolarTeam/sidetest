@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
	銷售個案<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url(config('backpack.base.route_prefix'),'promotion') }}">銷售個案</a></li>
		<li class="active">銷售個案</li>
	</ol>
</section>
<script>
	var single_shipment = "";
    var vendor_nm = "";
    var vendor_cd = "";
    var action_sort =0;
    var sort =0;
    @if(isset($newData))
        single_shipment = "N";
        sort = 99;
        action_sort = 99;
        vendor_cd = "{{$newData->email}}";
        vendor_nm = "{{$newData->name}}";
	@endif

</script>
@endsection 

@section('content')
	@include('backpack::template.toolbar')
	<div class="row">
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4>{{ trans('backpack::crud.please_fix') }}</h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">銷售個案</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="box-body">
								<div class="row">
                                <div class="form-group col-md-3">
                                            <label for="title">名稱</label>
                                            <input type="text" class="form-control" id="title" name="title">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="sub_title">副標題</label>
                                            <input type="text" class="form-control" id="sub_title" name="sub_title">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="addr">縣市區域/位置</label>
                                            <input type="text" class="form-control" id="addr" name="addr">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="finish_at">年份</label>
                                            <input type="text" class="form-control" id="finish_at" name="finish_at">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="structure">建案資訊</label>
                                            <select class="form-control select2" data-placeholder="請選擇" style="width: 100%;" name="structure[]"></select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="link">網站連結</label>
                                            <input type="text" class="form-control" id="link" name="link">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="tracking">地圖導航</label>
                                            <input type="text" class="form-control" id="tracking" name="tracking">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="planning">地圖網址</label>
                                            <input type="text" class="form-control" id="planning" name="planning">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="show_home">顯示首頁</label>
                                            <select class="form-control" id="show_home" name="show_home">
                                                <option value="Y">是</option>
                                                <option value="N">否</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-12" >
                                            <label for="content1">內文</label>
                                            <textarea class="form-control" rows="3" name="content1"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="img1">列表縮圖</label>
                                            <input type="file" name="img1" id="img1"> 
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="img2">導覽縮圖</label>
                                            <input type="file" name="img2" id="img2"> 
                                        </div>
                                    </div>
                                <div class="row">
                                    <div class="form-group col-md-3">

                                        @if(isset($id) && $crud->entry['original']['img1'])
                                        <a name="img1" href="{{Storage::url($crud->entry['original']['img1'])}}" target="_blank">
                                            <img name="img1" src="{{Storage::url($crud->entry['original']['img1'])}}" alt="" height="100">
                                        </a>
                                        @else
                                            <a name="img1" href="#" target="_blank">
                                                <img name="img1" src="#" alt="" height="100" style="display:none">
                                            </a>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-3">
                                        @if(isset($id) && $crud->entry['original']['img2'])
                                        <a name="img2" href="{{Storage::url($crud->entry['original']['img2'])}}" target="_blank">
                                            <img name="img2" src="{{Storage::url($crud->entry['original']['img2'])}}" alt="" height="100">
                                        </a>
                                        @else
                                            <a name="img2" href="#" target="_blank">
                                                <img name="img2" src="#" alt="" height="100" style="display:none">
                                            </a>
                                        @endif
                                    </div>
                                </div>

								@if(isset($id))
								<input type="hidden" name="id" value="{{$id}}" class="form-control">
								<input type="hidden" name="_method" value="PUT" class="form-control"> 
								@endif
                            </div>
                        </div>                        
                    </div>
				</div>
			</form>
		</div>	
    </div>


    <div class="row">

            <div class="col-md-12">
                <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
                    <ul class="nav nav-tabs">
                        <li name="prodDetail" class="active">
                            <a onclick="tab_4()" href="#tab_4" data-toggle="tab" aria-expanded="false">輪播圖片</a>
                        </li>
                        <li name="detail">
                            <a onclick="tab_5()" href="#tab_5" data-toggle="tab" aria-expanded="false">明細</a>
                        </li>
                        <li name="baseinfo">
                            <a onclick="tab_6()" href="#tab_6" data-toggle="tab" aria-expanded="false">基本資料</a>
                        </li>
                    </ul>
                    
                    <div class="tab-content">
                        <!-- /.tab-pane -->
                        <div class="tab-pane active" id="tab_4" name="prodDetail">
                            <div class="box box-primary" id="subBox" style="display:none">
                                <div class="box-header with-border">
                                <h3 class="box-title">圖片</h3>
                                </div>
                                <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="img1">圖片</label>
                                            <input type="file" name="img1" grid="true"> 
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="rank">排序</label>
                                            <input type="number" class="form-control input-sm" name="rank" grid="rank" >
                                        </div> 
                                    </div>
                                </div>
                                <!-- /.box-body -->
        
                                <div class="box-footer">
                                    <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                                    <button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
                                </div>
                                </form>
                            </div>
                            
                            <div id="jqxGrid"></div>
                        </div>
                        <!-- /.tab-pane -->
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_5" name="detail">
                            <div class="box box-primary" id="subBox1" style="display:none">
                                <div class="box-header with-border">
                                <h3 class="box-title">明細</h3>
                                </div>
                                <form method="POST"  accept-charset="UTF-8" id="subForm1" enctype="multipart/form-data">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="title">標體</label>
                                            <input type="text" class="form-control input-sm" name="title" grid="true" >
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="descp">敘述</label>
                                            <input type="text" class="form-control input-sm" name="descp" grid="true" >
                                        </div>  
                                        <div class="form-group col-md-3">
                                            <label for="value">內容</label>
                                            <input type="text" class="form-control input-sm" name="value" grid="true" >
                                        </div>    
                                        <div class="form-group col-md-3">
                                            <label for="rank">排序</label>
                                            <input type="number" class="form-control input-sm" name="rank" grid="true" >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="img1">圖片</label>
                                            <input type="file" name="img1" grid="true"> 
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
        
                                <div class="box-footer">
                                    <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                                    <button type="button" class="btn btn-sm btn-primary" id="Save1">{{ trans('common.save') }}</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="Cancel1">{{ trans('common.cancel') }}</button>
                                </div>
                                </form>
                            </div>
                            
                            <div id="jqxGrid1"></div>
                        </div>
                        <!-- /.tab-pane -->

                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_6" name="baseinfo">
                            <div class="box box-primary" id="subBox2" style="display:none">
                                <div class="box-header with-border">
                                <h3 class="box-title">基本資料</h3>
                                </div>
                                <form method="POST"  accept-charset="UTF-8" id="subForm2" enctype="multipart/form-data">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="title">標題</label>
                                            <input type="text" class="form-control input-sm" name="title" grid="true" >
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="content">內容</label>
                                            <input type="text" class="form-control input-sm" name="content" grid="true" >
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="rank">排序</label>
                                            <input type="number" class="form-control input-sm" name="rank" grid="true" >
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                                    <button type="button" class="btn btn-sm btn-primary" id="Save2">{{ trans('common.save') }}</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="Cancel2">{{ trans('common.cancel') }}</button>
                                </div>
                                </form>
                            </div>
                            <div id="jqxGrid2"></div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
            
        </div>
@endsection 
@include('backpack::template.lookup') 
@section('after_scripts')
<script src="{{ asset('vendor/backpack/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/backpack/ckeditor/adapters/jquery.js') }}"></script>
	<script>
        var mainId = "";
        var prodNo = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/promotion') }}";

		var fieldData = null;
        var fieldObj = null;
        
        var imgPath = "{{Storage::url('/')}}";

		@if(isset($crud -> create_fields))
		fieldData = '{{!! json_encode($crud->create_fields) !!}}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		@endif

		@if(isset($id))
		mainId   = "{{$id}}";
		editData = '{{!! $entry !!}}';
		editData = editData.substring(1);
		editData = editData.substring(0, editData.length - 1);
        var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
        console.log(editData);
		//editObj  = JSON.parse(objJson);
		@endif

		$(function () {
			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/promotion') }}";
			formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_promotion') }}/" + mainId;
			formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/promotion') }}";
			formOpt.afterInit = function() {
                $("#finish_at").datepicker({
                    format: "yyyy",
                    viewMode: "years", 
                    minViewMode: "years",
                    autoclose:true //to close picker once year is selected
                });
				if(mainId != null) {
                    // $("input[type='file']").not('[grid="true"]').prop("disabled", true);
                    setTimeout(function(){
                        //CKEDITOR.instances['descp'].setReadOnly(true);
                        //CKEDITOR.instances['slogan'].setReadOnly(true);
                        //CKEDITOR.instances['other_content'].setReadOnly(true);
                        
                    }, 500);
                }
                $("[name='prodDetail']").show();
                $("[href='#tab_4']").click();
                getDetailGrid();
                getDetailGrid_type();
                getBaseinfoGrid();
                // prodTypeSwitch(prodType);
			}

			formOpt.initFieldCustomFunc = function () {

			};

			formOpt.afterDel = function() {

			}

			formOpt.addFunc = function() {
                setTimeout(function(){
                    $("#single_shipment").val(single_shipment);
                    $("#vendor_nm").val(vendor_nm);
                    $("#action_sort").val(action_sort);
                    $("#sort").val(sort);
                }, 500);
                
                @if(isset($prodType))
                $("#prod_type").val("{{$prodType}}");
                $("#prod_type").trigger("change");
                @endif
			}

			formOpt.editFunc = function() {
                setTimeout(function(){
                    // CKEDITOR.instances['descp'].setReadOnly(false);
                    // CKEDITOR.instances['slogan'].setReadOnly(false);
                    // CKEDITOR.instances['other_content'].setReadOnly(false);
                }, 500);
                
			}

			formOpt.copyFunc = function() {
                $('#jqxGrid').jqxGrid('clear');
                setTimeout(function(){
                }, 500);
            }

            formOpt.beforeSave = function() {

            }

			formOpt.saveSuccessFunc = function(data) {
                var result = data.data;
                console.log(result);
                if(result.img1 != "" && result.img1 != null) {
                    $("img[name='img1']").attr("src", ROOT_URL+"/uploads/"+result.img1).show();
                    $("a[name='img1']").attr("href", ROOT_URL+"/uploads/"+result.img1);
                }
                if(result.img2 != "" && result.img2 != null) {
                    $("img[name='img2']").attr("src", ROOT_URL+"/uploads/"+result.img2).show();
                    $("a[name='img2']").attr("href", ROOT_URL+"/uploads/"+result.img2);
                }

                $("input[type='file']").not('[grid="true"]').prop("disabled", true);
                // $("input[type='file']").val("");
			}

			var btnGroup = [

			];

            initBtn(btnGroup);

            
        });
        function getDetailGrid_type() {
            console.log("nick");
            var imagerenderer = function (row, datafield, value) {
                if(value != "") {
                    return '<img style="margin-left: 5px;" height="60" width="50" src="'+ROOT_URL+'/uploads/' + value.replace('public/', '') + '"/>';
                }
                return "";
            }
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "title", type: "string"},
                    {name: "promotion_id", type: "string"},
                    {name: "descp", type: "string"},
                    {name: "value", type: "string"},
                    {name: "rank", type: "number"},
                    {name: "img1", type: "string"},
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true},
                    {text: "promotion_id", datafield: "promotion_id", width: 150, hidden: true},
                    {text: "標題", datafield: "title", width: 150},
                    {text: "敘述", datafield: "descp", width: 150},
                    {text: "內容", datafield: "value", width: 150},
                    {text: "排序", datafield: "rank", width: 150},
                    {text: "圖片", datafield: "img1", width: 150, cellsrenderer: imagerenderer},
                ]
            ];
            var opt = {};
            if(mainId==""){
                mainId="0";
            }
            opt.gridId = "jqxGrid1";
            opt.fieldData = col;
            opt.formId = "subForm1";
            opt.saveId = "Save1";
            opt.cancelId = "Cancel1";
            opt.showBoxId = "subBox1";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/promotionDetailcate') }}"+ "/"+ mainId;;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/promotionDetailcate') }}" + "/cateStore";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/promotionDetailcate') }}" + "/update/"+ mainId;
            opt.delUrl  = "{{ url(config('backpack.base.route_prefix', 'admin') . '/promotionDetailcate') }}" + "/catedelete/";
            opt.defaultKey = {'promotion_id': mainId};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.rowsheight = 60;
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                console.log(data);
            }
            genDetailGrid(opt);
            $("[name='prodDetail']").show();
            $("[href='#tab_4']").click();
        }

        function getBaseinfoGrid() {
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "title", type: "string"},
                    {name: "content", type: "string"},
                    {name: "rank", type: "number"},
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true},
                    {text: "promotion_id", datafield: "promotion_id", width: 150, hidden: true},
                    {text: "標題", datafield: "title", width: 150},
                    {text: "內容", datafield: "content", width: 150},
                    {text: "排序", datafield: "rank", width: 150},
                ]
            ];
            var opt = {};
            if(mainId==""){
                mainId="0";
            }
            opt.gridId = "jqxGrid2";
            opt.fieldData = col;
            opt.formId = "subForm2";
            opt.saveId = "Save2";
            opt.cancelId = "Cancel2";
            opt.showBoxId = "subBox2";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/promotioninfo') }}"+ "/"+ mainId;;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/promotioninfo') }}" + "/infoStore";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/promotioninfo') }}" + "/update/"+ mainId;
            opt.delUrl  = "{{ url(config('backpack.base.route_prefix', 'admin') . '/promotioninfo') }}" + "/infodelete/";
            opt.defaultKey = {'promotion_id': mainId};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.rowsheight = 60;
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                console.log(data);
            }
            genDetailGrid(opt);
        }
        
        function getDetailGrid() {
            var imagerenderer = function (row, datafield, value) {
                if(value != "") {
                    return '<img style="margin-left: 5px;" height="60" width="50" src="'+ROOT_URL+'/uploads/'+ value.replace('public/uploads/', 'uploads/') + '"/>';
                }

                return "";
            }
            // var countryCode = [];

            // if(fieldObj != null) {
            //     for(i in fieldObj["country"]["options"]) {
            //         var obj = {value: fieldObj["country"]["options"][i]["code"], label: fieldObj["country"]["options"][i]["descp"]};
            //         countryCode.push(obj);
            //     }
            // }

            // var countrySource =
            // {
            //         datatype: "array",
            //         datafields: [
            //             { name: 'label', type: 'string' },
            //             { name: 'value', type: 'string' }
            //         ],
            //         localdata: countryCode
            // };

            // var countryAdapter = new $.jqx.dataAdapter(countrySource, {
            //     autoBind: true
            // });
            //console.log(countryAdapter.records);
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "promotion_id", type: "string"},
                    {name: "img1", type: "string"},
                    {name: "img_descp1", type: "string"},
                    {name: "rank", type: "number"},
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true},
                    {text: "promotion_id", datafield: "promotion_id", width: 150, hidden: true},
                    {text: "圖片", datafield: "img1", width: 150, cellsrenderer: imagerenderer},
                    {text: "描述1", datafield: "img_descp1", width: 150,hidden: true},
                    {text: "排序", datafield: "rank", width: 100},
                ]
            ];
            var opt = {};
            console.log(mainId);
            if(mainId==""){
                mainId="0";
            }
            opt.gridId = "jqxGrid";
            opt.fieldData = col;
            opt.formId = "subForm";
            opt.saveId = "Save";
            opt.cancelId = "Cancel";
            opt.showBoxId = "subBox";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/promotionDetail') }}"+ "/"+ mainId;;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/promotionDetail') }}" + "/detailStore";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/promotionDetail') }}" + "/update/"+ mainId;
            opt.delUrl  = "{{ url(config('backpack.base.route_prefix', 'admin') . '/promotionDetail') }}" + "/delete/";
            opt.defaultKey = {'promotion_id': mainId};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.rowsheight = 60;
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                console.log(data);
            }
            console.log("nick");
            $("[name='prodDetail']").show();
            $("[href='#tab_4']").click();
            genDetailGrid(opt);
        }
        

    </script>
    <script>
            $(function(){
                formOpt.initFieldCustomFunc();
                $.get( formOpt.fieldsUrl , function( data ) {
                    if(typeof formOpt.afterInit === "function") {
                        if(data.img1 != undefined) {
                            $("img[name='img1']").attr("src", ROOT_URL+"/uploads/"+data.img1).show();
                            $("a[name='img1']").attr("href", ROOT_URL+"/uploads/"+data.img1);
                        }
                        if(data.img2 != undefined) {
                            $("img[name='img2']").attr("src", ROOT_URL+"/uploads/"+data.img2).show();
                            $("a[name='img2']").attr("href", ROOT_URL+"/uploads/"+data.img2);
                        }
                    }
                });
            });
            function tab_4() {
                document.getElementById('jqxGrid').style.display = 'block';
                document.getElementById('jqxGrid1').style.display = 'none';
                document.getElementById('jqxGrid2').style.display = 'none';
            }
            function tab_5() {
                document.getElementById('jqxGrid').style.display = 'none';
                document.getElementById('jqxGrid1').style.display = 'block';
                document.getElementById('jqxGrid2').style.display = 'none';
            }
            function tab_6() {
                document.getElementById('jqxGrid').style.display = 'none';
                document.getElementById('jqxGrid1').style.display = 'none';
                document.getElementById('jqxGrid2').style.display = 'block';
            }
    </script>
@endsection