@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        企業新訊<small></small>
      </h1>
      <ol class="breadcrumb">
      <li class="active">企業新訊</li>
      </ol>
    </section>
@endsection

@section('before_scripts')


<script>
var gridOpt = {};
gridOpt.pageId        = "modNews";
gridOpt.enabledStatus = false;
gridOpt.searchOpt     = true;
gridOpt.fieldsUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getFieldsJson/mod_news') }}";
gridOpt.dataUrl = "{{ url(config('backpack.base.api_route_prefix', 'api').'/admin/baseApi/getGridJson/mod_news') }}";
gridOpt.createUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/modNews/create') }}";
gridOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/modNews') }}" + "/{id}/edit";

var btnGroup = [
  {
    btnId: "btnSearchWindow",
    btnIcon: "fa fa-search",
    btnText: "{{ trans('common.search') }}",
    btnFunc: function () {
        $('#searchWindow').jqxWindow('open');
    }
  },
  {
    btnId: "btnExportExcel",
    btnIcon: "fa fa-cloud-download",
    btnText: "{{ trans('common.exportExcel') }}",
    btnFunc: function () {
      $("#jqxGrid").jqxGrid('exportdata', 'xls', '企業新訊');
    }
  },
  {
    btnId: "btnOpenGridOpt",
    btnIcon: "fa fa-table",
    btnText: "{{ trans('common.gridOption') }}",
    btnFunc: function () {
      $('#gridOptModal').modal('show');
    }
  },
  {
    btnId:"btnAdd",
    btnIcon:"fa fa-edit",
    btnText:"{{ trans('common.add') }}",
    btnFunc:function(){
      location.href= gridOpt.createUrl;
    }
  }, 
  {
    btnId:"btnDelete",
    btnIcon:"fa fa-trash-o",
    btnText:"{{ trans('common.delete') }}",
    btnFunc:function(){
          var rows = $("#jqxGrid").jqxGrid('selectedrowindexes');
          var ids = new Array();
          for (var m = 0; m < rows.length; m++) {
          var row = $("#jqxGrid").jqxGrid('getrowdata', rows[m]);
          if(typeof row != "undefined") {
              ids.push(row.id);
          }
          }
          if(ids.length == 0) {
          swal("請至少選擇一筆資料", "", "warning");
          }
          $.post(BASE_URL + '/modNews/multi/del', {'ids': ids}, function(data){
              if(data.msg == "success") {
                  swal("刪除成功", "", "success");
                  $("#jqxGrid").jqxGrid('updatebounddata');
              }
              else{
                  swal("操作失敗", "", "error");
              }
          });
      }
  }
];
</script>
@endsection

@include('backpack::template.search')
