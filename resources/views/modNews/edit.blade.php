@extends('backpack::layout') 
@section('header')
<section class="content-header">
	<h1>
	企業新訊<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url(config('backpack.base.route_prefix'),'OrderMgmt') }}">企業新訊</a></li>
		<li class="active">企業新訊</li>
	</ol>
</section>
<script>
	var single_shipment = "";
    var vendor_nm = "";
    var vendor_cd = "";
    var action_sort =0;
    var sort =0;
    @if(isset($newData))
        single_shipment = "N";
        sort = 99;
        action_sort = 99;
        vendor_cd = "";
        vendor_nm = "";
	@endif

</script>
@endsection 

@section('content')
	@include('backpack::template.toolbar')
	<div class="row">
		<div class="col-md-12">
			<div class="callout callout-danger" id="errorMsg" style="display:none">
				<h4>{{ trans('backpack::crud.please_fix') }}</h4>
				<ul>

				</ul>
			</div>
			<form method="POST" accept-charset="UTF-8" id="mainForm" enctype="multipart/form-data">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">企業新訊</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="box-body">
								<div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="title">標題</label>
                                        <input type="text" class="form-control" id="title" name="title">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="type">類別</label>
                                        <select class="form-control" style="width: 100%;" id='type' name="type">
                                        </select>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="years">時間</label>
                                        <input type="text" class="form-control" id="years" name="years">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="content">內文</label>
                                        <textarea class="form-control" rows="5" name="content"></textarea>
                                    </div>
                                </div>

                                @if(isset($id))
                                    <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                    <input type="hidden" name="_method" value="PUT" class="form-control"> 
                                @endif
                            </div>
                    </div>
                </div>
            </div>
        </form>
    </div>	
</div>


<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
            <ul class="nav nav-tabs">
                <li name="prodDetail" class="active">
                    <a href="#tab_4" data-toggle="tab" aria-expanded="false">輪播圖片</a>
                </li>
            </ul>
            
            <div class="tab-content">
                <!-- /.tab-pane -->
                <div class="tab-pane active" id="tab_4" name="prodDetail">
                    <div class="box box-primary" id="subBox" style="display:none">
                        <div class="box-header with-border">
                        <h3 class="box-title">圖片</h3>
                        </div>
                        <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="image">圖片</label>
                                    <input type="file" id='image' name="image" grid="true"> 
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="rank">排序</label>
                                    <input type="number" class="form-control input-sm" name="rank" grid="rank" >
                                </div> 
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                            <button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
                            <button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
                        </div>
                        </form>
                    </div>
                    
                    <div id="jqxGrid"></div>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
    </div>
</div>


@endsection 
@include('backpack::template.lookup') 
@section('after_scripts')
<script src="{{ asset('vendor/backpack/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/backpack/ckeditor/adapters/jquery.js') }}"></script>
	<script>
        var mainId = "";
        var prodNo = "";
		var editData = null;
		var editObj = null;
		var SAVE_URL = "{{ url(config('backpack.base.route_prefix', 'admin') . '/modNews') }}";

		var fieldData = null;
        var fieldObj = null;
        
        var imgPath = "{{Storage::url('/')}}";

		@if(isset($crud -> create_fields))
		fieldData = '{{!! json_encode($crud->create_fields) !!}}';
		fieldData = fieldData.substring(1);
		fieldData = fieldData.substring(0, fieldData.length - 1);
		fieldObj = JSON.parse(fieldData);
		@endif

		@if(isset($id))
		mainId   = "{{$id}}";
		editData = '{{!! $entry !!}}';
		editData = editData.substring(1);
		editData = editData.substring(0, editData.length - 1);
        var objJson = editData.replace(/(?:\r\n|\r|\n)/g, '<br />');
        console.log(editData);
		//editObj  = JSON.parse(objJson);
		@endif

		$(function () {

			//var formOpt = {};
			formOpt.formId = "mainForm";
			formOpt.editObj = editObj;
			formOpt.fieldObj = fieldObj;
			formOpt.editUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/modNews') }}";
			formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', 'admin') .'/get/mod_news') }}/" + mainId;
			formOpt.saveUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/modNews') }}";
			formOpt.afterInit = function() {
				if(mainId != null) {
                    // $("input[type='file']").not('[grid="true"]').prop("disabled", true);
                    setTimeout(function(){
                        //CKEDITOR.instances['descp'].setReadOnly(true);
                        //CKEDITOR.instances['slogan'].setReadOnly(true);
                        //CKEDITOR.instances['other_content'].setReadOnly(true);
                        
                    }, 500);            
                }                    
                
                getDetailGrid();
                getDetailGrid_type();
                // getBaseinfoGrid();
                // prodTypeSwitch(prodType);
			}

			formOpt.initFieldCustomFunc = function () {

			};

            formOpt.beforesaveFunc = function (){
                var iserror = 'N';

				if($('#title').val() == ""){
					document.getElementById("title").style.backgroundColor = "FCE8E6";
                    iserror = 'Y';
				} else {
                    document.getElementById("title").style.backgroundColor = "";
                }

                if($('#years').val() == ""){
					document.getElementById("years").style.backgroundColor = "FCE8E6";
                    iserror = 'Y';
				}else {
                    document.getElementById("years").style.backgroundColor = "";
                }

                if(iserror ==　"Y") {
                    swal("警告", "請輸入必填欄位", "warning");
					return false;
                }

                return true;
            }

			formOpt.afterDel = function() {

			}

			formOpt.addFunc = function() {
                setTimeout(function(){
                    $("#type").val('銷售資訊');
                    // $("#vendor_nm").val(vendor_nm);
                    // $("#action_sort").val(action_sort);
                    // $("#sort").val(sort);
                }, 500);
			}

			formOpt.editFunc = function() {

                
			}

			formOpt.copyFunc = function() {
                $('#jqxGrid').jqxGrid('clear');
                setTimeout(function(){
                }, 500);
            }

            formOpt.beforeSave = function() {

            }

			formOpt.saveSuccessFunc = function(data) {
                var result = data.data;
			}

			var btnGroup = [

			];

            initBtn(btnGroup);
            
        });

        function getDetailGrid_type() {
            var imagerenderer = function (row, datafield, value) {
                if(value != "") {
                    return '<img style="margin-left: 5px;" height="60" width="50" src="'+ROOT_URL+'/uploads/' + value.replace('public/', '') + '"/>';
                }
                return "";
            }

        }

        function getDetailGrid() {
            var imagerenderer = function (row, datafield, value) {
                if(value != "") {
                    return '<img style="margin-left: 5px;" height="60" width="50" src="'+ROOT_URL+'/uploads/'+ value.replace('public/uploads/', 'uploads/') + '"/>';
                }

                return "";
            }
            var col = [
                [
                    {name: "id", type: "number"},
                    {name: "news_id", type: "string"},
                    {name: "image", type: "string"},
                    // {name: "img_descp1", type: "string"},
                    {name: "rank", type: "number"},
                ],
                [
                    {text: "id", datafield: "id", width: 100, hidden: true},
                    {text: "news_id", datafield: "news_id", width: 150, hidden: true},
                    {text: "圖片1", datafield: "image", width: 150, cellsrenderer: imagerenderer},
                    // {text: "描述1", datafield: "img_descp1", width: 150,hidden: true},
                    {text: "排序", datafield: "rank", width: 150},
                ]
            ];
            var opt = {};
            if(mainId == "" ) {
                mainId = "0";
            }
            opt.gridId = "jqxGrid";
            opt.fieldData = col;
            opt.formId = "subForm";
            opt.saveId = "Save";
            opt.cancelId = "Cancel";
            opt.showBoxId = "subBox";
            opt.height = 300;
            opt.getUrl = "{{ url(config('backpack.base.route_prefix', 'admin').'/modNewsDetail') }}"+ "/"+ mainId;;
            opt.addUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/modNewsDetail') }}" + "/detailStore";
            opt.updateUrl = "{{ url(config('backpack.base.route_prefix', 'admin') . '/modNewsDetail') }}" + "/update/"+ mainId;
            opt.delUrl  = "{{ url(config('backpack.base.route_prefix', 'admin') . '/modNewsDetail') }}" + "/delete/";
            opt.defaultKey = {'news_id': mainId};
            opt.showaggregates = false;
            opt.commonBtn = true;
            opt.inlineEdit = false;
            opt.showtoolbar = true;
            opt.beforeSave = function(formData) {

            }

            opt.afterSave = function(data) {
                console.log(data);
                $("#image").val('');
                // $('#jqxGrid'+'[name="'+'image'+'"]').val('');
            }
            genDetailGrid(opt);
        }
        
    </script>

<script>

$(function(){
    formOpt.initFieldCustomFunc();
    $.get( formOpt.fieldsUrl , function( data ) {
        if(typeof formOpt.afterInit === "function") {
        }
    });
});
</script>
@endsection