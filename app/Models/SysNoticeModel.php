<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SysNoticeModel extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'sys_notice';
	// protected $primaryKey = 'cust_no';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $fillable = ['content','car_no','is_read','notice_time','title','g_key','c_key','s_key','d_key'];
	public $timestamps = false;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	public function insertNotice($title, $content, $car_no, $user) {
		DB::table('sys_notice')->insert([
			'title'   => $title,
			'content' => $content,
			'car_no'  => $car_no,
			'tag_cd'  => 'primary',
			'tag_nm'  => '系統公告',
			'is_read' => 0,
			'g_key'   => $user->g_key,
			'c_key'   => $user->c_key,
			's_key'   => $user->s_key,
			'd_key'   => $user->d_key,
			'notice_time'  => Carbon::now()->toDateTimeString()
		]);
	}

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
	

	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}