<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class CateMgmtModel extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'mod_cate';
	// protected $primaryKey = 'cust_no';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $guarded = array('id');
	public $timestamps = true;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	public function goNext($crud = false)
    {
		$id = $this->attributes['id'];
		return '<a class="btn btn-xs btn-default" href="'.url(config('backpack.base.route_prefix', 'admin')).'/CateMgmt?id='.$id.'"><i class="fa fa-mail-forward"></i> 下一層</a>';
	}
	
	public function createButton($crud = false) {
		$id = request('id');
		//return '<button class="btn btn-sm">test</button>';
		return '<a href="'.url(config('backpack.base.route_prefix', 'admin')).'/CateMgmt/create?id='.$id.'" class="btn btn-primary ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-plus"></i> 新增 類別</span></a>';
	}

	public function backButton($crud = false) {
		//return '<button class="btn btn-sm">test</button>';
		$id = request('id');

		if($id) {
			$parentData = $this::find($id);

			$parentId = $parentData->parent_id;

			if($parentId == -1) {
				return '<a href="'.url(config('backpack.base.route_prefix', 'admin')).'/CateMgmt" class="btn btn-primary ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-reply"></i> 返回</span></a>';
			}
			else {
				return '<a href="'.url(config('backpack.base.route_prefix', 'admin')).'/CateMgmt?id='.$parentId.'" class="btn btn-primary ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-reply"></i> 返回</span></a>';
			}
		}

		
		
		return '<a href="'.url(config('backpack.base.route_prefix', 'admin')).'/CateMgmt" class="btn btn-primary ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-reply"></i> 返回</span></a>';
	}

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}