<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class MemberMgmtModel extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'mod_member';
	// protected $primaryKey = 'cust_no';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $guarded = array('id');
	public $timestamps = true;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

	public function members()
    {
        return $this->hasMany('App\Models\Discount', 'phone');
	}
	
	public function memberUpdate($name, $phone, $zip, $city, $area, $addr, $id) {
		$user = $this::find($id);

		if(isset($user) && $user->is_updated != 'Y') {
			$this::find($id)->update([
				'name'      => $name,
				'cellphone' => $phone,
				'dlv_zip'   => $zip,
				'dlv_city'  => $city,
				'dlv_area'  => $area,
				'dlv_addr'  => $addr
			]);
		}
		
		return;
	}

	public function cardRemember($userId, $cardToken, $cardKey, $lastFour) {
		$user = $this::find($userId);

		$user->card_token = $cardToken;
		$user->card_key   = $cardKey;
		$user->last_four  = $lastFour;

		$user->save();
	}

	public function getMemberData($userId) {
		return $this::find($userId);
	}

	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}