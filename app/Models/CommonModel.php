<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use DB;
use Crypt;
use Shorty;

class CommonModel extends Model {

    public static  function processData($request, $fields) {
        $user = Auth::user();
        
        foreach($fields as $key=>$value) {
            if($value['type']  == 'checkbox' || $value['type'] == 'custom_html' || $value['type'] == 'checklist') {

                $postVal = $request[$key];
                if(count($request[$key]) > 0) {
                    $request[$key] = join(',', $postVal);
                }
                else {
                    $request[$key] = '';
                }

            }
            else if($value['type'] == 'select2_multiple' || $value['type'] == 'select_multiple' || $value['type'] == 'select2_from_ajax' || $value['type'] == 'select2_from_array') {
                $postVal = $request[$key];
                if(is_array($request[$key]) && count($request[$key]) > 0) {
                    $request[$key] = join(',', $postVal);
                    if($key == 'cate_id') {
                        $request[$key] = (int)$request[$key];
                    }
                }
                else {
                    $request[$key] = '';
                }
            }

            if($value['name'] == 'g_key'){
                $request[$key] = $user->g_key;
            }else if($value['name'] == 'c_key'){
                $request[$key] = $user->c_key;
            }else if($value['name'] == 's_key'){
                $request[$key] = $user->s_key;
            }else if($value['name'] == 'd_key'){
                $request[$key] = $user->d_key;
            }else if($value['name'] == 'created_by' && $request['created_by'] == null){
                $request[$key] = $user->email;
            }else if($value['name'] == 'updated_by'){
                $request[$key] = $user->email;
            }
        }

        return $request;
    }


    public function insertZip() {
        $str = file_get_contents('zip.json');
        $obj = json_decode($str);
        $insertArray = array();
        foreach($obj as $key=>$row) {
            

            $city = mb_substr($row->行政區名,0,3,'utf8');
            $area = mb_substr($row->行政區名,3,6,'utf8');
            $zip = $row->_x0033_碼郵遞區號;
            $a = array(
                "cntry_cd"   => "TW",
                "cntry_nm"   => "台灣",
                "city_nm"    => $city,
                "dist_nm"    => $area,
                "zip_f"      => $zip,
                "zip_e"      => $zip,
                "g_key"      => 'STD',
                "c_key"      => 'STD',
                "s_key"      => 'STD',
                "d_key"      => 'STD',
                "created_by" => 'TIM',
                "updated_by" => 'TIM',
                'created_at'          => \Carbon::now(),
                'updated_at'          => \Carbon::now(),
            );
            array_push($insertArray, $a);
            // foreach($row as $idx=>$sec) {
            //     $a = array(
            //         "cntry_cd"   => "TW",
            //         "cntry_nm"   => "台灣",
            //         "city_nm"    => $key,
            //         "dist_nm"    => $idx,
            //         "zip_f"      => $sec,
            //         "zip_e"      => $sec,
            //         "g_key"      => 'STD',
            //         "c_key"      => 'STD',
            //         "s_key"      => 'STD',
            //         "d_key"      => 'STD',
            //         "created_by" => 'TIM',
            //         "updated_by" => 'TIM',
            //         'created_at'          => \Carbon::now(),
			// 	    'updated_at'          => \Carbon::now(),
            //     );
            //     array_push($insertArray, $a);
            // }
            
            //dd($insertArray);
            
        }
        DB::table('sys_area')->insert($insertArray);
        return true;
    }

    public static function sendSmsMsg($msg, $phone) {
        $mitake = app(\Mitake\Client::class);

		$message = (new \Mitake\Message\Message())
			->setDstaddr($phone)
			->setSmbody($msg);
        $result = $mitake->send($message);
        
        return true;
    }


    public function sendMsg($ordNo, $dlvPhone) {
        $url = 'https://www.z-trip.com/orderQrCode/'.Crypt::encrypt($ordNo);
        $url = Shorty::shorten($url);

        $mitake = app(\Mitake\Client::class);
        $message = (new \Mitake\Message\Message())
        ->setDstaddr($dlvPhone)
        ->setSmbody('感謝您選擇Z-TRIP.COM出國商品，請於兩天後到機場櫃位出示連結內的訂購資訊領取機器:'.$url);
        $result = $mitake->send($message);

        echo 'success';
    }

    
}