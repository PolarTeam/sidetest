<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\Mail\messagemail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
class MailFormatModel extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'mail_format';
	// protected $primaryKey = 'id';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $fillable = ['title','type','name','content','created_by','updated_by','g_key','c_key','s_key','d_key'];
	public $timestamps = true;
	
	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

	public function sendmail()
    {
        $sendData = DB::table('mod_message')
        ->whereNull('send_mail')
        ->get();
		try {
			foreach($sendData as $row) {
				$mailData = DB::table('bscode')
				->where('cd_type','SENDMAIL')
				->pluck('cd');
				$sendData = [
					'name' => $row->name,
					'phone' => $row->phone,
					'subject' => $row->subject,
					'message' => $row->message,
				];
				\Log::info("nick");
					Mail::to($mailData)
					->send(new messagemail($sendData));
					$this->updatesendmail($row,'Y');
			}
		}
		catch(\Exception $e) {
			\Log::error($e->getMessage());
		}
        return ;
    }

	public function updatesendmail($row,$status) {
        DB::table('mod_message')
        ->where('id', $row->id)
        ->update([
            'send_mail' => $status,
            'updated_at' => \Carbon::now()]);
    }

	public function articles()
    {
        return $this->hasMany('App\Models\Article', 'article_tag');
    }

	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}