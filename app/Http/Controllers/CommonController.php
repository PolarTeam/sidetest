<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\CommonModel;
use App\Models\CustomerItemModel;
use App\Models\CustomerProfileModel;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Hash;
use HTML;
use Excel;

class CommonController extends Controller
{
    
    public function getData($table=null,$id=null) {
        $data = DB::table($table)->where('id', $id)->first();

        return response()->json($data);
    }

    public function getCompnayList() {
        $gData = DB::table('sys_customers')->where('type', 'SELF')->where('identity', 'G')->get();
        $cData = DB::table('sys_customers')->where('type', 'SELF')->where('identity', 'C')->get();
        $sData = DB::table('sys_customers')->where('type', 'SELF')->where('identity', 'S')->get();
        $dData = DB::table('sys_customers')->where('type', 'SELF')->where('identity', 'D')->get();

        $data = array(
            'gData' => $gData,
            'cData' => $cData,
            'sData' => $sData,
            'dData' => $dData,
        );
        
        return response()->json($data);
    }

    public function getSearchLayoutList($key) {
        $user = Auth::user();

        $data = DB::table('sys_search_layout')
                    ->select('id', 'title', 'layout_default')
                    ->where('key', $key)
                    ->where('created_by', $user->email)
                    ->get();
        
        return response()->json(['msg' => 'success', 'data' => $data]);
    }

    public function getSearchHtml($id=null) {
        return response()->json(['msg' => 'success', 'data' => DB::table('sys_search_layout')->where('id', $id)->first()]);
    }

    
    public function saveSearchLayout(Request $request) {
        $user = Auth::user();

        $data  = $request->data;
        $title = $request->title;
        $id    = $request->id;
        $key   = $request->key;

        if($id == null) {
            $id = DB::table('sys_search_layout')->insertGetId([
                'key'        => $key,
                'data'       => $data,
                'title'      => $title,
                'g_key'      => $user->g_key,
                'c_key'      => $user->c_key,
                's_key'      => $user->s_key,
                'd_key'      => $user->d_key,
                'created_by' => $user->email,
                'updated_by' => $user->email,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ]);
        }
        else {
            DB::table('sys_search_layout')
                ->where('id', $id)
                ->update([
                    'data'       => $data,
                    'updated_by' => $user->email,
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]);
        }

        return response()->json(['msg' => 'success', 'id' => $id]);
    }

    public function delSearchLayout($id=null) {
        DB::table('sys_search_layout')
            ->where('id', $id)
            ->delete();
            
        return response()->json(['msg' => 'success']);    
    }

    public function setSearchDefault($key, $id) {
        $user = Auth::user();

        DB::table('sys_search_layout')
                ->where('key', $key)
                ->where('created_by', $user->email)
                ->update([
                    'layout_default' => 'N'
                ]);
        
        DB::table('sys_search_layout')
                ->where('id', $id)
                ->update([
                    'layout_default' => 'Y'
                ]);

        return response()->json(['msg' => 'success']);
    }

    public function getCarType() {
        $user = Auth::user();
        $carType = request('carType');
        $result = array();
        if(isset($carType)) {
            $a = explode(',', $carType);

            $result = DB::table('bscode')
                ->select('cd', 'cd_descp')
                ->where('cd_type', 'CARTYPE')
                ->whereIn('cd', $a)
                ->where('g_key', $user->g_key)
                ->where('c_key', $user->c_key)
                ->get();
        }

        return response()->json($result);
    }

    public function updatePwd() {
        $password = request('password');
        $confirmPassword = request('confirmPassword');
        $originalPassword = request('originalPassword');

        if($password != $confirmPassword) {
            return response()->json(['msg' => 'error', 'msgLog' => '密碼不一致，請再檢查']);
        }

        $user = Auth::user();

        if (!(Hash::check($originalPassword, $user->password))) {
            return response()->json(['msg' => 'error', 'msgLog' => '原始密碼錯誤']);
        }
        $user->password = bcrypt($password);
        $user->save();

        return response()->json(['msg' => 'success']);
    }

    public function exportData(Request $request) {

        //dd(json_encode($request->content));
        $d = json_decode($request->content);

        $today = date('Y-m-d');
        $fileName = $request->filename;        
        
        Excel::create($fileName.'-'.$today, function($excel) use($d,$fileName) {
            $excel->sheet($fileName, function($sheet) use($d) {
                $header = array();
                if(count($d) > 0) {
                    foreach($d[0] as $key=>$val) {
                        array_push($header, $key);
                    }
                }
                $sheet->row(1, $header);
                
                foreach($d as $key=>$row) {
                    $body = array();

                    foreach($d[$key] as $val) {
                        if(preg_match("/^[0-9,.]+$/", $val)) {
                            $val = str_replace(',', '', $val);
                        }
                        array_push($body, $val);
                    }

                    $sheet->row($key+2, $body);
                }
                
        
            });
            
        })->export('xls');   

    }
}
