<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\NewsCrudRequest as StoreRequest;
use App\Http\Requests\NewsCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Auth;

class CategoryCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\Category');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/category');
        $this->crud->setEntityNameStrings('Category', 'Categorys');

        $this->crud->setColumns([
            [
                'name' => 'name',
                'label' => "名稱",
                'type'  => 'text'
            ],
            [
                'name' => 'created_at',
                'label' => "時間",
                'type'  => 'text'
            ]
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => '名稱'
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'created_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'updated_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}