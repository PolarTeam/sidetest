<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\MarqueeCrudRequest as StoreRequest;
use App\Http\Requests\MarqueeCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;

class MarqueeCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\Marquee');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/modMarquee');
        $this->crud->setEntityNameStrings('Marquee', 'Marquees');

        $this->crud->setColumns([
            [
                'name' => 'descp',
                'label' => "描述",
                'type'  => 'text'
            ],
            [
                'name' => 'link',
                'label' => "連結",
                'type'  => 'text'
            ],
            [
                'name' => 'created_by',
                'label' => "建立者",
                'type'  => 'text'
            ],
            [
                'name' => 'created_at',
                'label' => "時間",
                'type'  => 'text'
            ]
        ]);

        $this->crud->addField([
            'name' => 'link',
            'label' => '連結',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
            ],
        ]);


        $this->crud->addField([
            'name' => 'descp',
            'label' => '描述',
            // optional:
            'extra_plugins' => ['oembed', 'widget','justify']
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'created_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'updated_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);
        $this->crud->addField([   // Hidden
            'name' => 'g_key',
            'type' => 'hidden',
            'value' => 'STD'
        ]);
        $this->crud->addField([   // Hidden
            'name' => 'c_key',
            'type' => 'hidden',
            'value' => 'STD'
        ]);
        $this->crud->addField([   // Hidden
            'name' => 's_key',
            'type' => 'hidden',
            'value' => 'STD'
        ]);
        $this->crud->addField([   // Hidden
            'name' => 'd_key',
            'type' => 'hidden',
            'value' => 'STD'
        ]);

        
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
    }

}