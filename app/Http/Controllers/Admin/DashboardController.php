<?php

namespace App\Http\Controllers\Admin;

use Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;


class DashboardController extends Controller
{
    
    public function dashboard() {
        $title = trans('dashboard.titleName');
        $viewData = array(
        );
        return view('dashboard.dashboard')->with('viewData', $viewData);
    }
    
    public function map() {
        $title = trans('dashboard.map');
        echo "<title>$title</title>";
        return view('dashboard.map');
    }

    public function getInfo() {
        
        
    }
}
