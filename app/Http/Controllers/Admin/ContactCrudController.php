<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ContactCrudRequest as StoreRequest;
use App\Http\Requests\ContactCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ContactCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\Contact');
        $this->crud->denyAccess(['create']);
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/contact');
        $this->crud->setEntityNameStrings('Contact', 'Contacts');

        $this->crud->setColumns([
            [
                'name' => 'subject',
                'label' => "主旨",
                'type'  => 'text'
            ],
            [
                'name' => 'name',
                'label' => "客戶名稱",
                'type'  => 'text'
            ],
            [
                'name' => 'email',
                'label' => "E-mail",
                'type'  => 'text'
            ],
            [
                'name' => 'message',
                'label' => "內容",
                'type'  => 'text'
            ],
            // [
            //     'name' => 'created_by',
            //     'label' => "建立者",
            //     'type'  => 'text'
            // ],
            // [
            //     'name' => 'created_at',
            //     'label' => "時間",
            //     'type'  => 'text'
            // ]
        ]);

        $this->crud->addField([
            'name' => 'subject',
            'label' => '主旨'
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => '客戶名稱'
        ]);

        $this->crud->addField([
            'name' => 'email',
            'label' => 'E-mail'
        ]);

        $this->crud->addField([
            'name' => 'message',
            'label' => '內容',
            'type' => 'textarea'
        ]);
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
    }

}