<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\modNewsCrudRequest as StoreRequest;
use App\Http\Requests\modNewsCrudRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\CommonModel;
use App\Models\modNews;
use App\Models\modNewsDetailModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use File;
use Response;
class modNewsCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\modNews');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/modNews');
        $this->crud->setEntityNameStrings('modNews', 'modNews');


        $this->crud->setColumns(['name']);
        $this->crud->enableAjaxTable();
        $this->crud->setCreateView('modNews.edit');
        $this->crud->setEditView('modNews.edit');
        $this->crud->setListView('modNews.index');

        $this->crud->addField([
            'name' => 'content',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'title',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'years',
            'type' => 'text'
        ]);
        
        $this->crud->addField([
            'name' => 'type',
            'type' => 'select',
            'options' => DB::table('bscode')->select('cd as code', 'cd as descp')->where('cd_type', 'NEWSTYPE')->get()
        ]);

    }
    public function index() {
        $user = Auth::user();

        return view($this->crud->getListView());
    }

	public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $type = $request->cust_type;    
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::storeCrud($request);

            $modNewsData = modNews::find($this->data['entry']->getKey());
            $data = $request->all();
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
    }
    public function create()
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['prodType'] = request('prodType');
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
        $this->data['id'] = $id;


        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }

    public function getimg($id=null) {

        $prodDetail = [];
        if($id != 0) {
            $this_query = DB::table('mod_news_detail');
            $this_query->where('news_id', $id);
            $prodDetail = $this_query->get();
        }
        
        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }

    public function cateStore(Request $request)
    {
        $prodDetail = new modNewsDetailModel;
        $validator = $this->detailValidator($request);      
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $user = Auth::user();
            $request->merge(array('created_by' => $user->email));
            $request->merge(array('updated_by' => $user->email));
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();
            $prodDetailData = modNewsDetailModel::find($prodDetail->id);

            $data = $request->all();
            if(isset($data['image'])) {
                $oldPath = $prodDetail['image'];
                Storage::delete($oldPath);
                $img = $data['image'];
                $path = Storage::putFile('images', $img);
                $prodDetailData['image'] = $path;
                $prodDetailData->save();
            }
        }
        return ["msg"=>"success", "data"=>$prodDetail->where('id', $prodDetailData->id)->get()];
    }

    public function cateUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->detailValidator($request);
        $now = date('YmdHis');
        $prodDetail_INFO = modNewsDetailModel::find($request->id);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $prodDetail = modNewsDetailModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();
        }
        $prodDetail = modNewsDetailModel::find($request->id);
        $data = $request->all();
        if(isset($data['image'])) {
            $oldPath = $prodDetail['image'];
            Storage::delete($oldPath);
            $img = $data['image'];
            $path = Storage::putFile('images', $img);
            $prodDetail['image'] = $path;
            $prodDetail->save();
        }

        return ["msg"=>"success", "data"=>$prodDetail->where('id', $request->id)->get()];
    }
    public function cateDel($id)
    {
        $prodDetail = modNewsDetailModel::find($id);
        Storage::delete($prodDetail->imgage);
        $prodDetail->delete();

        return ["msg"=>"success"];
    }
    

    public function multiDel() {
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $prodMgmtModel = modNews::find($ids[$i]);
                // DB::table('mod_modNews')->where('id', $prodMgmtModel->id)->delete();
                $prodMgmtModel->delete();
                
            }
        }

        return response()->json(array('msg' => 'success'));
    }

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        $modNewsData = array();
        unset($request['created_by']);

        try {
            $response = parent::updateCrud($request);
            $modNewsData = modNews::find($request->id);  
        }
        catch (\Exception $e) {            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }

        
        return ["msg"=>"success", "response"=>$response, "data" => $modNewsData];
    }

    public function infoget($id=null) {
        $prodDetail = [];
        if($id != 0) {
            $this_query = DB::table('mod_menu_detail');
            $this_query->where('menu_id', $id);
            $prodDetail = $this_query->get();
            // mod_promotion_info
        }
        
        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }


    public function get($id=null) {

        $prodDetail = [];
        if($id != 0) {
            $this_query = DB::table('mod_news_detail');
            $this_query->where('news_id', $id);
            $prodDetail = $this_query->get();
        }
        
        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }

    public function detailStore(Request $request)
    {
        $validator = $this->detailValidator($request);        
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $user = Auth::user();
            $request->merge(array('created_by' => $user->email));
            $request->merge(array('updated_by' => $user->email));
            
            $prodDetail = new modNewsDetailModel;
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();

            $prodDetailData = modNewsDetailModel::find($prodDetail->id);
            $data = $request->all();

            if(isset($data['img1'])) {
                $img = $data['img1'];
                $path = Storage::putFile('img1', $img);
                $prodDetailData['img1'] = $path;
                $prodDetailData->save();
            }
        }

        return ["msg"=>"success", "data"=>$prodDetail->where('id', $prodDetailData->id)->get()];
    }

    public function detailUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->detailValidator($request);
        $now = date('YmdHis');
        $prodDetail_INFO = modNewsDetailModel::find($request->id);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $prodDetail = modNewsDetailModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();

            $prodDetailData = modNewsDetailModel::find($prodDetail->id);
            $data = $request->all();
            if(isset($data['img1'])) {
                $oldPath = $prodDetail['img1'];
                
                Storage::delete($oldPath);
                $img = $data['img1'];
                $path = Storage::putFile('img1', $img);
                $prodDetailData['img1'] = $path;
                $prodDetailData->save();
            }
        }


        return ["msg"=>"success", "data"=>$prodDetail->where('id', $request->id)->get()];
    }
    public function detailDel($id)
    {
        $prodDetail = modNewsDetailModel::find($id);
        Storage::delete($prodDetail->img1);
        $prodDetail->delete();

        return ["msg"=>"success"];
    }

}