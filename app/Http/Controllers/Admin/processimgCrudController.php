<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BannerCrudRequest as StoreRequest;
use App\Http\Requests\BannerCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Validator;
use App\Models\CommonModel;
use App\Models\process;
use App\Models\processimg;
use App\Models\processimgDetailModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use File;
use Response;

class processimgCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\processimg');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/processimg');
        $this->crud->setEntityNameStrings('工程進度', '工程進度');

        $this->crud->setColumns(['name']);
        $this->crud->enableAjaxTable();

        $this->crud->setCreateView('processimg.edit');
        $this->crud->setEditView('processimg.edit');
        $this->crud->setListView('processimg.index');


        $this->crud->addField([
            'name' => 'title',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'sub_title',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'content1',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'content2',
            'type' => 'text'
        ]);


        $this->crud->addField([
            'name' => 'process_id',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'process_name',
            'type' => 'lookup',
            'title' => '位置',
            'info1' => Crypt::encrypt('process_view'), //table
            'info2' => Crypt::encrypt("img_descp1+title,title,id,img_descp1"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "title=process_name;id=process_id;img_descp1=process_years;" //field mapping
        ]);
        $this->crud->addField([
            'name' => 'process_years',
            'type' => 'text'
        ]);
        
        $this->crud->addField([
            'name' => 'remark',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'planning',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'link',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'tracking',
            'type' => 'text'
        ]);
        

        $this->crud->addField([
            'name' => 'years',
            'type' => 'text',
        ]);

    }

    public function index() {
        $user = Auth::user();

        return view($this->crud->getListView());
    }

	public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $type = $request->cust_type;    
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::storeCrud($request);

            $prodData = process::find($this->data['entry']->getKey());
            $data = $request->all();
            for($i=1; $i<=5; $i++) {

                if(isset($data['img'.$i])) {
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads/', $img);
                    $prodData['img'.$i] = $path;
                    $prodData->save();
                }
                
            }
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
    }
    public function create()
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['prodType'] = request('prodType');
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
        $this->data['id'] = $id;


        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }

    public function imgget($id=null) {

        $prodDetail = [];
        if($id != 0) {
            $this_query = DB::table('mod_processbanner');
            $this_query->where('process_id', $id);
            $prodDetail = $this_query->get();
        }
        
        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }


    public function get($id=null) {

        $prodDetail = [];
        if($id != 0) {
            $this_query = DB::table('mod_processimg_detail');
            $this_query->where('processimg_id', $id);
            $prodDetail = $this_query->get();
        }
        
        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }

    public function detailStore(Request $request)
    {
        $validator = $this->detailValidator($request);        
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $user = Auth::user();
            $request->merge(array('created_by' => $user->email));
            $request->merge(array('updated_by' => $user->email));
            
            $prodDetail = new processimgDetailModel;
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();

            $prodDetailData = processimgDetailModel::find($prodDetail->id);
            $data = $request->all();
            for($i=1; $i<=1; $i++) {

                if(isset($data['img'.$i])) {
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads/', $img);
                    $prodDetailData['img'.$i] = $path;
                    $prodDetailData->save();
                }
                
            }
        }

        return ["msg"=>"success", "data"=>$prodDetail->where('id', $prodDetailData->id)->get()];
    }

    public function detailUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->detailValidator($request);
        $now = date('YmdHis');
        $prodDetail_INFO = processimgDetailModel::find($request->id);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $prodDetail = processimgDetailModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();

            $prodDetailData = processimgDetailModel::find($prodDetail->id);
            $data = $request->all();
            for($i=1; $i<=1; $i++) {

                if(isset($data['img'.$i])) {
                    $oldPath = $prodDetail['img'.$i];
                    
                    Storage::delete($oldPath);
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads', $img);
                    $prodDetailData['img'.$i] = $path;
                    $prodDetailData->save();
                }
            }
        }


        return ["msg"=>"success", "data"=>$prodDetail->where('id', $request->id)->get()];
    }
    public function detailDel($id)
    {
        $prodDetail = processimgDetailModel::find($id);
        // Storage::delete($prodDetail->img1);
        $prodDetail->delete();

        return ["msg"=>"success"];
    }

    public function multiDel() {
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $prodMgmtModel = processimg::find($ids[$i]);

                $prodMgmtModel->delete();
                
            }
        }

        return response()->json(array('msg' => 'success'));
    }

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        $oldData = process::find($request->id);
        $prodData = array();
        unset($request['created_by']);

        try {
            $response = parent::updateCrud($request);
            $prodData = process::find($request->id);
            $data = $request->all();
            if(isset($data['img1'])) {
                $oldPath = $oldData['img1'];
                
                Storage::delete($oldPath);
                $img = $data['img1'];
                $path = Storage::putFile('images', $img);
                $prodData['img1'] = $path;
                $prodData->save();
            }         
        }
        catch (\Exception $e) {            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }

        
        return ["msg"=>"success", "response"=>$response, "data" => $prodData];
    }

}