<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PageCrudRequest as StoreRequest;
use App\Http\Requests\PageCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;

class PageCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\Page');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/page');
        $this->crud->setEntityNameStrings('畫面', '畫面');


        $this->crud->setColumns([
            [
                'name' => 'name',
                'label' => "標題",
                'type'  => 'text'
            ],
            // [
            //     'name' => 'template',
            //     'label' => "摘要",
            //     'type'  => 'text'
            // ],
            [
                'name' => 'slug',
                'label' => "網站",
                'type'  => 'text'
            ],
            [
                'name' => 'created_at',
                'label' => "時間",
                'type'  => 'text'
            ]
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => '名稱',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
            ],
            // 'disabled' => 'disabled'
        ]);
        $this->crud->addField([
            'name' => 'title',
            'label' => '標題',
            'type' => 'text',
            // 'disabled' => 'disabled'
        ]);
        $this->crud->addField([   // Upload
            'name' => 'image',
            'label' => '圖片',
            'type' => 'upload',
            'upload' => true,
            'disk' => 'public' // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
        ]);
        $this->crud->addField([
            'name' => 'content',
            'label' => '內容',
            'type' => 'wysiwyg',
            'placeholder' => trans('backpack::pagemanager.content_placeholder'),
        ]);
        
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
    }
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->entity_name_plural;


        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }

}