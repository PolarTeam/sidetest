<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\mailCrudRequest as StoreRequest;
use App\Http\Requests\mailCrudRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\aboutModel;
use App\Models\CommonModel;
use Response;
class aboutCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\aboutModel');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/about');
        $this->crud->setEntityNameStrings('about', 'about');


        $this->crud->setColumns(['name']);
        $this->crud->enableAjaxTable();
        $this->crud->setCreateView('about.edit');
        $this->crud->setEditView('about.edit');
        $this->crud->setListView('about.index');

        // $this->crud->addField([
        //     'name' => 'title',
        //     'label' => '標題'
        // ]);
        
        $this->crud->addField([   // CKEditor
            'name' => 'content',
            'label' => '內容',
            'type' => 'ckeditor',
            // optional:
            'extra_plugins' => [ 'widget']
        ]);

        $this->crud->addField([
            'name' => 'phone',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'subject',
            'type' => 'textarea'
        ]);

        $this->crud->addField([
            'name' => 'message',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'created_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);


    }
    public function index() {
        $user = Auth::user();

        return view($this->crud->getListView());
    }

	public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $type = $request->cust_type;    
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::storeCrud($request);

            $prodData = aboutModel::find($this->data['entry']->getKey());
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
    }
    public function create()
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['prodType'] = request('prodType');
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');
        $request->merge(['oncar' => $oncarstr]);
        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
        $this->data['id'] = $id;


        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }





    public function multiDel() {
        $ids = request('ids');

        // if(count($ids) > 0) {
        //     for($i=0; $i<count($ids); $i++) {
        //         $prodMgmtModel = aboutModel::find($ids[$i]);
        //         DB::table('mod_message')->where('id', $prodMgmtModel->id)->delete();
        //         $prodMgmtModel->delete();
                
        //     }
        // }

        return response()->json(array('msg' => 'success'));
    }

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        $oldData = aboutModel::find($request->id);
        $prodData = array();
        unset($request['created_by']);

        try {
            $response = parent::updateCrud($request);
            $prodData = aboutModel::find($request->id);
            $data = $request->all();        
        }
        catch (\Exception $e) {            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }

        
        return ["msg"=>"success", "response"=>$response, "data" => $prodData];
    }

}