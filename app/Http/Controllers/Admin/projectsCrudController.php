<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\projectsCrudRequest as StoreRequest;
use App\Http\Requests\projectsCrudRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\CommonModel;
use App\Models\projects;
use App\Models\projectsDetailModel;
use App\Models\projectsTypeModel;
use App\Models\projectsinfoModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use File;
use Response;
class projectsCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\projects');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/projects');
        $this->crud->setEntityNameStrings('projects', 'projects');


        $this->crud->setColumns(['name']);
        $this->crud->enableAjaxTable();
        $this->crud->setCreateView('Projects.edit');
        $this->crud->setEditView('Projects.edit');
        $this->crud->setListView('Projects.index');

        $this->crud->addField([
            'name' => 'title',
            'type' => 'textarea'
        ]);

        $this->crud->addField([
            'name' => 'type',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'sorted',
            'type' => 'number'
        ]);
        $this->crud->addField([
            'name' => 'structure',
            'type' => 'select',
            'options' => DB::table('bscode')->select('cd as code', 'cd as descp')->where('cd_type', 'BUILDTYPE')->get()
        ]);
        $this->crud->addField([
            'name' => 'web_link',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'finish_at',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'facebook',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'tracking',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'phone',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'hall',
            'type' => 'text',
        ]);

    }
    public function index() {
        $user = Auth::user();

        return view($this->crud->getListView());
    }

	public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $type = $request->cust_type;    
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::storeCrud($request);

            $prodData = projects::find($this->data['entry']->getKey());
            $data = $request->all();
            for($i=1; $i<=2; $i++) {

                if(isset($data['img'.$i])) {
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads', $img);
                    $prodData['img'.$i] = $path;
                    $prodData->save();
                }
                
            }
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
    }
    public function create()
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['prodType'] = request('prodType');
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
        $this->data['id'] = $id;


        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }

    public function getcate($id=null) {

        $prodDetail = [];
        if($id != 0) {
            $this_query = DB::table('mod_projects_type');
            $this_query->where('projects_id', $id);
            $prodDetail = $this_query->get();
        }
        
        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }

    public function cateStore(Request $request)
    {
        $prodDetail = new projectsTypeModel;
        $validator = $this->detailValidator($request);      
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $user = Auth::user();
            $request->merge(array('created_by' => $user->email));
            $request->merge(array('updated_by' => $user->email));
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();
            $prodDetailData = projectsTypeModel::find($prodDetail->id);

            $data = $request->all();
            for($i=1; $i<=1; $i++) {

                if(isset($data['img'.$i])) {
                    $oldPath = $prodDetail['img'.$i];
                    Storage::delete($oldPath);
                    $img = $data['img'.$i];
                    $path = Storage::putFile('images', $img);
                    $prodDetailData['img'.$i] = $path;
                    $prodDetailData->save();
                }
            }
        }
        return ["msg"=>"success", "data"=>$prodDetail->where('id', $prodDetailData->id)->get()];
    }

    public function cateUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->detailValidator($request);
        $now = date('YmdHis');
        $prodDetail_INFO = projectsTypeModel::find($request->id);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $prodDetail = projectsTypeModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();
        }
        $prodDetail = projectsTypeModel::find($request->id);
        $data = $request->all();
        for($i=1; $i<=2; $i++) {

            if(isset($data['img'.$i])) {
                $oldPath = $prodDetail['img'.$i];
                Storage::delete($oldPath);
                $img = $data['img'.$i];
                $path = Storage::putFile('images', $img);
                $prodDetail['img'.$i] = $path;
                $prodDetail->save();
            }
        }

        return ["msg"=>"success", "data"=>$prodDetail->where('id', $request->id)->get()];
    }
    public function cateDel($id)
    {
        $prodDetail = projectsTypeModel::find($id);
        Storage::delete($prodDetail->img1);
        $prodDetail->delete();

        return ["msg"=>"success"];
    }
    
    public function get($id=null) {

        $prodDetail = [];
        if($id != 0) {
            $this_query = DB::table('mod_projects_detail');
            $this_query->where('projects_id', $id);
            $prodDetail = $this_query->get();
        }
        
        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }

    public function detailStore(Request $request)
    {
        $validator = $this->detailValidator($request);        
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $user = Auth::user();
            $request->merge(array('created_by' => $user->email));
            $request->merge(array('updated_by' => $user->email));
            
            $prodDetail = new projectsDetailModel;
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();

            $prodDetailData = projectsDetailModel::find($prodDetail->id);
            $data = $request->all();
            for($i=1; $i<=1; $i++) {

                if(isset($data['img'.$i])) {
                    $img = $data['img'.$i];
                    $path = Storage::putFile('images', $img);
                    $prodDetailData['img'.$i] = $path;
                    $prodDetailData->save();
                }
                
            }
        }

        return ["msg"=>"success", "data"=>$prodDetail->where('id', $prodDetailData->id)->get()];
    }

    public function detailUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->detailValidator($request);
        $now = date('YmdHis');
        $prodDetail_INFO = projectsDetailModel::find($request->id);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $prodDetail = projectsDetailModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();

            $prodDetailData = projectsDetailModel::find($prodDetail->id);
            $data = $request->all();
            for($i=1; $i<=1; $i++) {

                if(isset($data['img'.$i])) {
                    $oldPath = $prodDetail['img'.$i];
                    
                    Storage::delete($oldPath);
                    $img = $data['img'.$i];
                    $path = Storage::putFile('images', $img);
                    $prodDetailData['img'.$i] = $path;
                    $prodDetailData->save();
                }
            }
        }


        return ["msg"=>"success", "data"=>$prodDetail->where('id', $request->id)->get()];
    }
    public function detailDel($id)
    {
        $prodDetail = projectsDetailModel::find($id);
        Storage::delete($prodDetail->img1);
        $prodDetail->delete();

        return ["msg"=>"success"];
    }

    public function multiDel() {
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $prodMgmtModel = projects::find($ids[$i]);
                DB::table('mod_projects')->where('id', $prodMgmtModel->id)->delete();
                $prodMgmtModel->delete();
                
            }
        }

        return response()->json(array('msg' => 'success'));
    }

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        $oldData = projects::find($request->id);
        $prodData = array();
        unset($request['created_by']);

        try {
            $response = parent::updateCrud($request);
            $prodData = projects::find($request->id);
            $data = $request->all();
            if(isset($data['img1'])) {
                $oldPath = $oldData['img1'];
                
                Storage::delete($oldPath);
                $img = $data['img1'];
                $path = Storage::putFile('images', $img);
                $prodData['img1'] = $path;
                $prodData->save();
            }
            if(isset($data['img2'])) {
                $oldPath = $oldData['img2'];
                
                Storage::delete($oldPath);
                $img = $data['img2'];
                $path = Storage::putFile('images', $img);
                $prodData['img2'] = $path;
                $prodData->save();
            }      
        }
        catch (\Exception $e) {            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }

        
        return ["msg"=>"success", "response"=>$response, "data" => $prodData];
    }

    public function infoget($id=null) {
        $prodDetail = [];
        if($id != 0) {
            $this_query = DB::table('mod_projects_info');
            $this_query->where('projects_id', $id);
            $prodDetail = $this_query->get();
            // mod_promotion_info
        }
        
        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }

    public function infoStore(Request $request)
    {
        $prodDetail = new projectsinfoModel;
        $validator = $this->detailValidator($request);      
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $user = Auth::user();
            $request->merge(array('created_by' => $user->email));
            $request->merge(array('updated_by' => $user->email));
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();
            $prodDetailData = projectsinfoModel::find($prodDetail->id);
        }
        return ["msg"=>"success", "data"=>$prodDetail->where('id', $prodDetailData->id)->get()];
    }

    public function infoUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->detailValidator($request);
        $now = date('YmdHis');
        $prodDetail_INFO = projectsinfoModel::find($request->id);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $prodDetail = projectsinfoModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();
        }

        return ["msg"=>"success", "data"=>$prodDetail->where('id', $request->id)->get()];
    }
    
    public function infoDel($id)
    {
        $prodDetail = projectsinfoModel::find($id);
        // Storage::delete($prodDetail->img1);
        $prodDetail->delete();

        return ["msg"=>"success"];
    }

}