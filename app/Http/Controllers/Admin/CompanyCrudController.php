<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CompanyCrudRequest as StoreRequest;
use App\Http\Requests\CompanyCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CompanyCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\Company');
        $this->crud->denyAccess(['delete', 'create']);
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/company');
        $this->crud->setEntityNameStrings('Company', 'Companys');

        $this->crud->setColumns([
            [
                'name' => 'cname',
                'label' => "公司名稱",
                'type'  => 'text'
            ],
            [
                'name' => 'ename',
                'label' => "公司名稱(英)",
                'type'  => 'text'
            ],
            [
                'name' => 'email',
                'label' => "E-mail",
                'type'  => 'text'
            ],
            [
                'name' => 'address',
                'label' => "地址",
                'type'  => 'text'
            ],
            // [
            //     'name' => 'created_by',
            //     'label' => "建立者",
            //     'type'  => 'text'
            // ],
            // [
            //     'name' => 'created_at',
            //     'label' => "時間",
            //     'type'  => 'text'
            // ]
        ]);

        $this->crud->addField([
            'name' => 'cname',
            'label' => '公司名稱'
        ]);

        $this->crud->addField([
            'name' => 'ename',
            'label' => '公司名稱(英)'
        ]);

        $this->crud->addField([
            'name' => 'email',
            'label' => 'E-mail'
        ]);

        $this->crud->addField([
            'name' => 'phone',
            'label' => '電話'
        ]);

        $this->crud->addField([
            'name' => 'fax',
            'label' => '傳真'
        ]);

        $this->crud->addField([
            'name' => 'address',
            'label' => '地址'
        ]);

        $this->crud->addField([   // Upload
            'name' => 'logo',
            'label' => 'Logo',
            'type' => 'upload',
            'upload' => true,
            'disk' => 'public' // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
        ]);

        $this->crud->addField([
            'name' => 'descp',
            'label' => '關於我們',
            'type' => 'textarea'
        ]);

        $this->crud->addField([
            'name' => 'icon1',
            'label' => '圖示1'
        ]);
        $this->crud->addField([
            'name' => 'title1',
            'label' => '標題1'
        ]);
        $this->crud->addField([
            'name' => 'descp1',
            'label' => '描述1'
        ]);

        $this->crud->addField([
            'name' => 'icon2',
            'label' => '圖示2'
        ]);
        $this->crud->addField([
            'name' => 'title2',
            'label' => '標題2'
        ]);
        $this->crud->addField([
            'name' => 'descp2',
            'label' => '描述2'
        ]);

        $this->crud->addField([
            'name' => 'icon3',
            'label' => '圖示3'
        ]);
        $this->crud->addField([
            'name' => 'title3',
            'label' => '標題3'
        ]);
        $this->crud->addField([
            'name' => 'descp3',
            'label' => '描述3'
        ]);

        $this->crud->addField([
            'name' => 'icon4',
            'label' => '圖示4'
        ]);
        $this->crud->addField([
            'name' => 'title4',
            'label' => '標題4'
        ]);
        $this->crud->addField([
            'name' => 'descp4',
            'label' => '描述4'
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'created_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'updated_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
    }

}