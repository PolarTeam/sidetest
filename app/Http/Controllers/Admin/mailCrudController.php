<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\mailCrudRequest as StoreRequest;
use App\Http\Requests\mailCrudRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\mail;
use App\Models\CommonModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use File;
use Response;
class mailCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\mail');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/mail');
        $this->crud->setEntityNameStrings('mail', 'mail');


        $this->crud->setColumns(['name']);
        $this->crud->enableAjaxTable();
        $this->crud->setCreateView('mail.edit');
        $this->crud->setEditView('mail.edit');
        $this->crud->setListView('mail.index');

        // $this->crud->addField([
        //     'name' => 'title',
        //     'label' => '標題'
        // ]);

        $this->crud->addField([
            'name' => 'name',
            'type' => 'textarea'
        ]);

        $this->crud->addField([
            'name' => 'phone',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'type',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'cust_email',
            'type' => 'textarea'
        ]);
        
        $this->crud->addField([
            'name' => 'message',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'created_at',
            'type' => 'date_picker',
            // optional:
            'date_picker_options' => [
                'format' => 'YYYY-MM-DD',
                //'language' => 'en'
            ]
        ]);


    }
    public function index() {
        $user = Auth::user();

        return view($this->crud->getListView());
    }

	public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $type = $request->cust_type;    
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::storeCrud($request);

            $prodData = mail::find($this->data['entry']->getKey());
            $data = $request->all();
            for($i=1; $i<=5; $i++) {

                if(isset($data['img'.$i])) {
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads/', $img);
                    $prodData['img'.$i] = $path;
                    $prodData->save();
                }
                
            }
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
    }
    public function create()
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['prodType'] = request('prodType');
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
        $this->data['id'] = $id;


        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }





    public function multiDel() {
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $prodMgmtModel = mail::find($ids[$i]);
                DB::table('mod_message')->where('id', $prodMgmtModel->id)->delete();
                $prodMgmtModel->delete();
                
            }
        }

        return response()->json(array('msg' => 'success'));
    }

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        $oldData = mail::find($request->id);
        $prodData = array();
        unset($request['created_by']);

        try {
            $response = parent::updateCrud($request);
            $prodData = mail::find($request->id);
            $data = $request->all();        
        }
        catch (\Exception $e) {            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }

        
        return ["msg"=>"success", "response"=>$response, "data" => $prodData];
    }

}