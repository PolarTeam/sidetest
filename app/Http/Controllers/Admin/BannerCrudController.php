<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BannerCrudRequest as StoreRequest;
use App\Http\Requests\BannerCrudRequest as UpdateRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;

class BannerCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\Banner');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/modBanner');
        $this->crud->setEntityNameStrings('頁首', '頁首');
        $this->crud->orderBy('updated_at','desc');
        $this->crud->setColumns([
            [
                'name' => 'place',
                'label' => "名稱",
                'type'  => 'text'
            ],
            [
                'name' => 'title',
                'label' => "標題",
                'type'  => 'text'
            ],
            // [
            //     'name' => 'cate_id',
            //     'label' => "類別",
            //     'type'  => 'text'
            // ],
            [
                'name' => 'created_by',
                'label' => "建立者",
                'type'  => 'text'
            ],
            [
                'name' => 'created_at',
                'label' => "時間",
                'type'  => 'text'
            ]
        ]);

        $this->crud->addField([
            'name' => 'title',
            'label' => '標題',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4',
            ],
        ]);

        $this->crud->addField([
            'name' => 'cate_id',
            'label' => '排序',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4',
            ],
        ]);


        $this->crud->addField([  // Select
            'label' => "名稱",
            'name' => 'place', // the db column for the foreign key
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-8',
            ],
        ]);
        $this->crud->addField([
            'label' => '副標題',
            'name' => 'sub_title',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-8',
            ],
        ]);
        $this->crud->addField([  // Select
            'label' => "副標題2",
            'name' => 'position', // the db column for the foreign key
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-8',
            ],
        ]);

        $this->crud->addField([
            'name' => 'btn_text',
            'label' => '底下文字1',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-8',
            ],
        ]);

        $this->crud->addField([
            'name' => 'link',
            'label' => '底下文字2',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-8',
            ],
        ]);

        $this->crud->addField([   // Upload
            'name' => 'image',
            'label' => '圖片上傳',
            'type' => 'upload',
            'upload' => true,
            'disk' => 'public' // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
        ]);

        // $this->crud->addField([   // URL
        //     'name' => 'video',
        //     'label' => '影片url',
        //     'type' => 'url'
        // ]);

        // $this->crud->addField([       // Select2Multiple = n-n relationship (with pivot table)
        //     'label'     => "廣告商品",
        //     'type'      => 'select2_from_array',
        //     'name'      => 'prod_id', // the method that defines the relationship in your Model
        //     'options'     => DB::table('mod_product')->pluck('title', 'id'), // foreign key model
        //     'allows_multiple' => true,
        //     // 'select_all' => true, // show Select All and Clear buttons?
        // ]);

        $this->crud->addField([   // Hidden
            'name' => 'created_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        $this->crud->addField([   // Hidden
            'name' => 'updated_by',
            'type' => 'hidden',
            'value' => Auth::user()->name
        ]);

        
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
    }

}