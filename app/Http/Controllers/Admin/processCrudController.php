<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\processCrudRequest as StoreRequest;
use App\Http\Requests\processCrudRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\CommonModel;
use App\Models\process;
use App\Models\processDetailModel;
use App\Models\processBannerModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use File;
use Response;
class processCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\process');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/process');
        $this->crud->setEntityNameStrings('process', 'process');


        $this->crud->setColumns(['name']);
        $this->crud->enableAjaxTable();
        $this->crud->setCreateView('process.edit');
        $this->crud->setEditView('process.edit');
        $this->crud->setListView('process.index');

        // $this->crud->addField([
        //     'name' => 'title',
        //     'label' => '標題'
        // ]);

        $this->crud->addField([
            'name' => 'title',
            'type' => 'textarea'
        ]);

        $this->crud->addField([
            'name' => 'cate_id',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'addr',
            'type' => 'lookup',
            'title' => '位置',
            'info1' => Crypt::encrypt('sys_area'), //table
            'info2' => Crypt::encrypt("addr_name"), //column
            'info3' => Crypt::encrypt(""), //condition
            'info4' => "addr=addr_name;" //field mapping
        ]);

        $this->crud->addField([
            'name' => 'remark',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'planning',
            'type' => 'textarea'
        ]);
        $this->crud->addField([
            'name' => 'structure',
            'type' => 'select2_from_array',
            'options' => DB::table('bscode')->select('cd as code', 'cd as descp')->where('cd_type', 'BUILDTYPE')->get()
        ]);

        $this->crud->addField([
            'name' => 'years',
            'type' => 'text'
        ]);



    }
    public function index() {
        $user = Auth::user();

        return view($this->crud->getListView());
    }

	public function store(StoreRequest $request)
	{
        $user = Auth::user();
        $type = $request->cust_type;    
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        try {
            $response = parent::storeCrud($request);

            $prodData = process::find($this->data['entry']->getKey());
            $data = $request->all();
            for($i=1; $i<=5; $i++) {

                if(isset($data['img'.$i])) {
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads/', $img);
                    $prodData['img'.$i] = $path;
                    $prodData->save();
                }
                
            }
        }
        catch (\Exception $e) {
            
            \Log::error($e);
            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }
        
        return ["msg"=>"success", "response"=>$response, "lastId"=>$this->data['entry']->getKey()];
    }
    public function create()
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['prodType'] = request('prodType');
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['entry'] = str_replace("'", "",json_encode($this->data['entry']));

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
        $this->data['id'] = $id;


        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function detailValidator($request)
    {
        $validator = Validator::make($request->all(), []);

        return $validator;
    }

    public function imgget($id=null) {

        $prodDetail = [];
        if($id != 0) {
            $this_query = DB::table('mod_processbanner');
            $this_query->where('process_id', $id);
            $prodDetail = $this_query->get();
        }
        
        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }

    public function imgStore(Request $request)
    {
        $validator = $this->detailValidator($request);        
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $user = Auth::user();
            $request->merge(array('created_by' => $user->email));
            $request->merge(array('updated_by' => $user->email));
            
            $prodDetail = new processBannerModel;
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();

            $prodDetailData = processBannerModel::find($prodDetail->id);
            $data = $request->all();
            for($i=1; $i<=1; $i++) {

                if(isset($data['img'.$i])) {
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads/', $img);
                    $prodDetailData['img'.$i] = $path;
                    $prodDetailData->save();
                }
                
            }
        }

        return ["msg"=>"success", "data"=>$prodDetail->where('id', $prodDetailData->id)->get()];
    }

    public function imgUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->detailValidator($request);
        $now = date('YmdHis');
        $prodDetail_INFO = processBannerModel::find($request->id);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $prodDetail = processBannerModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();

            $prodDetailData = processBannerModel::find($prodDetail->id);
            $data = $request->all();
            for($i=1; $i<=1; $i++) {

                if(isset($data['img'.$i])) {
                    $oldPath = $prodDetail['img'.$i];
                    
                    Storage::delete($oldPath);
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads', $img);
                    $prodDetailData['img'.$i] = $path;
                    $prodDetailData->save();
                }
            }
        }


        return ["msg"=>"success", "data"=>$prodDetail->where('id', $request->id)->get()];
    }
    public function imgDel($id)
    {
        $prodDetail = processBannerModel::find($id);
        Storage::delete($prodDetail->img1);
        $prodDetail->delete();

        return ["msg"=>"success"];
    }

    public function get($id=null) {

        $prodDetail = [];
        if($id != 0) {
            $this_query = DB::table('mod_process_detail');
            $this_query->where('process_id', $id);
            $prodDetail = $this_query->get();
        }
        
        $data[] = array(
            'Rows' => $prodDetail,
        );

        return response()->json($data);
    }

    public function detailStore(Request $request)
    {
        $validator = $this->detailValidator($request);        
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $user = Auth::user();
            $request->merge(array('created_by' => $user->email));
            $request->merge(array('updated_by' => $user->email));
            
            $prodDetail = new processDetailModel;
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();

            $prodDetailData = processDetailModel::find($prodDetail->id);
            $data = $request->all();
            for($i=1; $i<=1; $i++) {

                if(isset($data['img'.$i])) {
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads/', $img);
                    $prodDetailData['img'.$i] = $path;
                    $prodDetailData->save();
                }
                
            }
        }

        return ["msg"=>"success", "data"=>$prodDetail->where('id', $prodDetailData->id)->get()];
    }

    public function detailUpdate(Request $request)
    {
        $user = Auth::user();
        $validator = $this->detailValidator($request);
        $now = date('YmdHis');
        $prodDetail_INFO = processDetailModel::find($request->id);
        if ($validator->fails()) {
            return ["msg"=>"error", "errorLog"=>$validator->messages()];
        }
        else {
            $prodDetail = processDetailModel::find($request->id);
            foreach($request->all() as $key=>$val) {
                $prodDetail[$key] = request($key);
            }
            $prodDetail->save();

            $prodDetailData = processDetailModel::find($prodDetail->id);
            $data = $request->all();
            for($i=1; $i<=1; $i++) {

                if(isset($data['img'.$i])) {
                    $oldPath = $prodDetail['img'.$i];
                    
                    Storage::delete($oldPath);
                    $img = $data['img'.$i];
                    $path = Storage::putFile('public/uploads', $img);
                    $prodDetailData['img'.$i] = $path;
                    $prodDetailData->save();
                }
            }
        }


        return ["msg"=>"success", "data"=>$prodDetail->where('id', $request->id)->get()];
    }
    public function detailDel($id)
    {
        $prodDetail = processDetailModel::find($id);
        Storage::delete($prodDetail->img1);
        $prodDetail->delete();

        return ["msg"=>"success"];
    }

    public function multiDel() {
        $ids = request('ids');

        if(count($ids) > 0) {
            for($i=0; $i<count($ids); $i++) {
                $prodMgmtModel = process::find($ids[$i]);
                DB::table('mod_process')->where('id', $prodMgmtModel->id)->delete();
                $prodMgmtModel->delete();
                
            }
        }

        return response()->json(array('msg' => 'success'));
    }

	public function update(UpdateRequest $request)
	{
        $user = Auth::user();
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        $oldData = process::find($request->id);
        $prodData = array();
        unset($request['created_by']);

        try {
            $response = parent::updateCrud($request);
            $prodData = process::find($request->id);
            $data = $request->all();
            if(isset($data['img1'])) {
                $oldPath = $oldData['img1'];
                
                Storage::delete($oldPath);
                $img = $data['img1'];
                $path = Storage::putFile('images', $img);
                $prodData['img1'] = $path;
                $prodData->save();
            }         
        }
        catch (\Exception $e) {            
            return ["msg"=>"error", "errorLog"=>$e->getMessage()];
        }

        
        return ["msg"=>"success", "response"=>$response, "data" => $prodData];
    }

}