<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\MailFormatCrudRequest as StoreRequest;
use App\Http\Requests\MailFormatCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\CommonModel;
class MailFormatCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel("App\Models\MailFormatModel");        
        $this->crud->setRoute(config('backpack.base.route_prefix').'/mailFormat');
        $this->crud->setEntityNameStrings(trans('mailFormat.titleName'), trans('mailFormat.titleName'));
        $bscode = DB::table('bscode')->where('cd_type','=','MF')->pluck('cd_descp', 'cd');
        //dd($bscode);
        $this->crud->setColumns([
            [
                'name' => 'title',
                'label' => trans('mailFormat.title'),
                'type'  => 'text'
            ],                
            // [  // Select
            //     'label' => trans('mailFormat.type'),
            //     'type' => 'select_from_array',
            //     'name' => 'type', 
            //     'options' => $bscode,
            //     'allows_null' => false                
            // ]           
            ]);
        //$this->crud->addColumns(['address']);
        $this->crud->addFields([
            [
                'name' => 'title',
                'label' => trans('mailFormat.title'),
                'type'  => 'text'
            ],                
            // [  // Select
            //     'label' => trans('mailFormat.type'),
            //     'type' => 'select_from_array',
            //     'name' => 'type', 
            //     'options' => $bscode,
            //     'allows_null' => false                
            // ],           
            [ // Table
                'name' => 'content',
                'label' => trans('mailFormat.content'),
                'type' => 'summernote'
            ],
            // [ // Table
            //     'name' => 'g_key',
            //     'label' => 'g_key',
            //     'type' => 'hidden'
            // ],
            // [ // Table
            //     'name' => 'c_key',
            //     'label' => 'c_key',
            //     'type' => 'hidden'
            // ],
            // [ // Table
            //     'name' => 's_key',
            //     'label' => 's_key',
            //     'type' => 'hidden'
            // ],
            // [ // Table
            //     'name' => 'd_key',
            //     'label' => 'd_key',
            //     'type' => 'hidden'
            // ],
            [ // Table`
                'name' => 'created_by',
                'label' => 'created_by',
                'type' => 'hidden'
            ],
            [ // Table
                'name' => 'updated_by',
                'label' => 'updated_by',
                'type' => 'hidden'
            ]
        ]);
    }

	public function store(StoreRequest $request)
	{
        //dd($request->all());
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);
        
        // file_put_contents(resource_path('views/emails/'.$request->type.'.blade.php'), $request->content);

		return parent::storeCrud($request);
	}

	public function update(UpdateRequest $request)
	{
        //dd($request->all());
        $commonFunc = new CommonModel;
        $request = $commonFunc->processData($request, $this->crud->create_fields);

        file_put_contents(resource_path('views/emails/'.$request->type.'.blade.php'), $request->content);

		return parent::updateCrud($request);
    }
}