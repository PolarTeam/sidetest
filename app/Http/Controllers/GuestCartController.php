<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Redirect;

use App\Models\MemberMgmtModel;
use App\Models\OrderMgmtModel;
use App\Models\BaseModel;
use App\Models\ProdDetailModel;
use App\Models\Discount;
use App\Models\ProdMgmtModel;
use App\Models\BscodeModel;

use Recca0120\Mitake\Client;

class GuestCartController extends Controller
{
    public function chkAuth() {
        $isLogin = false;
        $cartContent = null;
        $cartId = isset($_COOKIE['cartId'])?$_COOKIE['cartId']:null;
        if(Auth::guard('member')->check()) {
            $isLogin = true;
            $user = Auth::guard('member')->user();

            $cartData = DB::table('mod_cart')
                        ->where('user_id', $user->id)
                        ->first();

            if(isset($cartData)) {
                $cartContent = $cartData->content;
            }
        }
        else {
            $isLogin = true;

            $cartData = DB::table('mod_cart')
                        ->where('id', $cartId)
                        ->first();

            if(isset($cartData)) {
                $cartContent = $cartData->content;
            }
        }

        return response()->json(['status' => $isLogin, 'cartContent' => $cartContent]);
    }

    public function addCart(Request $request) {
        $user = null;
        if(Auth::guard('member')->check()) {
            $user = Auth::guard('member')->user();
        }

        $cartContent  = $request->cartContent;
        $prodDetailId = $request->prodDetailId;
        $prodid       = $request->prodid;
        $prodNo       = $request->prodNo;
        $num          = $request->num;
        $cnt          = 0;
        $userId       = null;
        $cartData     = null;
        $cartId       = isset($_COOKIE['cartId'])?$_COOKIE['cartId']:null;

        $prodMainData = DB::table('mod_product')->where('prod_no', $prodNo)->first();
        $prodType       = (isset($prodMainData->prod_type))?$prodMainData->prod_type:"";

        if(isset($user)) {
            $cartData = DB::table('mod_cart')->where('user_id', $user->id)->first();
        }
        else {
            $cartData = DB::table('mod_cart')->where('id', $cartId)->first();
        }

        if(isset($cartData)) {
            $cnt = 1;
            $cartId = $cartData->id;

            $obj = json_decode($cartData->content);

            if(isset($obj->sData)) {
                for($i=0; $i<count($obj->sData); $i++) {
                    if($prodDetailId == $obj->sData[$i]->prodDetailId)
                        $num += $obj->sData[$i]->num;
                }
            }

            $p = new ProdDetailModel();
            $hasStock = $p->hasStock($prodDetailId, $num, $prodType, $prodNo);

            if($hasStock === false) {
                return response()->json(['status' => 'error', 'message' => '存貨不足']);
            }
        }

        if($cnt > 0) {
            DB::table('mod_cart')
                ->where('id', $cartId)
                ->update([
                    'content'    => $cartContent,
                    'updated_at' => \Carbon::now()
                ]);
        }
        else {
            $cartId = DB::table('mod_cart')
                        ->insertGetId([
                            'user_id'    => (isset($user))?$user->id:null,
                            'content'    => $cartContent,
                            'created_at' => \Carbon::now(),
                            'updated_at' => \Carbon::now()
                        ]);
            
            if(!isset($user)) {
                setcookie('cartId', $cartId, time()+60*60*24, '/');
            }
            
        }

        return response()->json(['status' => 'success']);
    }

    public function updateInsuranceToCart(Request $request) {
        $user = Auth::guard('member')->user();
        $obj = null;
        try{
            $uuid = $request->uuid;
            $insurance = $request->insurance;

            $cartData = DB::table('mod_cart')
                        ->where('user_id', $user->id)
                        ->first();
            if(isset($cartData)) {
                $cartContent  = $cartData->content;
                $obj = json_decode($cartContent);
                for($i=0; $i<count($obj->rData); $i++) {
                    $id = $obj->rData[$i]->uuid;

                    if($id == $uuid) {
                        $obj->rData[$i]->insurance = $insurance;
                    }
                }

                $cartContent = json_encode($obj);

                DB::table('mod_cart')
                        ->where('user_id', $user->id)
                        ->update([
                            'content'    => $cartContent,
                            'updated_at' => \Carbon::now()
                        ]);
            }
        }
        catch(\Exception $e) {
            return response()->json(['status' => 'error', 'message' => '系統發生問題，請恰客服人員', 'log' => $e->getMessage()]);
        }

        $cartData = DB::table('mod_cart')->where('user_id', $user->id)->first();
        $cartContent = null;
        
        if(isset($cartData)) {
            $cartContent = json_decode($cartData->content);
        }

        return response()->json(['status' => 'success', 'cartContent' => $cartContent]);
    }

    public function updateNumToCart(Request $request) {
        $user = Auth::guard('member')->user();
        $obj = null;
        $prodDetailModel = new ProdDetailModel();
        $cartId = isset($_COOKIE['cartId'])?$_COOKIE['cartId']:null;
        $cartData = null;
        try{
            if(isset($user)) {
                $cartData = DB::table('mod_cart')
                        ->where('user_id', $user->id)
                        ->first();
            }
            else {
                $cartData = DB::table('mod_cart')
                        ->where('id', $cartId)
                        ->first();
            }
            $uuid = $request->uuid;
            $num = $request->num;

            if(isset($cartData)) {
                $cartContent  = $cartData->content;
                $obj = json_decode($cartContent);
                for($i=0; $i<count($obj->rData); $i++) {
                    $id = $obj->rData[$i]->uuid;

                    if($id == $uuid) {
                        $hasStock = $prodDetailModel->hasStock($obj->rData[$i]->prodDetailId, $num, $obj->rData[$i]->prodType, $obj->rData[$i]->prodNo);
                        if($hasStock === true) {
                            $obj->rData[$i]->num = $num;
                        }
                        else {
                            return response()->json(['status' => 'error', 'message' => '庫存不足']);
                        } 
                    }
                }
                
                for($i=0; $i<count($obj->sData); $i++) {
                    $id = $obj->sData[$i]->uuid;

                    if($id == $uuid) {
                        $hasStock = $prodDetailModel->hasStock($obj->sData[$i]->prodDetailId, $num, $obj->sData[$i]->prodType, $obj->sData[$i]->prodNo);
                        if($hasStock === true) {
                            $obj->sData[$i]->num = $num;
                        }
                        else {
                            return response()->json(['status' => 'error', 'message' => '庫存不足']);
                        }
                            
                    }
                }

                $cartContent = json_encode($obj);

                if(isset($user)) {
                    DB::table('mod_cart')
                        ->where('user_id', $user->id)
                        ->update([
                            'content'    => $cartContent,
                            'updated_at' => \Carbon::now()
                        ]);
                }
                else {
                    DB::table('mod_cart')
                        ->where('id', $cartId)
                        ->update([
                            'content'    => $cartContent,
                            'updated_at' => \Carbon::now()
                        ]);
                }
                
            }
        }
        catch(\Exception $e) {
            return response()->json(['status' => 'error', 'message' => '系統發生問題，請恰客服人員', 'log' => $e->getMessage()]);
        }

        $cartContent = null;
        if(isset($user)) {
            $cartData = DB::table('mod_cart')->where('user_id', $user->id)->first();
        }
        else {
            $cartData = DB::table('mod_cart')->where('id', $cartId)->first();
        }
        
        
        if(isset($cartData)) {
            $cartContent = json_decode($cartData->content);
        }

        return response()->json(['status' => 'success', 'cartContent' => $cartContent]);
    }

    public function delCart(Request $request) {
        $user = Auth::guard('member')->user();
        $uuid = $request->uuid;
        $sellType = $request->sellType;
        $cartNum = 0;
        $cartContent = null;
        $cartData = null;
        $cartId = isset($_COOKIE['cartId'])?$_COOKIE['cartId']:null;
        try {
            if(isset($user)) {
                $cartData = DB::table('mod_cart')
                        ->where('user_id', $user->id)
                        ->first();
            }
            else {
                $cartData = DB::table('mod_cart')
                        ->where('id', $cartId)
                        ->first();
            }
            
            if(isset($cartData)) {
                $cartContent = json_decode($cartData->content);
                if($sellType == "R") {
                    for($i=0; $i<count($cartContent->rData); $i++) {
                        $id = $cartContent->rData[$i]->uuid;

                        if($id == $uuid) {
                            array_splice($cartContent->rData, $i, 1);
                        }
                    }
                }

                if($sellType == "S") {
                    for($i=0; $i<count($cartContent->sData); $i++) {
                        $id = $cartContent->sData[$i]->uuid;

                        if($id == $uuid) {
                            array_splice($cartContent->sData, $i, 1);
                        }
                    }
                }

                $cartEncode = json_encode($cartContent);
                $cartNum = count($cartContent->rData) + count($cartContent->sData);

                if($cartNum == 0) {
                    DB::table('mod_cart')
                        ->where('id', $cartData->id)
                        ->delete();
                }

                setcookie('cartNum', $cartNum, time()+60*60*24, '/');

                DB::table('mod_cart')
                        ->where('id', $cartData->id)
                        ->update([
                            'content'    => $cartEncode,
                            'updated_at' => \Carbon::now()
                        ]);
            }
            
        }
        catch(\Exception $e) {
            return response()->json(['status' => 'error', 'message' => '系統發生問題，請恰客服人員', 'log' => $e->getMessage()]);
        }

        return response()->json(['status' => 'success', 'cartNum' => $cartNum, 'cartContent' => $cartContent]);
    }

    public function searchView(Request $request) {
        $viewData = [
            'dlv_nm'          => "",
            'dlv_phone'       => "",
        ];
        return view('FrontEnd.search')->with($viewData);
    }
    public function searchlistView(Request $request) {
        $ids = 0 ;
        $dlv_nm = $request->dlv_nm;
        $dlv_phone = $request->dlv_phone;;
        $ord_no = array();
        $ordData = DB::table('mod_order')
        ->where('dlv_nm', $dlv_nm)
        ->where('dlv_phone', $dlv_phone)
        ->whereIn('status', ['B', 'C', 'I'])
        ->get();;
        for($i=0;$i < sizeof($ordData) ;$i++){
            $ord_no[$i]=$ordData[$i]->ord_no;
        }
        // dd($ord_no);
        $snData = DB::table('sys_sn')
        ->whereIn('ord_no',$ord_no)
        ->get();
        // $snData = DB::table('sys_sn')
        //             ->where('ord_nm',$dlv_nm)
        //             ->where('ord_phone',$dlv_phone)
        //             ->get();
        // $remarkdata  = array();
        // foreach($orderData as $row){
        //     $remarkdata[$ids]["prod_nm"]=$row->normal_detail_descp;
        //     $remarkdata[$ids]["remark"]=$row->remark;
        //     $ids ++;
        // }
        return view('FrontEnd.search')->with([
            'dlv_nm'          => $dlv_nm,
            'snData' => $snData
        ]);;
    }
    public function cartsub(Request $request) {
        // $user = null;
        // $cartData = null;
        $now      = new \DateTime();
        $now =$now->format('Ymd');
        if((int)$now > 20190720){

            return redirect()->back()->withErrors(['message' => '日期已截止']);
        }
        if($request->type==""||$request->type==null)
        {
            $areaData = DB::table('mod_cate')->select('id', 'name')->where('parent_id', 6)->orderBy('id', 'asc')->get();
            $bannerProd = array();
    
            $today      = new \DateTime();
            $str_date    = $today->format('Y-m-d H:i:s');
            $flashingProdData = DB::table('mod_product')
                                ->whereNotNull('d_percent')
                                ->where('is_added', 'Y')
                                ->where('action_date', '>=', $str_date)
                                ->get();
            
    
            $focusProdData = DB::table('mod_product')
                                ->where('is_focus', 'Y')
                                ->where('is_added', 'Y')
                                ->orderBy('sort', 'asc')
                                ->orderBy('created_at', 'desc')
                                // ->where(function($query) use ($str_date){
                                //     $query->whereRaw("action_date is null or action_date >= "."'".$str_date."'");
                                // })
                                ->get();
            $snData = DB::table('sys_sn')->count();
            $viewData = array(
                'viewName'  => 'index',
                'areaData'  => $areaData,
                'flashingProdData' => $flashingProdData,
                'focusProdData' => $focusProdData,
                'snData' => $snData,
            );
            return view('FrontEnd.homePage')->with($viewData);
        }
        $type = $request->type;
        $shipfee = 0;
        if($type=="a"){
            $prod_nm = "開運招財貔貅手環(男款)及晶華養生會館200元抵用券";
            $num = $request->num_a;
        }else{
            $prod_nm = "開運招財貔貅手環(女款)及晶華養生會館200元抵用券";
            $num = $request->num_b;
        }
        if($num>1){
            $shipfee = 0;
        }
        else{
            $shipfee=80;
        }
        $sumamt = $num*499;
        $ttlamt = $sumamt+$shipfee;
        $viewData = [
            'prod_nm'    => $prod_nm,
            'type'       => $type,
            'num'        => $num,
            'shipfee'    => $shipfee,
            'sumamt'     => $sumamt,
            'ttlamt'     => $ttlamt
        ];
        return view('FrontEnd.newCart')->with($viewData);
    }
    public function cartView(Request $request) {
        // $user = null;
        // $cartData = null;

        // if(Auth::guard('member')->check()) {
        //     $user = Auth::guard('member')->user();
        //     $cartData = DB::table('mod_cart')
        //                 ->where('user_id', $user->id)
        //                 ->first();
        // }
        // else {
        //     $cartId = isset($_COOKIE['cartId'])?$_COOKIE['cartId']:null;
        //     $cartData = DB::table('mod_cart')
        //                 ->where('id', $cartId)
        //                 ->first();
        //     $user = new MemberMgmtModel();
        // }

        
        // $cartContent = json_encode(array('rData'=>array(), 'sData' => array()));
        // if(isset($cartData)) {
        //     $cartContent = $cartData->content;
        // }
        // $cartData = json_decode($cartContent);

        // $rSumAmt = 0;
        // $sSumAmt = 0;

        // $prodType = array();
        // $prodType['W'] = 0;
        // $prodType['S'] = 0;
        // $prodType['N'] = 0;


        // $canUseSellDiscount = true;

        // $ProdMgmtModel = new ProdMgmtModel();
        // $rentShipWay = array();
        // foreach($cartData->rData as $key=>$row) {
        //     $day = (((strtotime($row->endDate) - strtotime($row->startDate))/3600)/24) + 1;
        //     $amt = $row->dPrice * $day * $row->num;

        //     if($day >= 5) {
        //         $canUseSellDiscount = false;
        //     }

        //     if($row->insurance == "Y") {
        //         $amt = $amt + (50 * $day * $row->num);
        //     }

        //     $row->amt = $amt;

        //     $rSumAmt += $amt;

        //     $prodType['W']++;

        //     if($key == 0) {
        //         $rentShipWay = $ProdMgmtModel->getShipWayByProdNoToArray($row->prodNo);
        //     }
        // }

        
        // $shipWayArray = array();
        // $normalShipWay = array();
        // $periodsArray = array();
        // foreach($cartData->sData as $key=>$row) {
        //     $amt = $row->dPrice * $row->num;

        //     $row->amt = $amt;

        //     $sSumAmt += $amt;

        //     if($row->prodType == 'N') {
        //         $prodType['N']++;
        //     }

        //     if($row->prodType == 'S') {
        //         $prodType['S']++;
        //     }

        //     $normalShipWay = $ProdMgmtModel->getShipWayByProdNoToArray($row->prodNo);

        //     if(!empty($normalShipWay) && !empty($shipWayArray)) {
        //         $shipWayArray = array_intersect($shipWayArray, $normalShipWay);
        //     }
        //     else if(!empty($normalShipWay) && empty($shipWayArray)) {
        //         $shipWayArray = $normalShipWay;
        //     }

        //     $tmpPeriods = $ProdMgmtModel->getPeriodsToArray($row->prodNo);

        //     $periodsArray = array_merge($periodsArray, $tmpPeriods);
        // }

        // $periodsArray = $ProdMgmtModel->processPeriodsArray($periodsArray);
        
        // if($prodType['W'] > 0) {
        //     if(!empty($shipWayArray)) {
        //         $shipWayArray = array_intersect($shipWayArray, $rentShipWay);
        //     }
        //     else {
        //         $shipWayArray = $rentShipWay;
        //     }
        // }

        // $bscodeModel = new BscodeModel();
        // $shipWayArray = $bscodeModel->getShipWayKeyValueArray($shipWayArray);

        // $defaultShip = 1;
        // //如果購物車只有軟體時，預設不帶宅配
        // if($prodType['S'] > 0 && $prodType['N'] == 0 && $prodType['W'] == 0) {
        //     $defaultShip = 0;
        // }


        // $showPickAir = 1;
        // if($prodType['N'] > 0) {
        //     $showPickAir = 0;
        // }

        // $cityData = DB::table('sys_area')->select('city_nm')->groupBy('city_nm')->orderBy('id', 'asc')->get();

        // $shipFree = $bscodeModel->getShipFree();
        // $shipFee = 0;
        // if(!empty($shipWayArray)) {
        //     $shipFee = $bscodeModel->getShipFee($shipWayArray[0]);
        // }
        
        // $ttlAmt = $sSumAmt;
        // if($sSumAmt < $shipFree) {
        //     $ttlAmt = $shipFee +  $sSumAmt;
        // }

        // $periodsArray = $ProdMgmtModel->updatePeriodsArrayLabel($periodsArray, $ttlAmt);

        // $shipFeeArray = $bscodeModel->getShipFeeToArray();

        $viewData = [
            // 'viewName'    => 'cart',
            // 'cartData'    => $cartData,
            // 'rSumAmt'     => $rSumAmt,
            // 'sSumAmt'     => $sSumAmt,
            // 'defaultShip' => $defaultShip,
            // 'cartStr'     => json_encode($cartData),
            // 'airlineData' => DB::table('bscode')->where('cd_type', 'AIRLINE')->orderBy('id', 'asc')->get(),
            // 'cityData'    => $cityData,
            // 'canUseSellDiscount' => $canUseSellDiscount,
            // 'memberData' => $user,
            // 'showPickAir' => $showPickAir,
            // 'shipWay'     => $shipWayArray,
            // 'shipFree'    => $shipFree,
            // 'periodsArray' => $periodsArray,
            // 'shipFeeArray' => json_encode($shipFeeArray)
        ];

        return view('FrontEnd.newCart')->with($viewData);
    }

    public function getShipWayFromCart() {
        $user = Auth::guard('member')->user();

        $cartData = DB::table('mod_cart')
                        ->where('user_id', $user->id)
                        ->first();

        $cartContent = json_encode(array('rData'=>array(), 'sData' => array()));
        if(isset($cartData)) {
            $cartContent = $cartData->content;
        }

        $cartData = json_decode($cartContent);

        $ProdMgmtModel = new ProdMgmtModel();
        $rentShipWay = array();
        foreach($cartData->rData as $key=>$row) {
            if($key == 0) {
                $rentShipWay = $ProdMgmtModel->getShipWayByProdNoToArray($row->prodNo);
            }
        }

        
        $shipWayArray = array();
        $normalShipWay = array();
        foreach($cartData->sData as $key=>$row) {
            $normalShipWay = $ProdMgmtModel->getShipWayByProdNoToArray($row->prodNo);

            if(!empty($normalShipWay) && !empty($shipWayArray)) {
                $shipWayArray = array_intersect($shipWayArray, $normalShipWay);
            }
            else if(!empty($normalShipWay) && empty($shipWayArray)) {
                $shipWayArray = $normalShipWay;
            }
        }
        
        if(count($cartData->rData) > 0) {
            if(!empty($shipWayArray)) {
                $shipWayArray = array_intersect($shipWayArray, $rentShipWay);
            }
            else {
                $shipWayArray = $rentShipWay;
            }
        }

        $bscodeModel = new BscodeModel();
        $shipWayArray = $bscodeModel->getShipWayKeyValueArray($shipWayArray);

        return response()->json(['shipWay' => $shipWayArray]);
    }

    public function getCart() {
        $user = Auth::guard('member')->user();
        $cartData = null;
        $cartId = isset($_COOKIE['cartId'])?$_COOKIE['cartId']:null;

        if($user) {
            $cartData = DB::table('mod_cart')->where('user_id', $user->id)->first();
        }
        else {
            $cartData = DB::table('mod_cart')->where('id', $cartId)->first();
        }

       
        $obj = null;

        if(isset($cartData)) {
            $obj = json_decode($cartData->content);
        }

        return response()->json($obj);
    }

    public function updateInstalmentList(Request $request) {

        $user = null;
        $cartData = null;
        $ttlAmt = $request->ttlAmt;
        $ProdMgmtModel = new ProdMgmtModel();
        if(Auth::guard('member')->check()) {
            $user = Auth::guard('member')->user();
            $cartData = DB::table('mod_cart')
                        ->where('user_id', $user->id)
                        ->first();
        }
        else {
            $cartId = isset($_COOKIE['cartId'])?$_COOKIE['cartId']:null;
            $cartData = DB::table('mod_cart')
                        ->where('id', $cartId)
                        ->first();
        }

        $cartContent = json_encode(array('rData'=>array(), 'sData' => array()));
        if(isset($cartData)) {
            $cartContent = $cartData->content;
        }
        $cartData = json_decode($cartContent);
    
        $periodsArray = array();
        foreach($cartData->sData as $key=>$row) {
            $tmpPeriods = $ProdMgmtModel->getPeriodsToArray($row->prodNo);
            
            $periodsArray = array_merge($periodsArray, $tmpPeriods);
        }

        $periodsArray = $ProdMgmtModel->processPeriodsArray($periodsArray);
        $periodsArray = $ProdMgmtModel->updatePeriodsArrayLabel($periodsArray, $ttlAmt);

        return response()->json(['data' => $periodsArray]);

    }
}
