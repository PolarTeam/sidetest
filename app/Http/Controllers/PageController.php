<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;
use View;

class PageController extends Controller
{
    /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
    // Show a page by slug
    public function show($slug = '')
    {
        $page = Page::whereSlug($slug)->first();
        if(!isset($page)) {
            return abort(404);
        }
        return View::make('pages.index')->with('page', $page);
    }

    public function contact() {
        $cmpData = DB::table('mod_cmp')->where('id', 1)->first();
        return view('contact')->with('cmpData', $cmpData);
    }

    public function postMsg(Request $request) {
        $name    = $request->name;
        $email   = $request->email;
        $subject = $request->subject;
        $message = $request->message;
        try {
            DB::table('mod_contact')->insert([
                'name'    => $name,
                'email'   => $email,
                'subject' => $subject,
                'message' => $message
            ]);
        }
        catch(\Exception $e) { 
            return response()->json(['msg' => 'error']);
        }
        

        return response()->json(['msg' => 'success']);
    }
}