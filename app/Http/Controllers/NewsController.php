<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Redirect;


use App\Models\ModQaModel;


class NewsController extends Controller
{
    public function index(Request $request) {

        $cateId = $request->cateId;
        $cateData = DB::table('mod_cate')->where('parent_id', 39)->get();
        $newsData = array();

        if(isset($cateId)) {
            $newsData   = DB::table('mod_news')->where('cate_id', $cateId)->orderBy('created_at', 'desc')->paginate(10);
        }
        else {
            $newsData   = DB::table('mod_news')->orderBy('created_at', 'desc')->paginate(10);;
        }
        
        

        $viewData = array(
            'viewName' => 'news',
            'cateData' => $cateData,
            'newsData'   => $newsData,
            'cateId' => $cateId
        );

        return view('FrontEnd.news')->with($viewData);
    }

    public function newsDetail($id) {

        $cateData = DB::table('mod_cate')->where('parent_id', 39)->get();
        $newsData = DB::table('mod_news')->where('id', $id)->first();

        $viewData = array(
            'viewName' => 'news',
            'cateData' => $cateData,
            'newsData'   => $newsData
        );

        return view('FrontEnd.newsDetail')->with($viewData);
    }
}
