<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CateMgmtModel;
use DB;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = Category::where('name', 'LIKE', '%'.$search_term.'%')->paginate(10);
        }
        else
        {
            $results = Category::paginate(10);
        }

        return $results;
    }

    public function show($id)
    {
        $cateData = DB::table('mod_cate')->where('id', $id)->first();
        
        if(isset($cateData)) {
            return CateMgmtModel::where('parent_id', $cateData->parent_id)->get();
        }
        
        return array();
    }
}