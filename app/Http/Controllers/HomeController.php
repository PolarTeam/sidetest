<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


use Allpay;
use App\Models\CommonModel;
use App\Models\OrderMgmtModel;
use App\Models\Discount;
use App\Models\ModSnModel;

use Crypt;
use Shorty;
use MPG;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth:member');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newsData = DB::select('select * from mod_news order by created_at desc limit 0, 4');
        $bannerData = DB::select('select * from mod_banner order by created_at');
        $headerData = DB::select('select id, name, slug from pages order by id asc');
        $cmpData = DB::table('mod_cmp')->where('id', 1)->first();
        
        return view('welcome', ['newsData' => $newsData, 'bannerData' => $bannerData, 'cmpData' => $cmpData]);
    }

    public function index2()
    {
        $newsData = DB::select('select * from mod_news order by created_at desc limit 0, 4');
        $bannerData = DB::select('select * from mod_banner order by created_at');
        $headerData = DB::select('select id, name, slug from pages order by id asc');
        $cmpData = DB::table('mod_cmp')->where('id', 1)->first();
        
        return view('welcome2', ['newsData' => $newsData, 'bannerData' => $bannerData, 'cmpData' => $cmpData]);
    }

    public function news($id)
    {
        $newsData = DB::select('select * from mod_news where id='.$id);
        
        return view('news', ['newsData' => $newsData]);
    }

    public function projects()
    {
        $newsData = DB::select('select * from mod_news order by created_at desc limit 0, 4');
        $bannerData = DB::select('select * from mod_banner order by created_at');
        $headerData = DB::select('select id, name, slug from pages order by id asc');
        $cmpData = DB::table('mod_cmp')->where('id', 1)->first();
        
        return view('projects');
    }
    public function about()
    {
        $newsData = DB::select('select * from mod_news order by created_at desc limit 0, 4');
        $bannerData = DB::select('select * from mod_banner order by created_at');
        $headerData = DB::select('select id, name, slug from pages order by id asc');
        $cmpData = DB::table('mod_cmp')->where('id', 1)->first();
        
        return view('about', ['newsData' => $newsData, 'bannerData' => $bannerData, 'cmpData' => $cmpData]);
    }
    public function promotion()
    {
        $newsData = DB::select('select * from mod_news order by created_at desc limit 0, 4');
        $bannerData = DB::select('select * from mod_banner order by created_at');
        $headerData = DB::select('select id, name, slug from pages order by id asc');
        $cmpData = DB::table('mod_cmp')->where('id', 1)->first();
        
        return view('promotion', ['newsData' => $newsData, 'bannerData' => $bannerData, 'cmpData' => $cmpData]);
    }
    public function process()
    {
        $newsData = DB::select('select * from mod_news order by created_at desc limit 0, 4');
        $bannerData = DB::select('select * from mod_banner order by created_at');
        $headerData = DB::select('select id, name, slug from pages order by id asc');
        $cmpData = DB::table('mod_cmp')->where('id', 1)->first();
        
        return view('process', ['newsData' => $newsData, 'bannerData' => $bannerData, 'cmpData' => $cmpData]);
    }
    public function contact()
    {
        $newsData = DB::select('select * from mod_news order by created_at desc limit 0, 4');
        $bannerData = DB::select('select * from mod_banner order by created_at');
        $headerData = DB::select('select id, name, slug from pages order by id asc');
        $cmpData = DB::table('mod_cmp')->where('id', 1)->first();
        
        return view('contact', ['newsData' => $newsData, 'bannerData' => $bannerData, 'cmpData' => $cmpData]);
    }

    public function newsList()
    {
        $newsData = DB::table('mod_news')->orderBy('created_at', 'desc')->paginate(3);
        
        return view('newsList', ['newsData' => $newsData]);
    }
}