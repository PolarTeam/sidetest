<?php
namespace App\Http\Controllers;
use App\Mail\messagemail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use MPG;
use Redirect;
class WelcomeController extends Controller
{
    /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
    public function index()
    {
        return view('welcome');
    }

    public function index2()
    {

        return view('welcomenew');
    }


    public function menu()
    {
        $dataArray = DB::table('mod_team')
        ->select('id', 'img1','title', 'tags','type')
        ->orderBy('sorted', 'asc')
        ->get();

        foreach ($dataArray as $key => $row) {
            $data[] = array('id'=>$row->id,'url'=> 'https://phanes.com.tw:17890/uploads/'.$row->img1, 'title'=>$row->title, 'tags'=>explode(',', $row->tags), "type"=>$row->type  );
        }


        return response()->json(['msg' => 'success','code' => '00', 'data'=>$data]);
    }

    public function menudetail(Request $request)
    {
        $dataArray = DB::table('mod_menu_detail')
        ->select('id', 'img1','title', 'tags','intro','type')
        ->where('menu_id', $request->id)
        ->orderBy('rank', 'asc')
        ->get();

        foreach ($dataArray as $key => $row) {
            $data[] = array('id'=>$row->id,'url'=> 'https://phanes.com.tw:17890/uploads/'.$row->img1, 'title'=>$row->title, 'intro'=>$row->intro, 'tags'=>explode(',', $row->tags), "type"=>$row->type  );
        }


        return response()->json(['msg' => 'success','code' => '00', 'data'=>$data]);
    }

    public function product()
    {
        $dataArray = DB::table('mod_projects')
        ->select('id', 'img1','type')
        ->orderBy('sorted', 'asc')
        ->get();

        foreach ($dataArray as $key => $row) {
            $data[] = array('id'=>$row->id,'url'=> 'https://phanes.com.tw:17890/uploads/'.$row->img1, "type"=>$row->type);
        }


        return response()->json(['msg' => 'success','code' => '00', 'data'=>$data]);
    }

    public function productdetail(Request $request)
    {
        $dataArray = DB::table('mod_projects_detail')
        ->select('id', 'img1','title','intro','type')
        ->where('projects_id', $request->id)
        ->orderBy('sorted', 'asc')
        ->get();

        foreach ($dataArray as $key => $row) {
            $data[] = array('id'=>$row->id,'url'=> 'https://phanes.com.tw:17890/uploads/'.$row->img1, 'title'=>$row->title, 'intro'=>$row->intro , "type"=>$row->type);
        }


        return response()->json(['msg' => 'success','code' => '00', 'data'=>$data]);
    }

    public function ideal()
    {

        return view('ideal');
    }

    public function team()
    {
        $data = DB::table('mod_team')->get();
        return view('team', ['data' => $data]);
    }

    public function teamdetail($id) {
        $data = DB::table('mod_team')->where('id', $id)->first();
        return view('teamdetail', ['data' => $data]);
    }




    public function news()
    {
        $data = DB::table('mod_news')
        ->orderBy('years', 'desc')
        ->get();

        foreach ($data as $key => $row) {
            $color =  DB::table('bscode')
            ->where('cd_type', 'NEWSTYPE')
            ->where('cd', $row->type)
            ->first();
            $row->color = isset($color) ? $color->cd_descp : '#008961';
        }

        $years = DB::table('mod_news')
        ->select(
            db::raw('year(years) as year')
        )
        ->groupBy('year')
        ->get();

        $type = DB::table('mod_news')
        ->select('type')
        ->groupBy('type')
        ->get();

        return view('news', ['data' => $data, 'years' => $years, 'type' => $type]);
    }

    public function newsdetail($id) {
        $data = DB::table('mod_news')->where('id', $id)->first();
        $detail = DB::table('mod_news_detail')
        ->where('news_id', $id)
        ->orderBy('rank','asc')
        ->get();

        $color =  DB::table('bscode')
        ->where('cd_type', 'NEWSTYPE')
        ->where('cd', $data->type)
        ->first();

        return view('newsdetail', ['data' => $data, 'detail' => $detail, 'color' => $color]);
    }


    public function service()
    {
        \Log::info('enter service');
        return view('service');
    }



    public function newsList() {
        // dd('test');
        $newsData = DB::table('mod_news')->orderBy('years', 'desc')->get();

        $pagecount = DB::table('mod_news')->count();
        if($pagecount % 5 ==0){
            $pagecount = (int)$pagecount / 5;
        }
        else{
            $pagecount = (int)($pagecount / 5) +1;
        }
        return view('newsList', [
            'newsData' => $newsData, 'pagecount' => $pagecount,
            'pageid' => '1'
        ]);
    }

    public function contact()
    {
        // $newsData = DB::select("select * from pages where title='聯絡我們'; ");
        return view('contact');
    }

    public function sendmsg(Request $request)
    {
        $name    = $request->name;
        $phone   = $request->tel;
        $mail    = $request->mail;
        $type    = $request->type;
        $message = $request->message;
        try {
            \Log::info('enter send message');
            DB::table('mod_message')->insert([
                'name'       => $name,
                'phone'      => $phone,
                'cust_email'  => $mail,
                'send_mail'  => '已發送',
                'type'       => $type,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
                'message'    => $message
            ]);
            $sendData = [
                'name'       => $name,
                'phone'      => $phone,
                'cust_email' => $mail,
                'type'       => $type,
                'message'    => $message
            ];
            $sendpeople  = DB::select("select * from bscode where cd_type='SENDMAIL'");
            foreach ($sendpeople as $key => $row) {
                Mail::to($row->cd)
                ->send(new messagemail($sendData));
            }

        }
        catch(\Exception $e) { 
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return redirect()->to('service')->with(['message' => '網路發生問題請稍後再試']);
        }
        

        return redirect()->to('service')->with(['message' => '已收到您的問題，我們將盡快與您聯絡']);
    }

    public function sendmail(Request $request)
    {
        try {
            $name    = $request->name;
            $mail    = $request->email;
            $subject = $request->subject;
            $message = $request->message;
            $emails=["alex@phanes.com.tw","leon@phanes.com.tw"];

            $html = @'<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <title>Document</title>
            </head>
            <body>
                <p>
                <span>'.'名稱:'.$name.'</span>
                </p>
                <p>
                <span>'.'客戶信箱:'.$mail.'</span>
                </p>
                <p>
                <span>'.'內容:'.$message.'</span>
                </p>
            </body>
            </html>';
            Mail::send([], [], function ($message) use ($html,$emails,$subject ) {
                $message->to($emails)
                    ->subject($subject)
                    ->setBody($html, 'text/html');
            });
            return response()->json(['msg' => 'success','code' => '00', 'data'=>null]);
        }
        catch(\Exception $e) { 
            \Log::info($e->getMessage());
            \Log::info($e->getLine());
            return response()->json(['msg' => 'success','code' => '01', 'data'=>null, 'errorline'=>$e->getLine() ,'errormsg'=> $e->getMessage()]);
        }
        return response()->json(['msg' => 'success','code' => '00', 'data'=>null]);
    }

}