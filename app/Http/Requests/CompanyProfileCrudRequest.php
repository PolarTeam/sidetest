<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CompanyProfileCrudRequest  extends \Backpack\CRUD\app\Http\Requests\CrudRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'cust_no' => 'required|unique:sys_customers,cust_no,NULL,id,identity,'.request('identity').'|min:1|max:20',
                    'cname' => 'required|min:1|max:70',
                    'ename' => 'nullable|min:1|max:100',
                    'phone' => 'nullable|min:1|max:20',
                    'contact' => 'nullable|min:1|max:20',
                    'cmp_abbr' => 'nullable|min:1|max:50',
                    'address' => 'nullable|min:1|max:150'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [ ];
            }
            default:break;
        }
    }

}