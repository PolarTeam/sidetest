<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContactCrudRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'max:50',
            'name' => 'max:50',
            'email' => 'max:150'
        ];
    }

}