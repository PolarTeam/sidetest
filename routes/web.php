<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('admin/login');
});


// Route::get('/index2', 'WelcomeController@index2');

Route::get('menu', 'WelcomeController@menu');
Route::get('menudetail', 'WelcomeController@menudetail');
Route::get('product', 'WelcomeController@product');
Route::get('productdetail', 'WelcomeController@productdetail');
Route::get('sendmail', 'WelcomeController@sendmail');


Route::get('lang/{locale}', ['as'=>'lang.change', 'uses'=>'LanguageController@setLocale']);




Route::get('chkAuth', 'GuestCartController@chkAuth');



Route::group([
    'namespace' => '\Admin',
    'prefix'    => 'allpay'],
    function () {
        Route::get('/', 'AllPayController@index');
        Route::get('/checkout', 'AllPayController@checkout');
        Route::get('/payinmart', 'AllPayController@payinmart');
        Route::get('/payinmart_reply', 'AllPayController@payinmart_reply');
        Route::get('/send_msg', 'AllPayController@send_msg');
        
    }
);



Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('payment', function(){
    $viewData = [
        'viewName'    => 'payment'
    ];

    return View('FrontEnd.payment')->with($viewData);
});


Auth::routes();

Route::get('newcart', function() {
    $viewData = array(
        'viewName' => 'cart'
    );

    return view('FrontEnd.newCart')->with($viewData);
});
