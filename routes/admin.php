<?php
Auth::routes();
// Backpack\CRUD: Define the resources for the entities you want to CRUD.
CRUD::resource('tag', 'TagCrudController');
Route::get('/', 'Auth\LoginController@login');
Route::group([
        'middleware' => ['admin', 'auth', 'vendor']
], function () {
    
// Backpack\CRUD: Define the resources for the entities you want to CRUD.
    CRUD::resource('category', 'CategoryCrudController');
    CRUD::resource('banner', 'BannerCrudController');
    CRUD::resource('contact', 'ContactCrudController');
    CRUD::resource('home', 'LoginController');
    CRUD::resource('projects', 'projectsCrudController');
    
    
    CRUD::resource('user', 'UserController');
    CRUD::resource('customer', 'CustomerCrudController');

    CRUD::resource('customerProfile', 'CustomerProfileCrudController');
    Route::post('custProfile/multi/del', 'CustomerProfileCrudController@multiDel'); 

    CRUD::resource('companyProfile', 'CompanyProfileCrudController');

    CRUD::resource('bulletin', 'BulletinCrudController');


    CRUD::resource('bscodeKind', 'BscodeKindCrudController');
    Route::get('bscode/{cd_type?}', 'BscodeKindCrudController@get');
    Route::post('bscode/store', 'BscodeKindCrudController@detailStore');
    Route::post('bscode/update/{cd_type}', 'BscodeKindCrudController@detailUpdate');
    Route::get('bscode/delete/{id}', 'BscodeKindCrudController@detailDel');
    Route::get('bscodeKind/detail/{cd_type}', 'BscodeKindCrudController@get');

    CRUD::resource('mailFormat', 'MailFormatCrudController');

    CRUD::resource('MemberMgmt', 'MemberMgmtCrudController');   //會員管理
    Route::post('activeMember', 'MemberMgmtCrudController@activeMember'); 


    CRUD::resource('CateMgmt', 'CateMgmtCrudController');   //類別管理

    CRUD::resource('sysCountry', 'SysCountryCrudController');   //國家代碼建檔
    CRUD::resource('sysArea', 'SysAreaCrudController');   //區域建檔
    CRUD::resource('modTransStatus', 'ModTransStatusCrudController');   //貨況建檔

    
});

Route::group([
    'middleware' => ['admin', 'auth']
], function () {    

    // 0321 新增後台套版
    CRUD::resource('menu', 'TeamCrudController');
    Route::get('menu/{id}', 'TeamCrudController@get');
    Route::post('menu/multi/del', 'TeamCrudController@multiDel');
    Route::get('menuDetail/{id}', 'TeamCrudController@get');
    Route::post('menuDetail/detailStore', 'TeamCrudController@detailStore');
    Route::post('menuDetail/update/{cd_type}', 'TeamCrudController@detailUpdate');
    Route::get('menuDetail/delete/{id}', 'TeamCrudController@detailDel');


    CRUD::resource('projects', 'projectsCrudController');
    Route::get('projectsDetail/{id}', 'projectsCrudController@get');
    Route::post('projectsDetail/detailStore', 'projectsCrudController@detailStore');
    Route::post('projectsDetail/update/{cd_type}', 'projectsCrudController@detailUpdate');
    Route::get('projectsDetail/delete/{id}', 'projectsCrudController@detailDel');
    Route::get('projectsDetailcate/{id}', 'projectsCrudController@getcate');
    Route::post('projectsDetailcate/cateStore', 'projectsCrudController@cateStore');
    Route::post('projectsDetailcate/update/{cd_type}', 'projectsCrudController@cateUpdate');
    Route::get('projectsDetailcate/catedelete/{id}', 'projectsCrudController@cateDel');
    Route::post('projectsDetailcate/multi/del', 'projectsCrudController@multiDel');
    Route::post('projectsDetail/multi/del', 'projectsCrudController@multiDel');
    Route::get('projectsinfo/{id}', 'projectsCrudController@infoget');
    Route::post('projectsinfo/infoStore', 'projectsCrudController@infoStore');
    Route::post('projectsinfo/update/{cd_type}', 'projectsCrudController@infoUpdate');
    Route::get('projectsinfo/infodelete/{id}', 'projectsCrudController@infoDel');

    CRUD::resource('modNews', 'modNewsCrudController');
    Route::post('modNews/multi/del', 'modNewsCrudController@multiDel');

    Route::post('modNewsDetail/detailStore', 'modNewsCrudController@detailStore');
    Route::post('modNewsDetail/update/{cd_type}', 'modNewsCrudController@detailUpdate');
    Route::get('modNewsDetail/delete/{id}', 'modNewsCrudController@detailDel');
    Route::get('modNewsDetail/{id}', 'modNewsCrudController@getimg');
    
    // news


    // ---- 以下都是 舊有的
    CRUD::resource('category', 'CategoryCrudController');
    CRUD::resource('banner', 'BannerCrudController');
    CRUD::resource('contact', 'ContactCrudController');
    CRUD::resource('home', 'LoginController');
    CRUD::resource('companyProfile', 'CompanyProfileCrudController');
    CRUD::resource('modBase', 'BaseCrudController');

    CRUD::resource('modmedia', 'mediaCrudController');
    CRUD::resource('page', 'PageCrudController');
    CRUD::resource('modBanner', 'BannerCrudController');
    CRUD::resource('modMarquee', 'MarqueeCrudController');
    CRUD::resource('modMarquee', 'MarqueeCrudController');
    CRUD::resource('modUnboxing', 'UnboxingCrudController');
    CRUD::resource('modDiscount', 'DiscountCrudController');
    CRUD::resource('mailFormat', 'MailFormatCrudController');
    Route::get('searchList/get/{key}', 'CommonController@getSearchLayoutList');
    Route::get('searchHtml/get/{id?}', 'CommonController@getSearchHtml');
    Route::post('saveSearchLayout', 'CommonController@saveSearchLayout');
    Route::put('setSearchDefault/{key}/{id}', 'CommonController@setSearchDefault');
    Route::delete('delSearchLayout/{id}', 'CommonController@delSearchLayout');

    CRUD::resource('bscodeKind', 'BscodeKindCrudController');
    Route::get('bscode/{cd_type?}', 'BscodeKindCrudController@get');
    Route::post('bscode/store', 'BscodeKindCrudController@detailStore');
    Route::post('bscode/update/{cd_type}', 'BscodeKindCrudController@detailUpdate');
    Route::get('bscode/delete/{id}', 'BscodeKindCrudController@detailDel');
    Route::get('bscodeKind/detail/{cd_type}', 'BscodeKindCrudController@get');


    Route::post('userPwd/update', 'CommonController@updatePwd');

    Route::get('get/{table?}/{id?}', 'CommonController@getData'); 
    Route::get('gcsd/get', 'CommonController@getCompnayList'); 
        
    Route::get('board', 'DashboardController@dashboard')->name('admin.board');
    Route::get('getFieldsLangSepc/{table}', 'BaseApiController@getFieldsLangSepc');


    Route::get('promotionDetailcate/{id}', 'promotionCrudController@getcate');
    Route::post('promotionDetailcate/cateStore', 'promotionCrudController@cateStore');
    Route::post('promotionDetailcate/update/{cd_type}', 'promotionCrudController@cateUpdate');
    Route::get('promotionDetailcate/catedelete/{id}', 'promotionCrudController@cateDel');


    Route::get('promotioninfo/{id}', 'promotionCrudController@infoget');
    Route::post('promotioninfo/infoStore', 'promotionCrudController@infoStore');
    Route::post('promotioninfo/update/{cd_type}', 'promotionCrudController@infoUpdate');
    Route::get('promotioninfo/infodelete/{id}', 'promotionCrudController@infoDel');

    Route::post('promotionDetail/multi/del', 'promotionCrudController@multiDel');
    
    CRUD::resource('mail', 'mailCrudController');
    Route::post('mail/multi/del', 'mailCrudController@multiDel');

    CRUD::resource('about', 'aboutCrudController');

    CRUD::resource('process', 'processCrudController');
    Route::get('processDetail/{id}', 'processCrudController@get');
    Route::post('processDetail/detailStore', 'processCrudController@detailStore');
    Route::post('processDetail/update/{cd_type}', 'processCrudController@detailUpdate');
    Route::get('processDetail/delete/{id}', 'processCrudController@detailDel');
    Route::post('processDetail/multi/del', 'processCrudController@multiDel');

    CRUD::resource('processimg', 'processimgCrudController');
    Route::get('processimgdetail/{id}', 'processimgCrudController@get');
    Route::post('processimgdetail/detailStore', 'processimgCrudController@detailStore');
    Route::post('processimgdetail/update/{cd_type}', 'processimgCrudController@detailUpdate');
    Route::get('processimgdetail/delete/{id}', 'processimgCrudController@detailDel');
    
    Route::post('processimg/multi/del', 'processimgCrudController@multiDel');

    Route::get('processbanner/{id}', 'processCrudController@imgget');
    Route::post('processbanner/detailStore', 'processCrudController@imgStore');
    Route::post('processbanner/update/{cd_type}', 'processCrudController@imgUpdate');
    Route::get('processbanner/delete/{id}', 'processCrudController@imgDel');


    CRUD::resource('promotion', 'promotionCrudController');
    Route::get('promotionDetail/{id}', 'promotionCrudController@get');
    Route::post('promotionDetail/detailStore', 'promotionCrudController@detailStore');
    Route::post('promotionDetail/update/{cd_type}', 'promotionCrudController@detailUpdate');
    Route::get('promotionDetail/delete/{id}', 'promotionCrudController@detailDel');



});

Route::auth();
Route::get('logout', 'Auth\LoginController@logout');

