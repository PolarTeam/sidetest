<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $row = 1;
        setlocale(LC_ALL, 'zh_TW');
        if (($handle = fopen("./cust.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $num = count($data);
                $cust_no = "CSTD".str_pad($row, 5, "0", STR_PAD_LEFT);
                $row++;

                $insertData = [
                    'cust_no'    => $cust_no,
                    'cname'      => mb_convert_encoding($data[0], "UTF-8"),
                    'ename'      => mb_convert_encoding($data[1], "UTF-8"),
                    'industry'   => mb_convert_encoding($data[2], "UTF-8"),
                    'zip_code'   => mb_convert_encoding($data[3], "UTF-8"),
                    'address'    => mb_convert_encoding($data[4], "UTF-8"),
                    'en_address' => mb_convert_encoding($data[5], "UTF-8"),
                    'phone'      => mb_convert_encoding($data[6], "UTF-8"),
                    'fax'        => mb_convert_encoding($data[7], "UTF-8"),
                    'email'      => mb_convert_encoding($data[8], "UTF-8"),
                    'website'    => mb_convert_encoding($data[9], "UTF-8"),
                    'type'       => 'SELF',
                    'status'     => 'B',
                    'created_at' => 'TIM',
                    'g_key'      => 'STD',
                    'c_key'      => 'STD',
                    'd_key'      => 'STD',
                    's_key'      => 'STD',
                ];

                DB::table('sys_customers')->insert($insertData);
            }
            fclose($handle);
        }
        return;
    }
}
